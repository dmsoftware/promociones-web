<?php
namespace AppBundle\Command;

use IsmaAmbrosi\Bundle\GeneratorBundle\Generator\DoctrineCrudGenerator;

class DefaultDoctrineCrudCommand extends \IsmaAmbrosi\Bundle\GeneratorBundle\Command\GenerateDoctrineCrudCommand
{
    protected function configure()
    {
        parent::configure();
        $this->setName('doctrine:mongodb:generate:default:crud');
    }

    protected function getGenerator()
    {
        $generator = new DoctrineCrudGenerator($this->getContainer()->get('filesystem'), __DIR__.'/../../../app/Resources/IsmaAmbrosiGeneratorBundle/skeleton/crud');
        $this->setGenerator($generator);
        return parent::getGenerator();
    }
}

?>