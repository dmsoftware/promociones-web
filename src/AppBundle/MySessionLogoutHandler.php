<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle;

use Symfony\Component\Security\Http\Logout\SessionLogoutHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Doctrine\ORM\EntityManager;

/**
 * Description of LogoutSuccessHandler
 *
 * @author Aritz
 */
class MySessionLogoutHandler extends SessionLogoutHandler {
  private $em;

  public function __construct(EntityManager $em) {
    $this->em = $em;
  }
  
  public function logout(Request $request, Response $response, TokenInterface $token) {
    $user = $token->getUser();

    $logoutAuto = $user->getLogoutAuto();
    
    $user->setLogoutAuto(false);
    $user->setSessionId(null);
    $this->em->flush();
    
    if($logoutAuto){
      $this->em->getRepository('CommonBundleAdministratorBundle:LogAccess')->saveAction('logout_cs', $user);
    }else{
      $this->em->getRepository('CommonBundleAdministratorBundle:LogAccess')->saveAction('logout', $user);
    }

    parent::logout($request, $response, $token);
  }
}
