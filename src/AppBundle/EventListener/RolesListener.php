<?php

namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\ORM\EntityManager;
use CommonBundle\AdministratorBundle\Entity\LogUse;
use CommonBundle\UserBundle\Entity\UserAccess;


class RolesListener {
  private $em;
  private $context;
  private $container;
  private $router;
  private $redirectRoute;

  public function __construct(SecurityContextInterface $securityContext, Router $router, EntityManager $em) {
    $this->em = $em;
    $this->context = $securityContext;
    $this->router = $router;
    $this->redirectRoute = 'common_bundle_user_no_permissions';
    $this->restrictedPages = array(
		'common_bundle_user_no_permissions',
		'fos_user_change_password',
		'fos_user_security_logout',
		'common_bundle_user_access_forbidden',
		'common_bundle_user_change_language'
	);
  }

  public function onKernelController(FilterControllerEvent $event) {
    
    try {
      $request = $event->getRequest();
      $referer = $request->headers->get('referer');

      // Controlar restricciones de IPs
      $ips = $this->em->getRepository('CommonBundleAdministratorBundle:IpControl')->findAll();

      if(count($ips)){
        $valid = false;

        foreach($ips as $ip){
          if($_SERVER['REMOTE_ADDR'] === $ip->getIp()){
            $valid = true;
            break;
          }
        }

        if(!$valid){
          exit('<h1>Acceso denegado desde esta IP</h1>');
        }
      }

      $token = $this->context->getToken();
      
      if ($token) {
        $user = $token->getUser();
        $requestUrl = $request->get('_route');

        if($this->context->isGranted('IS_AUTHENTICATED_FULLY')){
          // Controlar las sesiones simultaneas
          $cs = $this->em->getRepository('CommonBundleAdministratorBundle:Configuracion')->conexionesSimultaneas();
          if(!$cs){
            $session = $request->getSession();
            if($session && $user->getSessionId() && $user->getSessionId() != $session->getId() && strpos($referer, '/login') === false){            
              $redirectUrl = $this->router->generate('fos_user_security_logout');
              $user->setLogoutAuto(true);
              $this->em->flush();
              $event->setController(function() use ($redirectUrl) {
                return new RedirectResponse($redirectUrl);
              });
            }
          }

          // Controlar los permisos
          if (!$user->hasRole('ROLE_ADMIN')) { 
            if(!$user->checkAccess($request->attributes->get('_controller'), $this->em)){
              if($requestUrl !== $this->redirectRoute && $requestUrl !== 'project_homepage' && $requestUrl !== 'common_bundle_administrator_homepage' && $requestUrl !== 'dmintegra_app_default_index'){
                $redirectUrl = $this->router->generate($this->redirectRoute);
                $event->setController(function() use ($redirectUrl) {
                  return new RedirectResponse($redirectUrl);
                });
              }
            }

            // Controlar que la clave no está caducada
            if($user->getClaveCaducada()){            
              if($requestUrl !== 'fos_user_change_password'){
                $redirectUrl = $this->router->generate('fos_user_change_password');
                $event->setController(function() use ($redirectUrl) {
                  return new RedirectResponse($redirectUrl);
                });
              }
            }
			
			// Controlar accesos usuario
			$rol = $this->em->getRepository('CommonBundleUserBundle:Role')->findOneByMachinename($token->getUser()->getTipo());
			$useraccess = $this->em->getRepository('CommonBundleUserBundle:UserAccess')->findByRole($rol);
			$allow = empty($useraccess);
			foreach ($useraccess as $access) {
			  if ($access->getRoute() == '*' && $access->getAllow()) {
				$allow = true;
			  } else if ($access->getRoute() === $requestUrl && $access->getAllow()) {
				$allow = true;
			  } else if (strpos($access->getRoute(), '*') !== false && strlen($access->getRoute()) > 1) {
				if (strpos($requestUrl, str_replace('*', '', $access->getRoute())) !== false) {
				  $allow = true;
				}
			  } else if ($access->getRoute() === $requestUrl && !$access->getAllow()) {
				$allow = false;
				break;
			  }
			}
			if (!$allow && !empty($requestUrl) && !in_array($requestUrl, $this->restrictedPages)) {
              $redirectUrl = $this->router->generate('common_bundle_user_access_forbidden', array('path' => $requestUrl));
              $event->setController(function() use ($redirectUrl) {
                return new RedirectResponse($redirectUrl);
              });
			 }
			  
		  }
	  
		  // Registrar a dónde a accedido el usuario
		  if (!empty($requestUrl) && strpos($requestUrl, '_ajax') === false) {
			$loguse = new LogUse();
			$loguse->setUsuario($token->getUser());
			$loguse->setUrl($request->getUri());
			$loguse->setController($request->attributes->get('_controller'));
			$loguse->setAccess(new \DateTime());
			
			$this->em->persist($loguse);
			$this->em->flush();	
		  }
		  
        }
		  
      }  
	  
    } catch (\Exception $e) {
      echo $e->getMessage();
      return;
    }
  }
}
