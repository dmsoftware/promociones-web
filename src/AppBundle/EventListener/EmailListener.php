<?php

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class EmailListener implements EventSubscriberInterface {

  /**
   * @var Swift_Transport_EsmtpTransport
   */
  private $transport;

  /**
   * @var Doctrine\ORM\EntityManager
   */
  private $em;

  public function __construct($transport, $em) {
    $this->transport = $transport;
    $this->em = $em;
  }

  public function onKernelRequest(GetResponseEvent $event) {
    $conf = $this->em->getRepository('CommonBundleAdministratorBundle:Configuracion')->getConfiguration();

    if($conf){
      $this->transport->setHost($conf->getSenderHost());
      $this->transport->setPort($conf->getSenderPort());
      $this->transport->setEncryption($conf->getSenderEncryption());
      $this->transport->setUserName($conf->getSenderUser());
      $this->transport->setPassword($conf->getSenderPass());
      if($conf->getSenderEncryption() === 'ssl'){
        $this->transport->setAuthMode("login");
      }
    }
    
    $request = $event->getRequest()->request;
    if($request->has('sendTransport')){
      $request->add(array('transport' => $this->transport));
    }
  }

  static public function getSubscribedEvents() {
    return array(
        KernelEvents::REQUEST => array('onKernelRequest', 0)
    );
  }

}
