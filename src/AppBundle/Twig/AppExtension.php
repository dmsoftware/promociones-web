<?php

namespace AppBundle\Twig;

use Symfony\Component\Intl\Intl;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use AppBundle\Util\Util;

class AppExtension extends \Twig_Extension {

  private $generator;

  public function __construct(UrlGeneratorInterface $generator) {
    $this->generator = $generator;
  }

  public function getFilters() {
    return array(
        new \Twig_SimpleFilter('lang', array($this, 'languageFilter')),
        new \Twig_SimpleFilter('menu', array($this, 'menuFilter')),
        new \Twig_SimpleFilter('semaphore', array($this, 'semaphoreFilter')),
        new \Twig_SimpleFilter('file_exists', array($this, 'fileExistsFilter')),
        new \Twig_SimpleFilter('select2ajax', array($this, 'select2ajaxFilter')),
        new \Twig_SimpleFilter('isHidden', array($this, 'isHiddenFilter')),
        new \Twig_SimpleFilter('json_decode', array($this, 'jsonDecode'))
    );
  }
  
  public function isHiddenFilter($field){
    $key = $field->vars['cache_key'];
    
    return strpos($key, 'hidden') !== false;
  }
  
  public function select2ajaxFilter($filter){
    if(isset($filter['ajax'])){
      $ruta = $this->generator->generate($filter['ajax']['route'], $filter['ajax']['params']);
    
      echo 'data-ajax--url="'.$ruta.'"';  
    }
  }
  
  public function fileExistsFilter($file){
    return file_exists($file);
  }
  
  public function semaphoreFilter($value, $width = 16){
    echo Util::semaphore($value, $width);
  }

  public function languageFilter($locale) {
    return Util::getLocaleName($locale);
  }

  public function menuFilter($tree,$locale = 'es') {
    echo Util::renderTree($tree, 0, $this->generator, true, array(), $locale);
  }

  public function getName() {
    return 'app_extension';
  }

  public function jsonDecode($str) {
	return json_decode($str);
  }

}
