<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Util\DataTableEntity;
use AppBundle\Util\Util;

class DefaultController extends Controller {

  protected $bundle;
  protected $controller;
  protected $action;
  protected $entity;
  protected $em;
  protected $rep;
  protected $responseData;
  private $originHead;
  private $translatables;

  public function indexAction(Request $request, $class = '') {    
    $data = array();
    $this->getRequestParameters($class);

    if ($this->entity) {
      $data = array('generator' => $this->rep->getGenerator());
    }

    return $this->generateResponse($data);
  }

  public function showAction($id, Request $request, $class = '') {
    $data = array();
    $this->getRequestParameters($class);

    if ($this->entity) {
      $locale = $request->getLocale();
      $router = $this->get('router');
      $trans = $this->get('translator');
      $entity = $this->rep->getEntityData($id, $locale, $router, $trans);
    }

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find User entity.');
    }

    $data['generator'] = $this->rep->getGenerator('show');
    $data['entity'] = $entity;

    return $this->generateResponse($data);
  }

  public function newAction(Request $request, $class = '') {
    
    //Fix translation error in non default-locale
    $translatable = $this->get('stof_doctrine_extensions.listener.translatable');
    $translatable->setTranslatableLocale($translatable->getDefaultLocale());

    $this->getRequestParameters($class);
    $generator = $this->rep->getGenerator('new');

    $entity = new $this->entityClass();
  
    $this->translatables = $this->getTranslatableFieldsClass($entity,'new');
    $generator['translatables'] = $this->translatables;
  
    $get_params = $request->query->all();
    $generator['origin'] = (!empty($get_params['originId']) && !empty($get_params['originClass'])) ? array('originId' => $get_params['originId'], 'originClass' => $get_params['originClass']) : NULL;
    
    $form = $this->createCreateForm($entity, $class, '', $generator);
    $form->handleRequest($request);

    $valid = 0;
    if ($form->isValid()) {
      $valid = 1;

      $entity = $this->proccessForm($generator['formRelations'], $entity, $form, true);

      $this->responseData = array('entity' => $entity);
      $session = $request->getSession();
      $session->getFlashBag()->add('success', $this->get('translator')->trans('form.flash.success'));
    }

    return $this->generateResponse(array('form' => $form->createView(), 'entity' => $entity, 'valid' => $valid, 'class' => $class, 'generator' => $generator, 'cabeceraOrigin' => $this->originHead));
  }

  private function createCreateForm($entity, $class, $route, $generator) {
    $form = $this->generateForm('new', $class, $entity, $generator);
  $path_params = (!empty($generator['origin'])) ? '?originId=' . $generator['origin']['originId'] . '&originClass=' . $generator['origin']['originClass'] : '';
    
    if ($route){
      $form->setAction($this->generateUrl($route));
    }else{
      $form->setAction($this->generateUrl($this->rep->getRoutePrefix() . '_new') . $path_params);
    }
    
    $form->add('save', 'submit', array('attr' => array('class' => 'tiny'), 'label' => $this->get('translator')->trans('form.action.save')));

    return $form->getForm();
  }

  private function generateForm($action, $class, $entity, $generator) {
    $request = $this->getRequest();
    $origin = null;
    $this->originHead = false;
    
    $arrayTranslations = array();

    if ($request->get('originId') && $request->get('originClass')) {
      $origin_class = $request->get('originClass');
      if (strpos($origin_class, '\\') === false) {
        $origin_class = $this->entityFolder . $origin_class;
      }
      $origin = $this->em->getRepository($origin_class)->find($request->get('originId'));

      if ($origin) {
        $this->originHead = $this->em->getRepository($origin_class)->getHeadData($origin, $request->getLocale(), $this->get('router'), $this->get('translator'));
      }

      if ($action == 'new') {
        foreach ($this->em->getClassMetadata($this->entityClass)->associationMappings as $key => $tmp) {
          if ($tmp['targetEntity'] === $origin_class) {
            $method = Util::getObjectFieldMethodName($key, 'set');
            $entity->$method($origin);
            break;
          }
        }
      }
    }
    
    $this->rep->setFormOrigin($origin);
    $this->rep->setObject($entity);
    $formFields = $this->rep->getFormFields($action);

    $form = $this->get('form.factory')->createNamedBuilder('form' . $class, 'form', $entity, array('attr' => array('id' => 'JSmodalForm_' . $class)));

    foreach ($formFields as $field => $datos) {
      $datos = $this->modifyFieldOptions($datos);
      if (isset($datos['options']['widget']) && $datos['options']['widget'] == 'single_text' && isset($datos['options']['attr'])) {
        if(strpos($datos['options']['attr']['class'], 'fdatepicker') !== false){
          $datos['options']['format'] = Util::getDatePickerFormat($this->getRequest()->getLocale());
        }
        if(strpos($datos['options']['attr']['class'], 'fdatetimepicker') !== false){
          $datos['options']['format'] = Util::getDatePickerFormat($this->getRequest()->getLocale()).' H:m';
        }
      }
      if(isset($datos['options']['attr']) && isset($datos['options']['attr']['ajax'])){

        $form->addEventListener(\Symfony\Component\Form\FormEvents::PRE_SUBMIT, function (\Symfony\Component\Form\FormEvent $event) use ($field, $datos) {
          $data = $event->getData();

          if(is_array($data[$field])) $data = array_flip($data[$field]);
          else $data = $data[$field];

          if ($this->getRequest()->get('id') != '') {
            $method = 'set' . ucfirst($field);
            $entity = $this->em->getRepository($this->bundle . ':' . $this->entity)->findOneById($this->getRequest()->get('id'));
            $entity->$method($data);
            $this->em->persist($entity);
          }

          $datos['options']['choices'] = $data;
          $event->getForm()->add($field, $datos['type'], $datos['options']);
        });
      }
      if(isset($datos['options']['attr']) && isset($datos['options']['attr']['data-ajax--url'])){

        $form->addEventListener(\Symfony\Component\Form\FormEvents::PRE_SUBMIT, function (\Symfony\Component\Form\FormEvent $event) use ($field, $datos) {
          $data = $event->getData();

          if(is_array($data[$field])) $data = array_flip($data[$field]);
          else $data = $data[$field];

          $datos['options']['choices'] = $data;
          $event->getForm()->add($field, $datos['type'], $datos['options']);
        });
      }
      if(isset($datos['options']['attr']['readonly']) && $datos['options']['attr']['readonly']){
        $datos['options']['attr']['readonlyTxt'] = $this->rep->renderRowCol($entity, $field, $this->rep->getColData($field), $request->getLocale(), $this->get('router'), $this->get('translator'));
      }


      //Si es translatable no va individual sino dentro de translations 
      if (!in_array($field, $this->translatables)){
        $form->add($field, $datos['type'], $datos['options']);
      } else {
        //$form->add($field, $datos['type'], $datos['options']);
        if (isset($datos['options']['required']) && $datos['options']['required']){
          $req = $datos['options']['required'];
        } else {
          $req = false;
        }       
        $arrayTranslations[]= array('field'=>$field,'required'=>$req,'type'=>$datos['type']);        
      }
    }
    
    foreach($this->rep->ooRelations as $tab => $tabClass){
      $form->add($tab, new $tabClass['formClass'](), array('required' => false, 'label' => false, 'attr' => array('class' => 'well')));  
    }

    //insertamos campo translations
    if (!empty($this->translatables)&&!empty($arrayTranslations)){
      $fieldsArray = $this->setTranslatableFieldArray($arrayTranslations, $generator['labels'],$action);
      $form->add('translations', 'a2lix_translations_gedmo', array(
                'translatable_class' => $this->entityClass,
                'label' => false,
                'fields' => $fieldsArray)
                );
    }
    return $form;
  }

  private function modifyFieldOptions($datos) {
    if (isset($datos['options']['attr']) && isset($datos['options']['attr']['data-ajax--url'])) {
      $ruta = $datos['options']['attr']['data-ajax--url'];
      $params = array();
      if (isset($datos['options']['attr']['data-ajax--params'])) {
        $params = $datos['options']['attr']['data-ajax--params'];
        unset($datos['options']['attr']['data-ajax--params']);
      }
      $url = $this->generateUrl($ruta, $params);
      $datos['options']['attr']['data-ajax--url'] = $url;
    }

    return $datos;
  }

  private function proccessForm($relations, $entity, $form, $new = false) {
    $this->cleanEntityRelations($relations, $new, $entity);
    
    foreach ($relations as $rel => $relType) {
      $ids = array();
      $objs = new \Doctrine\Common\Collections\ArrayCollection();
      $opts = $form->get($rel)->getConfig()->getOptions();
      $method_get = Util::getObjectFieldMethodName($rel, 'get');

      foreach ($form->get($rel)->getData() as $relObj) {
        if ($relType === 'collection') {
          $parentObj = $relObj->$method_get();
          if (!$parentObj->getId() || !in_array($parentObj->getId(), $ids)) {
            if (isset($opts['options']['attr']['exclude'])) {
              $method = Util::getObjectFieldMethodName($opts['options']['attr']['exclude'], 'set');
              $relObj->$method($entity);
            }
            $this->em->persist($relObj);
            $objs->add($relObj);
            $ids[] = $parentObj->getId();
          }
        }
        if ($relType === 'entity') {
          if ($new) {
            $method_add = Util::getObjectFieldMethodName($rel, 'addRel');
            $tmp = $entity->$method_add($relObj);
            $this->em->persist($tmp);
          } elseif (isset($this->originRels[$rel])) {
            $key = array_search($relObj, $this->originRels[$rel]);
            if (is_numeric($key)) {
              unset($this->originRels[$rel][$key]);
            } else {
              $method_add = Util::getObjectFieldMethodName($rel, 'addRel');
              $tmp = $entity->$method_add($relObj);
              $this->em->persist($tmp);
            }
          }
        }
      }

      if (!$new) {
        $this->deleteUnSelectedRelations($rel, $relType, $objs, $entity);
      }
    }

    try {
      $this->em->flush();
    } catch (\Exception $e) {
      //echo 'Error: ' . $e->getMessage();
    }

    return $entity;
  }

  private function cleanEntityRelations($relations, $new, $entity) {
    if (!property_exists($this, 'originRels')) {
      $this->originRels = array();
    }

    foreach ($relations as $rel => $relType) {
      if (!$new && $relType == 'collection') {
        $method = Util::getObjectFieldMethodName($rel, 'get');
        $aux = $entity->$method();
        $this->originRels[$rel] = $aux->getSnapshot();
      }

      $method = Util::getObjectFieldMethodName($rel, 'unset');
      $entity->$method();
    }

    $this->em->persist($entity);

    try {
      $logAction = Util::logAction($this->em, $entity, $new, $this->getUser());
      $this->em->flush();
      Util::saveLogAction($logAction, $entity);
    } catch (\Exception $e) {
      //echo 'EEROR: '.$e->getMessage();
    }
  }

  private function deleteUnSelectedRelations($rel, $relType, $objs, $entity) {
    if ($relType == 'collection' && isset($this->originRels[$rel])) {
      foreach ($this->originRels[$rel] as $obj) {
        if ($obj && !$objs->contains($obj)) {
          $this->em->remove($obj);
        }
      }
    }
    if ($relType == 'entity' && isset($this->originRels[$rel])) {
      $method = Util::getObjectFieldMethodName($rel, 'getRel');
      foreach ($this->originRels[$rel] as $dato) {
        $del = $entity->$method($dato);
        if ($del) {
          $this->em->remove($del);
        }
      }
    }
  }

  public function editAction($id, Request $request, $class = '') {

    //Fix translation error in non default-locale
    $translatable = $this->get('stof_doctrine_extensions.listener.translatable');
    $translatable->setTranslatableLocale($translatable->getDefaultLocale());

    $this->getRequestParameters($class);

    $generator = $this->rep->getGenerator('edit');
    $entity = $this->rep->getEntity($id);

    $this->translatables = $this->getTranslatableFieldsClass($entity,'edit');
    $generator['translatables'] = $this->translatables;

    $this->originRels = array();
    foreach ($generator['formRelations'] as $rel => $relType) {
      if ($relType == 'entity') {
        $method = Util::getObjectFieldMethodName($rel, 'get');
        $this->originRels[$rel] = $entity->$method();
      }
    }

    $hasFile = false;
    $formFields = $this->rep->getFormFields('edit');
    foreach ($formFields as $field => $datos) {
      if(isset($datos['options']['attr']) && isset($datos['options']['attr']['data-type'])){
        $hasFile = true;
      }
    }
  
    $get_params = $request->query->all();
    $generator['origin'] = (!empty($get_params['originId']) && !empty($get_params['originClass'])) ? array('originId' => $get_params['originId'], 'originClass' => $get_params['originClass']) : NULL;

    $form = $this->createEditForm($entity, $class, '', $generator);
    $form->handleRequest($request);

    $valid = 0;
    if ($this->getRequest()->isMethod('POST') && $hasFile) {
      $valid = 1;

      $entity = $this->proccessForm($generator['formRelations'], $entity, $form);
      $this->responseData = array('entity' => $entity);

      $session = $request->getSession();
      $session->getFlashBag()->add('success', $this->get('translator')->trans('form.flash.success'));
    } else if ($form->isValid()) {
      $valid = 1;

      $entity = $this->proccessForm($generator['formRelations'], $entity, $form);
      $this->responseData = array('entity' => $entity);

      $session = $request->getSession();
      $session->getFlashBag()->add('success', $this->get('translator')->trans('form.flash.success'));
    }

    $cabecera = $this->rep->getHeadData($entity, $request->getLocale(), $this->get('router'), $this->get('translator'));

    return $this->generateResponse(array('form' => $form->createView(), 'entity' => $entity, 'valid' => $valid, 'class' => $class, 'generator' => $generator, 'cabecera' => $cabecera, 'cabeceraOrigin' => $this->originHead));
  }

  protected function createEditForm($entity, $class, $route = '', $generator) {
    $form = $this->generateForm('edit', $class, $entity, $generator);
    $path_params = (!empty($generator['origin'])) ? '?originId=' . $generator['origin']['originId'] . '&originClass=' . $generator['origin']['originClass'] : '';

    if ($route)
      $form->setAction($this->generateUrl($route, array('id' => $entity->getId())));
    else
      $form->setAction($this->generateUrl($this->rep->getRoutePrefix() . '_edit', array('id' => $entity->getId())) . $path_params);
    $form->add('save', 'submit', array('attr' => array('class' => 'tiny'), 'label' => $this->get('translator')->trans('form.action.save')));

    return $form->getForm();
  }

  public function deleteAction($id, Request $request, $class = '') {
    $this->getRequestParameters($class);

    $ids = $request->get('ids', array());

    if ($id)
      $ids[] = $id;

    $qb = $this->rep->createQueryBuilder('t');
    $q2 = $this->rep->createQueryBuilder('t');
    $q = $qb->delete();

    if ($request->get('nm', 0) == '1') {
      $multpIds = array();
      foreach ($ids as $id) {
        $tmp = explode('_', $id);
        foreach ($tmp as $i => $t) {
          $multpIds[$i][] = $t;
        }
      }
      foreach ($this->rep->getClass()->identifier as $i => $idCol) {
        $q->andWhere($qb->expr()->in('t.' . $idCol, $multpIds[$i]));
        $q2->andWhere($q2->expr()->in('t.' . $idCol, $multpIds[$i]));
      }
    } else {
      $q->andWhere($qb->expr()->in('t.id', $ids));
      $q2->andWhere($q2->expr()->in('t.id', $ids));
    }

    $objs = $q2->getQuery()->getResult();

    Util::logActionDelete($objs, $this->rep, $this->em, $this->getUser());

    $q = $q->getQuery();
    $sql = $q->getSQL();
    $result = $q->getResult();

    $this->logDelete($sql, $result);

    return new Response(json_encode(array('result' => $result)));
  }

  public function seleccionAjaxAction(Request $request) {
    $em = $this->getDoctrine()->getManager();
    $params = $request->request->all();
    $params['filtros'] = $request->query->all();
    $locale = $request->getLocale();
    $router = $this->get('router');
    $trans = $this->get('translator');

    $dte = new DataTableEntity($params, $em, $locale, $router, $trans);
    $datos = $dte->getData();

    return new Response(json_encode($datos));
  }

  public function accionesTablaAction(Request $request) {
    $acciones = $request->get('acciones');
    $route_prefix = $request->get('route_prefix');
    $tabla = $request->get('tabla');

    return $this->render('AppBundle:Default:accionesTabla.html.twig', array(
                'acciones' => $acciones,
                'route_prefix' => $route_prefix,
                'tabla' => $tabla
    ));
  }

  public function accionesTablaRelAction(Request $request) {
    $acciones = $request->get('acciones');
    $route_prefix = $request->get('route_prefix');
    $tabla = $request->get('tabla');
    $origin_id = $request->get('originId');
    $origin_class = $request->get('originClass');

    return $this->render('AppBundle:Default:accionesTablaRel.html.twig', array(
                'acciones' => $acciones,
                'route_prefix' => $route_prefix,
                'tabla' => $tabla,
                'originId' => $origin_id,
                'originClass' => $origin_class
    ));
  }

  public function filtrosTablaAction(Request $request) {
    $col = $request->get('col');
    $filtro = $request->get('filtro');
    $entity = $request->get('entity');
    $obj = $request->get('obj', null);

    if (is_array($filtro) && isset($filtro['fnChoices'])){
      $filtro['choices'] = $obj->$filtro['fnChoices']();
    }
    if (is_array($filtro) && $filtro['type'] === 'entity' && !isset($filtro['choices']) && !isset($filtro['ajax'])) {
      $em = $this->getDoctrine()->getManager();
      $choices = $em->getRepository($filtro['class'])->findAll();
      //$choices = $em->createQuery( 'SELECT t FROM ' . $filtro['class'] .' t' )->getResult();

      if (isset($filtro['group_by'])) {
        $tmp = array();
        foreach ($choices as $choice) {
          $key = $choice->$filtro['group_by']();
          $tmp[$key][] = $choice;
        }
        $filtro['choices'] = $tmp;
      } else {
        $filtro['choices'] = $choices;
      }
    }

    return $this->render('AppBundle:Default:filtrosTabla.html.twig', array(
                'col' => $col,
                'filtro' => $filtro,
                'entity' => $entity
    ));
  }

  public function generateResponse($data) {
    $response = $this->render($this->bundle . ':' . $this->controller . ':' . $this->action . '.html.twig', $data);
    
    return $response;
  }

  public function getRequestParameters($class) {
    $params = Util::getForwardParameters($this->getRequest(), $class);

    $this->entityFolder = $params['folder'] . '\\' . $params['bundle'] . '\\Entity\\';
    $this->entityClass = $params['folder'] . '\\' . $params['bundle'] . '\\Entity\\' . $class;
    $this->formClass = $params['folder'] . '\\' . $params['bundle'] . '\\Form\\' . $class . 'Type';
    $this->bundle = $params['folder'] . $params['bundle'];
    $this->controller = $params['controller'];
    $this->action = $params['action'];
    $this->entity = $params['entity'];
    $this->responseType = $params['responseType'];

    if ($this->entity) {
      $this->em = $this->getDoctrine()->getManager();
      $this->rep = $this->em->getRepository($this->bundle . ':' . $this->entity);
    }

  }

  public function logDelete($sql, $result) {
    $url = str_replace(array('http://', 'https://', $this->getRequest()->server->get('HTTP_HOST')), array('', '', ''), $this->getRequest()->server->get('HTTP_REFERER'));

    $log = new \CommonBundle\AdministratorBundle\Entity\LogDelete();
    $log->setUsuario($this->getUser()->getId());
    $log->setUrl($url);
    $log->setQuery($sql);
    $log->setNumRegistros($result);

    $this->em->persist($log);
    $this->em->flush();
  }

  public function objectRelCreationAction(Request $request) {
    
    //Fix translation error in non default-locale
    $translatable = $this->get('stof_doctrine_extensions.listener.translatable');
    $translatable->setTranslatableLocale($translatable->getDefaultLocale());

    $className = $request->get('entityClass');
    $class = Util::getEntityClassName($className);
    $entityClass = Util::getEntityClass($className);
    $this->entityClass = $className;
    //$this->entityClass = $entityClass;

    $this->em = $this->getDoctrine()->getManager();
    $this->rep = $this->em->getRepository($entityClass);
    $generator = $this->rep->getGenerator('new');
    
    $entity = new $className();

    $this->translatables = $this->getTranslatableFieldsClass($entity,'new');
    $generator['translatables'] = $this->translatables;


    $form = $this->createCreateForm($entity, Util::getEntityClassName($className), 'app_bundle_obj_relation', $generator);
    $form->handleRequest($request);

    if ($form->isValid()) {
      $entity = $this->proccessForm($generator['formRelations'], $entity, $form, true);

      $resp = array('status' => 'OK', 'entity' => array('id' => $entity->getId(), 'text' => $entity->__toString()));
    } else {
      $resp = array('status' => 'KO', 'html' => $this->renderView('AppBundle:Default:objectRel.html.twig', array(
              'form' => $form->createView(),
              'class' => $class,
              'generator' => $generator,
              'titulo' => $this->rep->title_new,
              'entityClass' => $className)));
    }

    $response = new Response(json_encode($resp));
    $response->headers->set('Content-Type', 'application/json');
    return $response;
  }

  public function select2ajaxAction(Request $request) {
    $datos = array();
    $class = $request->get('class');
    $fields = $request->get('fields');
    $rels = $request->get('rels', array());
    $term = trim($request->get('q'));
    $fnToString = $request->get('fnToString', '');

    $em = $this->getDoctrine()->getManager();
    $rep = $em->getRepository($class);

    $q = $rep->createQueryBuilder('t');
    foreach($rels as $rel => $alias){
      if (strpos($rel, '.') !== false) {
        $q->leftJoin($rel, $alias);
      } else {
        $q->leftJoin('t.'.$rel, $alias);
      }
    }
    
    //$orX = $q->expr()->orX();
    $fieldConcat = '';
    $fieldTmp = '';
    foreach ($fields as $field) {
      if(strpos($field, '.') === false){
        $field = 't.'.$field;
      }
      if ($fieldTmp === '') $fieldTmp = $field;
      else {
        $fieldConcat = 'UPPER(CONCAT(COALESCE(' . $fieldTmp . ', \'\'), \' \', COALESCE(' . $field . ', \'\')))';
        $fieldTmp = $fieldConcat;
      }
      //$orX->add("{$field} LIKE '%{$term}%'");
    }
    if ($fieldConcat !== '') $fieldConcat .= ' LIKE UPPER(\'%' . ($term) . '%\')';
    $objs = $q->andWhere($fieldConcat)
            ->setMaxResults(50)
            ->getQuery()
            ->getResult();

    $texts = [];
    foreach ($objs as $obj) {
      $text = ($fnToString ? $obj->$fnToString() : $obj->__toString());
      if (!in_array($text, $texts)) {
        $texts[] = $text;
        $datos[] = array('id' => $obj->getId(), 'text' => $text);
      }
    }

    return new Response(json_encode($datos));
  }

  public function exportExcelAction(Request $request) {
    if ($request->isXmlHttpRequest()) {
      $em = $this->getDoctrine()->getManager();
      $params = $request->request->all();
      $params['filtros'] = $request->query->all();
      $locale = $request->getLocale();
      $router = $this->get('router');
      $trans = $this->get('translator');

      $dte = new DataTableEntity($params, $em, $locale, $router, $trans);
      $dte->setExcel(true);
      $datos = $dte->getData();

      $request->getSession()->set('tmpExcel', $datos);

      $response = new Response('OK');
    } else {
      $tabla = $request->get('tabla');
      $datos = $request->getSession()->get('tmpExcel');
      $request->getSession()->remove('tmpExcel');

      // create an empty object
      $phpExcelObject = $this->createXSLObject($datos, $tabla);
      // create the writer
      $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
      // create the response
      $response = $this->get('phpexcel')->createStreamedResponse($writer);
      // adding headers
      $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
      $response->headers->set('Content-Disposition', 'attachment;filename=' . $tabla . '_' . date('YmdHis') . '.xls');
      $response->headers->set('Pragma', 'public');
      $response->headers->set('Cache-Control', 'maxage=1');
    }

    return $response;
  }

  private function createXSLObject($datos, $tabla) {
    $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

    $phpExcelObject->getProperties()
            ->setTitle($tabla)
            ->setSubject($tabla)
            ->setDescription($tabla);
    $phpExcelObject->setActiveSheetIndex(0);

    $head = $datos['head'];
    for ($iRow = 0; $iRow < count($datos) - 1; $iRow++) {
      for ($iCol = 1; $iCol < $datos['numCols']; $iCol++) {
        $cell = Util::letraAbecedario($iCol - 1) . ($iRow + 1);

        if ($iRow == 0) {
          $cellValue = $head[$iCol];
        } else {
          $cellValue = $datos[$iRow - 1][$iCol];
        }
        $phpExcelObject->getActiveSheet()->setCellValue($cell, strip_tags($cellValue));

        if ($iRow == 0) {
          $phpExcelObject->getActiveSheet()->getStyle($cell)->getFont()->setBold(true);
        } else {
          if (strpos($cellValue, "\n") !== FALSE) {
            $phpExcelObject->getActiveSheet()->getStyle($cell)->getAlignment()->setWrapText(true);
          }
        }
      }
    }
    $phpExcelObject->getActiveSheet()->setTitle($tabla);
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $phpExcelObject->setActiveSheetIndex(0);

    return $phpExcelObject;
  }

  public function editNmRelationAction(Request $request) {
    $datos = $request->query->all();
    $em = $this->getDoctrine()->getManager();
    $rep = $em->getRepository($datos['relClass']);
    $formClass = Util::getFormClass($rep->getClassName());
    $ids = $rep->getClass()->identifier;
    $rels = $rep->getAssociatedMapping();
    $nmRel = Util::getParamsNmRelations($rels, $ids, $datos['id'], $datos['originId'], $datos['refClass'], true);
    
    $repOri = $em->getRepository($rels[$nmRel['originCol']]['targetEntity']);
    $repRef = $em->getRepository($rels[$nmRel['refCol']]['targetEntity']);
    
    $refObj[$nmRel['originCol']] = $repOri->find($nmRel['params'][$nmRel['originCol']]);
    $refObj[$nmRel['refCol']] = $repRef->find($nmRel['params'][$nmRel['refCol']]);
    $cabeceraOrigin = $repOri->getHeadData($refObj[$nmRel['originCol']], $request->getLocale(), $this->get('router'), $this->get('translator'));
    $cabecera = $repRef->getHeadData($refObj[$nmRel['refCol']], $request->getLocale(), $this->get('router'), $this->get('translator'));
    
    $entity = $rep->findOneBy($nmRel['params']);
    if (!$entity) {
      $entity = $this->createNmRelationObj($em, $rep->getClassName(), $nmRel, $refObj);
    }

    $form = $this->createForm(new $formClass(), $entity, array('action' => $this->generateUrl('app_bundle_edit_nm_rel', $datos), 'attr' => array('id' => 'JSnmRelationForm')));

    $respParam = array_merge(array(
        'refObj' => array(
            $nmRel['originCol'] => $refObj[$nmRel['originCol']]->__toString(),
            $nmRel['refCol'] => $refObj[$nmRel['refCol']]->__toString()
        ),
        'cabecera' => $cabecera,
        'cabeceraOrigin' => $cabeceraOrigin,
        'ids' => implode('_', $ids),
        'form' => $form->createView(),
        'titulo' => $rep->title_new), $datos);

    if ($request->isMethod('POST')) {
      $form->handleRequest($request);
      if ($form->isValid()) {
        $em->persist($entity);
        $em->flush();
        $data = array();
        
        foreach($rep->getExtraFields() as $col){
          $data[$col] = $rep->renderRowCol($entity, $col, $rep->getColData($col), $request->getLocale(), $this->get('router'), $this->get('translator'));
        }

        $resp = array('status' => 'OK', 'entity' => $data);
      } else {
        $resp = array('status' => 'KO', 'html' => $this->renderView('AppBundle:Default:editNmRelation.html.twig', $respParam));
      }

      $response = new Response(json_encode($resp));
      $response->headers->set('Content-Type', 'application/json');
    } else {
      $response = $this->render('AppBundle:Default:editNmRelation.html.twig', $respParam);
    }

    return $response;
  }

  public function saveNmRelationAction(Request $request){
    $refClass = $request->get('refClass');
    $relClass = $request->get('relClass');
    $checks = json_decode($request->get('checks', array()), true);
    
    $em = $this->getDoctrine()->getManager();
    $rep = $em->getRepository($relClass);
    $ids = $rep->getClass()->identifier;
    $rels = $rep->getAssociatedMapping();
    
    foreach($checks as $check){
      $nmRel = Util::getParamsNmRelations($rels, $ids, $check['id'], $check['originId'], $refClass, true);
      $entity = $rep->findOneBy($nmRel['params']);
      
      if($entity && !$check['checked']){
        $em->remove($entity);
        $em->flush();
      }
      if(!$entity && $check['checked']){
        $refObj[$nmRel['originCol']] = $em->getRepository($rels[$nmRel['originCol']]['targetEntity'])->find($nmRel['params'][$nmRel['originCol']]);
        $refObj[$nmRel['refCol']] = $em->getRepository($rels[$nmRel['refCol']]['targetEntity'])->find($nmRel['params'][$nmRel['refCol']]);
        $this->createNmRelationObj($em, $rep->getClassName(), $nmRel, $refObj);
      }
    }
    
    die;
  }
  
  protected function createNmRelationObj($em, $cName, $nmRel, $refObj){
    $entity = new $cName();
    
    foreach ($nmRel['params'] as $key => $val) {
      $fn = Util::getObjectFieldMethodName($key, 'set');
      $entity->$fn($refObj[$key]);
    }

    $em->persist($entity);
    $em->flush();
    
    return $entity;
  }

  public function infoNmRelationAction(Request $request){
    $em = $this->getDoctrine()->getManager();
    $rep = $em->getRepository($request->get('nmRel'));

    foreach($rep->getAssociatedMapping() as $relCol => $relData){
      if(Util::getEntityClassName($relData['targetEntity']) === $request->get('originClass')){
        break;
      }
    }

    $n = $rep->createQueryBuilder('t')
      ->select('count(r.id) AS num')
      ->innerJoin('t.' . $relCol, ' r')
      ->where('r.id = ' . $request->get('originId'))
      ->getQuery()->getArrayResult();

    exit($n[0]['num']);
  }

  /**
   * Get all translatable fields (Gedmo)
   */
  public function getTranslatableFields($class, $type){

    $fields = array();
    $convertedObject = new \stdClass;
    $reflectionObject = new \ReflectionObject($class);
        
    foreach ($reflectionObject->getProperties() as $prop) {
        $annotation = $this->get('annotation_reader')->getPropertyAnnotation($prop,'Gedmo\\Mapping\\Annotation\\Translatable');
        if (null !== $annotation) {
            $fields[] = $prop->getName();                              
        }            
    }    

    return $fields;
  }

   /**
   * Get all translatable fields (Gedmo)
   */
  public function getTranslatableFieldsClass($class, $type){

    $fields = array();
    $convertedObject = new \stdClass;
    $reflectionObject = new \ReflectionClass($class);
   
    foreach ($reflectionObject->getProperties() as $prop) {
        $annotation = $this->get('annotation_reader')->getPropertyAnnotation($prop,'Gedmo\\Mapping\\Annotation\\Translatable');
        if (null !== $annotation) {
            $fields[] = $prop->getName();                              
        }            
    }

    return $fields;
  }
  
  /**
   * Set array for a2lix_translations_gedmo
   */
  public function setTranslatableFieldArray($fields, $labels, $type){

    $ret = array();

    foreach ($fields as $value) {
      $field = $value['field'];
      $formtype = $value['type'];
      //$data = $this->rep->getColData($field);
      //if (!empty($data[$type]) && $data[$type]) {
      
      if (isset($labels[$field])){
        $label = $labels[$field];
      } else {
        $label = $field;
      }            
      $ret[$field] = array('label' => $label,'required' => $value['required'],'field_type' => $formtype);      
    }
    
    foreach($this->translatables as $field){
      if (!array_key_exists($field, $ret)){
        $ret[$field] = array('display'=>false);  
      }

    }
    
    return $ret;
    
  }
  
}
