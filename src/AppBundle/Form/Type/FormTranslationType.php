<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use \Symfony\Component\Form\FormView;
use \Symfony\Component\Form\FormInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Form\FormBuilderInterface;

class FormTranslationType extends AbstractType {
  private $local;
  private $locales;

  public function __construct($local, $locales) {
    $this->local = $local;
    $this->locales = $locales;
  }
    
  public function setDefaultOptions(OptionsResolverInterface $resolver) {
    foreach($this->locales as $l){
      $lang[$l] = Intl::getLanguageBundle()->getLanguageName($l);  
    }
    $resolver->setDefaults(array(
        'data' => $lang
    ));
  }

  public function getName() {
    return 'formtranslation';
  }

}
