<?php

namespace AppBundle\Util;

use Doctrine\ORM\Tools\Pagination\Paginator;

class DataTableEntity {
  const SQLSRV2008 = 'Doctrine\DBAL\Platforms\SQLServer2008Platform';
  const SQLSRV2008_LINUX = 'Realestate\MssqlBundle\Platforms\DblibPlatform';

  protected $em;
  protected $rep;
  protected $opts;
  protected $cols;
  protected $locale;
  protected $router;
  protected $translator;
  protected $filters;
  protected $sCols = array();
  protected $iTotalRecords = 0;
  protected $iTotalDisplayRecords = 0;
  public $relAlias;
  protected $excel = false;
  protected $platform;

  public function __construct($options, $em, $locale, $router, $trans) {
    $this->opts = $options;
    $this->em = $em;
    $this->locale = $locale;
    $this->rep = $em->getRepository($options['entityClass']);
    $generator = $this->rep->getGenerator('list', false, (isset($options['nmRel']) ? $options['nmRel'] : ''));
    $this->cols = $generator['cols'];
    $this->filters = $generator['filtros'];
    $this->router = $router;
    $this->translator = $trans;
    if (method_exists($em->getConnection(), 'getDatabasePlatform')) $this->platform = get_class($em->getConnection()->getDatabasePlatform());
    else $this->platform = "";

    foreach ($this->cols as $i => $col) {
      if (isset($options['bSearchable_' . $i]) && $options['bSearchable_' . $i] === 'true') {
        $this->sCols[] = $col;
      }
    }
    
    if(isset($this->opts['originId']) && $this->opts['originId']){
      $this->rep->setOrigin($this->opts['originId'], $this->opts['originClass']);
    }
  }
  
  public function setExcel($excel){
    $this->excel = $excel;
  }

  public function getData() {
    $this->iRel = 0;
    $this->alias = 't';
    $this->q = $this->rep->createQueryBuilder($this->alias);
    $this->relations = $this->rep->getAssociatedMapping();
    $this->nmRelations = $this->rep->nmRelations;
    $this->deepRelations = $this->rep->deepRelations;
    $this->relAlias = array();

    foreach($this->relations as $rel => $relData){
      if(!in_array($rel, $this->rep->excludeRelations)){
        $this->q->leftJoin($this->alias.'.'.$rel, $rel);
        $this->relAlias[$relData['targetEntity']] = $rel;
        
        if(isset($this->nmRelations[$relData['targetEntity']])){
          if(in_array('nm_'.$this->nmRelations[$relData['targetEntity']]['alias'], $this->relAlias)){
            $this->q->leftJoin($rel.'.'.$this->nmRelations[$relData['targetEntity']]['alias'], 'nm_'.$this->nmRelations[$relData['targetEntity']]['alias'].(++$this->iRel));
            $this->relAlias[Util::getEntityClass($this->nmRelations[$relData['targetEntity']]['relclass'])] = 'nm_'.$this->nmRelations[$relData['targetEntity']]['alias'].($this->iRel);
          }else{
            $this->q->leftJoin($rel.'.'.$this->nmRelations[$relData['targetEntity']]['alias'], 'nm_'.$this->nmRelations[$relData['targetEntity']]['alias']);
            $this->relAlias[$this->nmRelations[$relData['targetEntity']]['class']] = 'nm_'.$this->nmRelations[$relData['targetEntity']]['alias'];
          }
        }
      }
    }

    foreach($this->deepRelations as $rel => $relData){
      $this->q->leftJoin($relData['rel'] . '.' . $rel, $rel);
      $this->relAlias[$relData['targetEntity']] = $rel;
    }

    if(isset($this->opts['originId']) && $this->opts['originId']){
      $nmRel = $this->rep->getNmRelation($this->opts['originClass']);
      if($nmRel && isset($nmRel['relclass'])){
        if(!isset($this->opts['nmRel']) || !$this->opts['nmRel']){
          $this->q->innerJoin($this->alias.'.'.lcfirst($this->opts['originClass']), 'origen_'.$this->opts['originClass'])
            ->innerJoin('origen_'.$this->opts['originClass'].'.'.lcfirst($this->opts['originClass']), 'reforigen_'.$this->opts['originClass'])
            ->andWhere('reforigen_'.$this->opts['originClass'].'.id = '.$this->opts['originId']);
        }elseif(isset($this->opts['nmRelFiltro'])){
          if($this->opts['nmRelFiltro'] === 'asigned'){
            if(isset($this->relAlias[$this->opts['nmRel']])){
              $this->q->andWhere($this->relAlias[$this->opts['nmRel']].'.id = '.$this->opts['originId']);
            }elseif(in_array('nm_'.lcfirst($this->opts['originClass']), $this->relAlias)){
              $this->q->andWhere('nm_'.lcfirst($this->opts['originClass']).'.id = '.$this->opts['originId']);
            }else{
              $this->q->innerJoin($this->alias.'.'.lcfirst($this->opts['originClass']), 'origen_'.$this->opts['originClass'])
                ->innerJoin('origen_'.$this->opts['originClass'].'.'.lcfirst($this->opts['originClass']), 'reforigen_'.$this->opts['originClass'])
                ->andWhere('reforigen_'.$this->opts['originClass'].'.id = '.$this->opts['originId']);
            }
          }elseif($this->opts['nmRelFiltro'] === 'unasigned'){
            $ids = array();
            
            $nots = $this->rep->createQueryBuilder($this->alias)->select($this->alias.'.id')
              ->innerJoin($this->alias.'.'.$this->rep->getNmRelAlias(lcfirst($this->opts['originClass'])), 'origen_'.$this->opts['originClass'])
              ->innerJoin('origen_'.$this->opts['originClass'].'.'.lcfirst($this->opts['originClass']), 'reforigen_'.$this->opts['originClass'])
              ->andWhere('reforigen_'.$this->opts['originClass'].'.id = '.$this->opts['originId'])
              ->getQuery()
              ->getResult();
            
            foreach($nots as $not){
              $ids[] = $not['id'];
            }

            if($ids){
              $this->q->andWhere($this->q->expr()->notIn($this->alias.'.id', $ids));
            }
          }
        }
      }else{
        $this->q->innerJoin($this->alias.'.'.lcfirst($this->opts['originClass']), 'origen_'.$this->opts['originClass'])
          ->andWhere('origen_'.$this->opts['originClass'].'.id = '.$this->opts['originId']);
      }
    }
    
    //Calcular número de resultados totales antes de cualquier filtro
    if ($this->platform != "") {
      $paginator = new Paginator($this->q);
      $this->iTotalRecords = $paginator->count();
    } else {
      $this->iTotalRecords = $this->q->getQuery()->execute()->count();
    }

    //Filtro para el campo de busqueda del dataTable
    $this->q = $this->addSearchFilter();
    
    //Filtros generales
    $this->q = $this->addGralFilter();

    //Calcular número de resultados totales despues de filtrar
    if ($this->platform != "") {
      $paginator = new Paginator($this->q);
      $this->iTotalDisplayRecords = $paginator->count();
    } else {
      $this->iTotalDisplayRecords = $this->q->getQuery()->execute()->count();
    }

    if(!$this->excel && $this->platform != ""){
      if($this->opts['iDisplayLength'] > 0){
        $this->q->setMaxResults( $this->opts['iDisplayLength'] );
      }

      $this->q->setFirstResult( $this->opts['iDisplayStart'] );
    } else if (!$this->excel){
      $this->q->limit( $this->opts['iDisplayLength'] );
      $this->q->skip( $this->opts['iDisplayStart'] );
    }
    
    if ($this->platform != "") {
      for($i = 0; $i < $this->opts['iSortingCols']; $i++){
        $sortCol = $this->cols[$this->opts['iSortCol_'.$i]];
        if(strpos($sortCol, '__') === false){
          $this->q->addOrderBy($this->alias.'.'.$sortCol, $this->opts['sSortDir_'.$i]);
        }else{
          $aux = explode('__', $sortCol);
          if(count($aux) === 2){
            $this->q->addOrderBy($aux[0].'.'.$aux[1], $this->opts['sSortDir_'.$i]);
          }elseif (count($aux) === 3) {
            $this->q->addOrderBy($aux[1].'.'.$aux[2], $this->opts['sSortDir_'.$i]);
          }
        }
      }
    } else {
      for($i = 0; $i < $this->opts['iSortingCols']; $i++){
        $sortCol = $this->cols[$this->opts['iSortCol_'.$i]];
        $this->q->sort(array($this->alias.'.'.$sortCol => $this->opts['sSortDir_'.$i]));
      }
    }
    
    if ($this->platform != "")
    if ($this->platform === self::SQLSRV2008){
      $subQ = clone $this->q;
      $subQ->groupBy($this->alias.'.id');
      $subQ->select($this->alias.'.id');
      
      $this->q = $this->rep->createQueryBuilder('subqt');
      $this->q->andWhere($this->q->expr()->in('subqt.id',$subQ->getDQL()));

      $this->alias = 'subqt';

      foreach($this->relations as $rel => $relData){
        if($relData['type'] === 2){
          $this->q->leftJoin($this->alias.'.'.$rel, 'subqt_'.$rel);
          $this->relAlias[$relData['targetEntity']] = 'subqt_'.$rel;
        }
      }
    
      for($i = 0; $i < $this->opts['iSortingCols']; $i++){
        $sortCol = $this->cols[$this->opts['iSortCol_'.$i]];
        if(strpos($sortCol, '__') === false){
          $this->q->addOrderBy($this->alias.'.'.$sortCol, $this->opts['sSortDir_'.$i]);
        }else{
          $aux = explode('__', $sortCol);
          if(count($aux) === 2){
            $this->q->addOrderBy('subqt_'.$aux[0].'.'.$aux[1], $this->opts['sSortDir_'.$i]);
          }elseif (count($aux) === 3) {
            $this->q->addOrderBy('subqt_'.$aux[1].'.'.$aux[2], $this->opts['sSortDir_'.$i]);
          }
        }
      }
    }else{
      foreach($this->rep->getClass()->identifier as $idCol){
        $this->q->groupBy($this->alias.'.'.$idCol);
      }
    
      for($i = 0; $i < $this->opts['iSortingCols']; $i++){
        $sortCol = $this->cols[$this->opts['iSortCol_'.$i]];
        if(strpos($sortCol, '__') === false){
          $this->q->addOrderBy($this->alias.'.'.$sortCol, $this->opts['sSortDir_'.$i]);
        }else{
          $aux = explode('__', $sortCol);
          if(count($aux) === 2){
            $this->q->addOrderBy($aux[0].'.'.$aux[1], $this->opts['sSortDir_'.$i]);
          }elseif (count($aux) === 3) {
            $this->q->addOrderBy($aux[1].'.'.$aux[2], $this->opts['sSortDir_'.$i]);
          }
        }
      }
    }

    if ($this->platform != "")
    if (!$this->excel){
      if($this->opts['iDisplayLength'] > 0){
        $this->q->setMaxResults( $this->opts['iDisplayLength'] );
      }

      $this->q->setFirstResult( $this->opts['iDisplayStart'] );
    }

    //echo $this->q->getQuery()->getSql();

    if ($this->platform != "") {
      $rows = $this->q->getQuery()->getResult();
    } else {
      $rows_dev = $this->q->getQuery()->execute()->toArray();
      $num = 0;
      $rows = array();
      foreach($rows_dev as $row) {
        $rows[$num] = $row;
        ++$num;
      }
    }
    
    //Transformar los datos para el dataTable
    $data = $this->rep->rowsToRender($rows, $this->locale, $this->router, $this->translator, $this->excel);
    
    if($this->excel){
      return $data;
    }

    $datos = array(
        'sEcho' => $this->opts['sEcho'],
        'iTotalRecords' => $this->iTotalRecords,
        'iTotalDisplayRecords' => $this->iTotalDisplayRecords,
        'aaData' => $data
    );
    
    if(isset($this->opts['originId']) && $this->opts['originId']){
      $datos['originId'] = $this->opts['originId'];
      $datos['originClass'] = $this->opts['originClass'];
    }
    
    return $datos;
  }

  protected function addSearchFilter(){
    if (trim($this->opts['sSearch']) && $this->sCols) {
      if ($this->platform != "") $orX = $this->q->expr()->orX();
      else $qb = $this->q;
      foreach ($this->sCols as $scol) {
        if($this->rep->inFieldMapping($scol)){
          if ($this->platform != "") $orX->add($this->alias.'.'.$scol.' LIKE \'%'.trim($this->opts['sSearch']).'%\'');
          else $this->q->addOr($qb->expr()->field($scol)->equals(new \MongoRegex('/^' . trim($this->opts['sSearch'] . '/'))));
        }elseif(isset($this->relations[$scol])){
          foreach($this->em->getClassMetadata($this->relations[$scol]['targetEntity'])->fieldMappings as $relField => $relFieldData){
            if($relFieldData['type'] === 'string'){
              if ($this->platform != "") $orX->add($this->relAlias[$this->relations[$scol]['targetEntity']].'.'.$relField.' LIKE \'%'.trim($this->opts['sSearch']).'%\'');
              else $this->q->addOr($qb->expr()->field($relField)->equals(new \MongoRegex('/^' . trim($this->opts['sSearch'] . '/'))));
            }
          }
          if(isset($this->nmRelations[$this->relations[$scol]['targetEntity']])){
            foreach($this->em->getClassMetadata($this->nmRelations[$this->relations[$scol]['targetEntity']]['class'])->fieldMappings as $relField => $relFieldData){
              if($relFieldData['type'] === 'string'){
                if ($this->platform != "") $orX->add($this->relAlias[$this->nmRelations[$this->relations[$scol]['targetEntity']]['class']].'.'.$relField.' LIKE \'%'.trim($this->opts['sSearch']).'%\'');
                else $this->q->addOr($qb->expr()->field($relField)->equals(new \MongoRegex('/^' . trim($this->opts['sSearch'] . '/'))));
              }
            }
          }
        }elseif(strpos($scol, '__') !== false){
          $aux = explode('__', $scol);
          if(count($aux) === 2){
             if ($this->platform != "") $orX->add($aux[0].'.'.$aux[1].' LIKE \'%'.trim($this->opts['sSearch']).'%\'');
             else $this->q->addOr($qb->expr()->field($aux[1])->equals(new \MongoRegex('/^' . trim($this->opts['sSearch'] . '/'))));
          }elseif (count($aux) === 3) {
             if ($this->platform != "") $orX->add($aux[1].'.'.$aux[2].' LIKE \'%'.trim($this->opts['sSearch']).'%\'');
             else $this->q->addOr($qb->expr()->field($aux[2])->equals(new \MongoRegex('/^' . trim($this->opts['sSearch'] . '/'))));
          }
        }
      }

      if ($this->platform != "") $this->q->andWhere($orX);
    }
    
    return $this->q;
  }
  
  protected function addGralFilter(){
    foreach($this->filters as $group => $filtros){
      foreach($filtros as $col => $filter){
        if(isset($this->opts['filtros'][$col])){
          $f = $this->opts['filtros'][$col];
          $column = $this->alias.'.'.$col;

          if(isset($filter['nmRelation']) && $filter['nmRelation']){
            $column = $this->relAlias[$filter['nmRelation']].'.id';
          }
          if(isset($filter['target']) && isset($filter['target']['class'])){
            $column = $this->relAlias[$filter['target']['class']].'.'.$filter['target']['field'];
          }
          if(strpos($col, '__') !== false){
            $aux = explode('__', $col);
            if(count($aux) === 2){
              $column = $aux[0].'.'.$aux[1];
            }elseif (count($aux) === 3) {
              $column = $aux[1].'.'.$aux[2];
            }
          }

          $this->addFilterCondition($column, $f, $filter);
        }
      }
    }
    
    return $this->q;
  }
  
  private function addFilterCondition($column, $f, $filter){
    if(isset($filter['fn']) && method_exists($this->rep, $filter['fn'])){
      $fn = $filter['fn'];
      $this->q = $this->rep->$fn($this->q, $column, $f);
    } else if ($this->platform != "") {
      switch ($filter['type']) {
        case 'choice':
          if(is_numeric($f)){
            $this->q->andWhere("{$column} = {$f}");
          }elseif($f){
            $this->q->andWhere("{$column} = '{$f}'");
          }
          break;
        case 'date':
          $nf = strtotime($f);
          if($nf > 0){
            $f = date('Y-m-d', $nf);
            $this->q->andWhere("{$column} = '{$f}'");
          }
          break;
        case 'date_range':
          $nfd = strtotime($f['desde']);
          $nfh = strtotime($f['hasta']);

          if($nfd > 0 && $nfh > 0){
            $fd = date('Y-m-d', $nfd);
            $fh = date('Y-m-d', $nfh);
            $this->q->andWhere("{$column} BETWEEN '{$fd}' AND '{$fh}'");
          }elseif ($nfd > 0) {
            $fd = date('Y-m-d', $nfd);
            $this->q->andWhere("{$column} >= '{$fd}'");
          }elseif ($nfh > 0) {
            $fh = date('Y-m-d', $nfh);
            $this->q->andWhere("{$column} <= '{$fh}'");
          }
          break;
        case 'entity':
          if($f){
            $this->q->andWhere("{$column} = {$f}");
          }
          break;
        case 'numeric':
          $op = $f['operator'];
          $val = $f['value'];
          if(is_numeric($val)){
            $this->q->andWhere("{$column} {$op} {$val}");
          }
          break;
        default:
          if($f){
            $this->q->andWhere("{$column} LIKE '%{$f}%'");
          }
      }
    } else {
      $column = explode(".", $column);
      $column = $column[1];
      switch ($filter['type']) {
        case 'choice':
          $this->q->field("{$column}")->equals("{$f}");
          break;
        case 'date':
          $nf = strtotime($f);
          if($nf > 0){
            //MongoTimestamp Object ( [sec] => 1444052486 [inc] => 0 )
            $date = new \MongoTimestamp($nf, 0);
            $this->q->field("{$column}")->equals($date);
          }
          break;
        case 'date_range':
          $nfd = strtotime($f['desde']);
          $nfh = strtotime($f['hasta']);

          if($nfd > 0 && $nfh > 0){
            $fd = date('Y-m-d', $nfd);
            $fh = date('Y-m-d', $nfh);
            $this->q->field("{$column}")->gte("{$fd}");
            $this->q->field("{$column}")->lte("{$fh}");
          }elseif ($nfd > 0) {
            $fd = date('Y-m-d', $nfd);
            $this->q->field("{$column}")->gte("{$fd}");
          }elseif ($nfh > 0) {
            $fh = date('Y-m-d', $nfh);
            $this->q->field("{$column}")->lte("{$fh}");
          }
          break;
        case 'entity':
          if($f){
            $this->q->field("{$column}")->equals("{$f}");
          }
          break;
        case 'numeric':
          $op = $f['operator'];
          $val = $f['value'];
          if(is_numeric($val)){
            $this->q->field("{$column}")->equals("{$f}");
          }
          break;
        default:
          if($f){
            $this->q->field("{$column}")->equals("{$f}");
          }
      }
    }
  }

  private function isSqlServer(){
    return ($this->platform === self::SQLSRV2008 || $this->platform === self::SQLSRV2008_LINUX);
  }
}
