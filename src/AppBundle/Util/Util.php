<?php

namespace AppBundle\Util;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Intl\Intl;
use Doctrine\Common\Util\Inflector;

class Util {

  protected $em;
  public static $globalController = 'AppBundle:Default';

  public function __construct(EntityManager $entityManager) {
    $this->em = $entityManager;
  }

  public static function collectionToArray($collection, $column, $key = 'id') {
    $resp = array();

    foreach ($collection as $i => $obj) {
      $k = $key ? $obj[$key] : $i;
      $v = $column ? $obj[$column] : $obj;

      $resp[$k] = $v;
    }

    return $resp;
  }

  public static function semaphore($value, $width = 16) {
    $img = 'red.png';
    $alt = 'No';

    if ($value) {
      $img = 'green.png';
      $alt = 'Si';
    }

    return '<img src="/images/' . $img . '" width="' . $width . '" alt="' . $alt . '"/>';
  }

  public static function getDatePickerFormat($locale = 'es'){
    $formats = array(
        'es' => 'dd-MM-yyyy',
        'en' => 'yyyy-MM-dd',
        'eu' => 'yyyy-MM-dd',
        'fr' => 'yyyy-MM-dd'
    );
    
    if(isset($formats[$locale])){
      return $formats[$locale];
    }
    
    return $formats['es'];
  }
  
  public static function localizedDateTime($date, $dateType = 'long', $timeType = 'long', $locale = 'es') {
    $formatedDate = '';
    $dateformats = array(
        'short' => 'd-m-y',
        'long' => 'd-m-Y'
    );
    $timeFormats = array(
        'short' => 'H:i',
        'long' => 'H:i:s'
    );
    
    if (in_array($locale, array('en', 'eu', 'fr'))) {
      $dateformats = array(
        'short' => 'y-m-d',
        'long' => 'Y-m-d'
      );
    }
    
    if($date){
      if(is_string($date)){
        if(isset($dateformats[$dateType])) $formatedDate .= date($dateformats[$dateType], strtotime($date));
        if(isset($timeFormats[$timeType])) $formatedDate .= ' '.date($timeFormats[$timeType], strtotime($date));
      }else{
        if(isset($dateformats[$dateType])) $formatedDate .= $date->format($dateformats[$dateType]);
        if(isset($timeFormats[$timeType])) $formatedDate .= ' '.$date->format($timeFormats[$timeType]);
      }
    }
    
    return $formatedDate;
  }

  public static function getCountries(){
    return Intl::getRegionBundle()->getCountryNames();  
  }
  
  public static function getLocaleName($locale) {
    $lang = Intl::getLanguageBundle()->getLanguageName($locale);

    if ($lang) {
      $lang = ucfirst($lang);
    } else {
      $lang = $locale;
    }

    return $lang;
  }

  public static function getCountryName($country) {
    if(strlen($country) === 3){
      require 'iso3.php';
      require 'iso2.php';
      
      if(isset($iso_array[$country])){
        foreach($countries as $key => $c){
          if(strtolower($iso_array[$country]) == strtolower($c)){
            $country = $key;
            break;
          }
        }
      }
    }
    
    $lang = Intl::getRegionBundle()->getCountryName(strtoupper($country));

    if ($lang) {
      $lang = ucfirst($lang);
    } else {
      $lang = $country;
    }

    return $lang;
  }

  public static function renderTree($tree, $i, $router, $nav = true, $tmp = array(), $locale ='es') {
    $menu = '';
    if (is_array($tree))

      foreach ($tree as $id => $t) {
        //print_r($t['data']);
        if ($locale == 'es'){
          $nombre =  $t['data']['nombre'];
        } else {

          $nombre =  $t['data']['nombre'];
          if (isset($t['data']['translations'])){
            if (count($t['data']['translations'])>0){              
              foreach ($t['data']['translations'] as $trad) {
                if ($trad['locale'] == $locale){
                  $nombre =  $trad['content'];
                  break;
                }
              }              
            }
          }
        }

        if(!$t['data']['parentId']){
          $i = 0;
        }
        if (count($t['children'])) {
          if($nav){
            $menu .= '<li class="has-dropdown"><a href="#" data-id="' . $id . '" data-level="' . $i . '">' . $nombre . '</a>';
            $menu .= '<ul class="dropdown">';
            $menu .= self::renderTree($t['children'], $i + 1, $router, $nav, $tmp, $locale);
            $menu .= '</ul></li>';
          }else{
            $tmp[] = array('id' => $id, 'nombre' => str_repeat("&nbsp;", $i * 4) . '<span class="menuLevel level' . $i . '">' . $nombre . '</span>', 'admin' => $t['data']['admin']);
            $tmp = self::renderTree($t['children'], $i + 1, $router, $nav, $tmp, $locale);
          }
        } else {
          if($nav){
            $ruta = '#';
            if($t['data']['ruta']){
              $ruta = $router->generate($t['data']['ruta']);
            }
            $menu .= '<li><a href="' . $ruta . '" data-id="' . $id . '" data-level="' . $i . '">' . $nombre . '</a></li>';
          }else{
            $tmp[] = array('id' => $id, 'nombre' => str_repeat("&nbsp;", $i * 4) . '<span class="menuLevel level' . $i . '">' . $nombre . '</span>', 'admin' => $t['data']['admin']);
          }
        }
      }

    if($nav){
      return $menu;
    }else{
      return $tmp;
    }
  }
  
  /**
   * Devuelve los parametros para la redirección al controlador global
   * @param Request $request
   * @param string $return ('' : nada, 'view': el template, 'json': el json con los datos)
   * @param string $entity (nombre de la entidad)
   * @return array
   */
  public static function getForwardParameters($request, $entity){
    $params = self::getControllerInfo($request->attributes->get('_controller'), true); // array('folder', 'bundle', 'controller', 'action')
    $params['get'] = $request->query->all(); // parámetros GET
    $params['post'] = $request->request->all(); // parámetros POST
    $params['entity'] = $entity;
    $params['responseType'] = $request->query->get('responseType', 'json');
    
    return $params;
  }
  
  public static function getControllerInfo($_controller, $bundle_complete = false){
    $action = str_replace(array(':','Action'), array('',''), strrchr($_controller, ":"));
    $folders = explode('\\', str_replace(':', '\\',substr($_controller, 0, strrpos($_controller, ":"))));
    $i_controller = array_search('Controller', $folders);
    
    if($i_controller){
      $controller = $folders[$i_controller + 1];
    }else{
      $i_controller = count($folders) - 1;
      $controller = $folders[$i_controller];
    }
    
    $controller = str_replace('Controller', '', $controller);
    if($bundle_complete){
      $bundle = $folders[$i_controller - 1];
    }else{
      $bundle = str_replace('Bundle', '', $folders[$i_controller - 1]);
    }
    
    $folder = '';
    if(isset($folders[$i_controller - 2])){
      $folder = $folders[$i_controller - 2];
    }
    
    return array('folder' => $folder, 'bundle' => $bundle, 'controller' => $controller, 'action' => $action);
  }
  
  public static function generateValidUrl($url){
    $url = trim($url);
    
    if(!$url){
      $url = '#';
    }elseif(strpos($url, 'http://') === false && strpos($url, 'https://') === false){
      $url = 'http://'.$url;
    }
    
    return $url;
  }
  
  public static function objectToArray($obj, $keys = array()){
    $row = array();
    $reflectionClass = new \ReflectionClass(get_class($obj));
    
    foreach ($reflectionClass->getProperties() as $property) {
      if(!$keys || in_array($property->getName(), $keys)){
        $property->setAccessible(true);
        $row[$property->getName()] = $property->getValue($obj);
        $property->setAccessible(false);
      }
    }  
    
    return $row;
  }
  
  public static function getEntityClassName($className){
    return substr($className, strrpos($className, '\\') + 1);
  }
  
  public static function getEntityClass($className){
    return str_replace(array('\\', 'Entity'), array('', ':'), $className);
  }
  
  public static function getDocumentClass($className){
    return str_replace(array('\\', 'Document'), array('', ':'), $className);
  }
  
  public static function getFormClass($className){
    return str_replace('\\Document\\', '\\Form\\', str_replace('\\Entity\\', '\\Form\\', $className)) . 'Type';
  }
  
  public static function getObjectFieldMethodName($col, $type = 'get'){
    return $type . Inflector::classify($col);
  }
  
  public static function buildYearChoices($distance = 10){
    $yearsBefore = date('Y', mktime(0, 0, 0, date("m"), date("d"), date("Y") - $distance));
    $yearsAfter = date('Y', mktime(0, 0, 0, date("m"), date("d"), date("Y") + $distance));
    
    return array_combine(range($yearsBefore, $yearsAfter), range($yearsBefore, $yearsAfter));
  }
  
  public static function buildMonthChoices(){
    $months = array();
    
    for($i = 1; $i <= 12; $i++){
      $months[$i] = self::getMonthName($i);  
    }
    
    return $months;
  }
  
  public static function getMonthName($monthNumber){
    $m = '';
    
    if($monthNumber >= 1 && $monthNumber <= 12){
      $fmt = new \IntlDateFormatter(null, null, null, null, null, 'MMMM');
      $m = $fmt->format(mktime(0, 0, 0, $monthNumber, 1, 1970));
    }
    
    return ucfirst($m);
  }
  
  public static function letraAbecedario($i, $up = true){
    $n = ceil($i / 26);
    $letra = '';
    
    if($n > 1){
      $letra .= chr(65 + $n);
      $i = $i - 26;
    }
    
    $letra .= chr(65 + $i);
    
    if($up){
      $letra = strtoupper($letra);
    }
      
    return $letra;
  }
  
  public static function getParamsNmRelations($rels, $ids, $id, $originId, $refClass, $originCol = false){
    $resp = array('originCol' => 0, 'refCol' => 1, 'params' => array());
    
    foreach($ids as $i => $idCol){
      if(self::getEntityClass($rels[$idCol]['targetEntity']) == $refClass){
        $resp['refCol'] = $idCol;
        $resp['params'][$idCol] = $id;
      }else{
        $resp['originCol'] = $idCol;
        $resp['params'][$idCol] = $originId;
      }
    }
    
    if($originCol) return $resp;
        
    return $resp['params'];
  }
  
  public static function clearNumbers($str){
    return str_replace(range(0,9), '', str);
  }

  public static function logAction($em, $entity, $new, $user){
    $logAction = null;

    try{
      $uow = $em->getUnitOfWork();
      $uow->computeChangeSets();
      $changeset = $uow->getEntityChangeSet($entity);

      $action = $new ? 'new' : 'edit';

      $log = new \CommonBundle\AdministratorBundle\Entity\LogAction();
      $log->setUsuario($user);
      $log->setAction($action);

      if(!$new && method_exists($entity, 'getId')){
        $log->setEntityId($entity->getId());
      }

      $log->setEntityClass(get_class($entity));
      $log->setModified($changeset);
      $log->em = $em;
      $log->new = $new;

      $logAction = $log;
    }catch(\Exception $e){
      //echo $e->getMessage();
    }

    return $logAction;
  }

  public static function saveLogAction($logAction, $entity){
    try{
      $em = $logAction->em;

      if($logAction->new && method_exists($entity, 'getId')){
        $logAction->setEntityId($entity->getId());
      }

      $em->persist($logAction);
      $em->flush();
    }catch(\Exception $e){
      //echo $e->getMessage();
    }
  }

  public static function logActionDelete($objs, $rep, $em, $user, $origin = null){
    $cascade = array();
    $associateMapping = $rep->getAssociatedMapping();
 
    foreach($associateMapping as $col => $am){
      if(isset($am['isCascadeRemove']) && $am['isCascadeRemove'] === true){
        $cascade[$col] = $am;
      }
    }

    foreach($objs as $obj){
      $log = new \CommonBundle\AdministratorBundle\Entity\LogAction();
      $log->createLogDeleteObject($user, $obj, $em, $origin);
      $em->persist($log);

      foreach($cascade as $rel => $relData){
        $fn = Util::getObjectFieldMethodName($rel);
        $relObj = $obj->$fn();

        if ($relObj && get_class($relObj) == 'Doctrine\ORM\PersistentCollection') {
            foreach($relObj as $ro){
              if (get_parent_class($ro)!= 'Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation'){
                self::logActionDelete(array($ro), $em->getRepository(self::getEntityClass(get_class($ro))), $em, $user, $obj);
              }
            }
        } elseif($relObj) {
          if (get_parent_class($relObj)!= 'Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation'){
            self::logActionDelete(array($relObj), $em->getRepository(self::getEntityClass(get_class($relObj))), $em, $user, $obj);
          }
        }       

      }

      $em->flush();
    }
  }

  public static function generatePassword($length = 4){
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $password = substr( str_shuffle( $chars ), 0, $length );

    return $password;
  }
}