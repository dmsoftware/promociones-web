<?php

namespace CommonBundle\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Form\CallbackTransformer;
use AppBundle\Entity\DefaultRepository;
use CommonBundle\UserBundle\Entity\Archivo;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArchivoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id')
            ->add('alt')
            ->add('orden')
            ->add('file', 'file', array('data_class' => null));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\UserBundle\Entity\Archivo',
            'validation_groups' => false
        ));
    }

    /*public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['my_options'] = $options['my_options'];
    }*/

    /**
     * @return string
     */
    public function getName()
    {
        return 'archivo';
    }
}
