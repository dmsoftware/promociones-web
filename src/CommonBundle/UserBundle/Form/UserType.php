<?php

namespace CommonBundle\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;

class UserType extends AbstractType
{
    /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */
	protected $em;

  	public function __construct(EntityManager $em)
  	{
     	$this->em = $em;
  	}
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$role_collection = array();
    	$roles = $this->em->getRepository('CommonBundleUserBundle:Role')->findAll();
		foreach($roles as $role) {
			$role_collection[$role->getMachinename()] = $role->getName();
		}
		
        $builder
            ->add('username')
//            ->add('usernameCanonical')
            ->add('email')
//            ->add('emailCanonical')
            ->add('enabled', null, array('required' => false))
//            ->add('salt')
            ->add('password', 'password')
//            ->add('plainPassword')
//            ->add('lastLogin')
//            ->add('confirmationToken')
//            ->add('passwordRequestedAt')
//            ->add('locked')
//            ->add('expired')
//            ->add('expiresAt')
//            ->add('roles')
//            ->add('credentialsExpired')
//            ->add('credentialsExpireAt')
//            ->add('locale')
//            ->add('groups')
//            ->add('menus')
            ->add('tipo', 'choice', array(
                'label' => 'Tipo',
                //'choices' => array('' => 'Usuario', 'ROLE_ADMIN' => 'Administrador'),
				'choices' => $role_collection,
                'attr' => array('class' => 'JSselect2No')
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'commonbundle_userbundle_user';
    }
}
