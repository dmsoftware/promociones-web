<?php

namespace CommonBundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Translatable\Translatable;
use AppBundle\Util\Util;

/**
 * Archivo
 *
 * @ORM\Table(name="Archivo")
 * @ORM\Entity(repositoryClass="CommonBundle\UserBundle\Entity\ArchivoRepository")
 * @Gedmo\TranslationEntity(class="CommonBundle\UserBundle\Entity\ArchivoTranslation")
 * @ORM\HasLifecycleCallbacks
 */
class Archivo
{
	//Max size de cada archivo por defecto = 2MB
	const MAX_FILE_SIZE	= 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="alt", type="string", length=255, nullable=true)
     */
    private $alt;

    /**
     * @var string
     *
     * @ORM\Column(name="orden", type="string")
     */
    private $orden = '0';

    /**
     * @var string
     * @Assert\File(
     * )
     * @ORM\Column(name="file", type="string", length=255, nullable=true)
     */
    private $file;

    /**
     * @ORM\OneToMany(
     *     targetEntity="ArchivoTranslation",
     *  mappedBy="object",
     *  cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /**
    * Constructor
    */
    public function __construct() {
    	$this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {
        $result = '';
        if ($this->getFile()) {
            $result = pathinfo($this->getFile(),PATHINFO_FILENAME);
        }
        else
        {
            $result = ''.$this->getId();
        }
        return $result;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alt
     *
     * @param string $alt
     * @return Accion
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get alt
     *
     * @return string 
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * Set orden
     *
     * @param string $orden
     * @return Accion
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return string 
     */
    public function getOrden()
    {
        return $this->orden;
    }
 
/*************************************************************/
/* file */

    /**
     * Set file
     * @param string $file
     * @return Archivo
     */
    public function setFile($file = null)
    {
        if($file)
        {
            //if($this->file)
                //{$this->unsetFile();}
            $this->file = $file;
        }
        return $this;
    }
    /**
     * Get file
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }
    public function getFullFilePath() {
        return null === $this->file ? null : $this->getUploadRootDir(). pathinfo($this->file,PATHINFO_BASENAME);
    }
    public function unsetFile(){
        
        $this->removeFile();
        $this->file = '';
        return $this;
    }
    /**
    * @ORM\PrePersist()
    * @ORM\PreUpdate()
    */
    public function uploadFile() {
        if (null === $this->file || empty($this->file) || !is_object($this->file)) {
        return;
        }
        $partes_ruta = pathinfo($this->file->getClientOriginalName());
        $this->getFile()->move($this->getUploadRootDir(), $this->toAscii($partes_ruta['filename']).'.'.$partes_ruta['extension']);
        $this->setFile($this->getRelPath() . $this->getId() . "/" . $this->toAscii($partes_ruta['filename']).'.'.$partes_ruta['extension']);
    }
    /**
    * @ORM\PostPersist()
    */
    public function moveFile()
    {
        if (null === $this->file || empty($this->file) || !is_object($this->file)) {
            return;
        }
        if(!is_dir($this->getUploadRootDir())){
            mkdir($this->getUploadRootDir());
        }
        copy($this->getTmpUploadRootDir().$this->file, $this->getFullFilePath());
        unlink($this->getTmpUploadRootDir().$this->file);
    }
    /**
    * @ORM\PreRemove()
    */
    public function removeFile()
    {
		@unlink($this->getFullFilePath());
		@rmdir($this->getUploadRootDir());
    }
    public function getFileThumbnail($w = '150px', $h = 'auto')
    {
        $result = ($this->file) ? '<img class="fileActual" src="/' . $this->file . '" style="width:'.$w.'; height:'.$h.';" />' : 'NO IMAGE';
        return $result;
    }
/* fin file */
/*************************************************************/
    //FOTO
    public function getAbsolutePath() {
        return $this->getRelPath().$this->getId()."/";
    }

    public function getRelPath() {
        return 'uploads/archivos/';
    }

    public function getUploadRootDir() {
    // the absolute directory path where uploaded documents should be saved
        return $this->getTmpUploadRootDir().$this->getId()."/";
    }

    protected function getTmpUploadRootDir() {
    // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getRelPath();
    }

    public function toAscii( $string, $separator = '-' )
    {
        $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
        $special_cases = array( '&' => 'and', "'" => '');
        $string = mb_strtolower( trim( $string ), 'UTF-8' );
        $string = str_replace( array_keys($special_cases), array_values( $special_cases), $string );
        $string = preg_replace( $accents_regex, '$1', htmlentities( $string, ENT_QUOTES, 'UTF-8' ) );
        $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
        $string = preg_replace("/[$separator]+/u", "$separator", $string);
        return $string;
    }
/****************************************************************/

    public function getTranslations() {
        return $this->translations;
    }
    public function addTranslation(ArchivoTranslation $t) {
        if (!$this->translations->contains($t)) {
          $this->translations[] = $t;
          $t->setObject($this);
        }
    }
    /**
    * Remove translations
    *
    * @param \CommonBundle\UserBundle\Entity\ArchivoTranslation $translations
    */
    public function removeTranslation(\CommonBundle\UserBundle\Entity\ArchivoTranslation $translations) {
        $this->translations->removeElement($translations);
    }

}
