<?php

namespace CommonBundle\UserBundle\Entity;

use AppBundle\Entity\DefaultRepository;

/**
 * ArchivoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ArchivoRepository extends DefaultRepository
{
	protected $routePrefix = 'archivo';
	public function __construct($em, \Doctrine\ORM\Mapping\ClassMetadata $class) {
		parent::__construct($em, $class);

		//$excluir = array('new', 'delete', 'deselectAll', 'selectAll', 'excel');
		//$this->excludeGralActions($excluir);
		$this->labels = array_merge($this->labels, array(
			'alt'					=> 'Alt'
			,'file'				=> 'Archivo'
			,'orden'				=> 'Orden'
			,'transtions'		=> 'Traducciones'
		));
		$excluir = array(
			'list' => array('translations'), 
			'new' => array('file','translations'), 
			'edit' => array('file','translations'), 
			'head' => array('file','translations'),
			'excel' => array('translations') 
		);
    	$this->excludeColumnsActions($excluir);
		$orden = array('id','alt','orden','file','translations');
		$this->setColumnsOrder($orden);

		//$fields = $this->getFormFields();
		//unset($fields['promocionAccion']['options']['attr']['data-rel-entity']);
		//$this->cols['promocionAccion']['form'] = $fields['promocionAccion'];


	}
}
