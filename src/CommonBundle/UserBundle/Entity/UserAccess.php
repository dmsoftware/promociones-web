<?php

namespace CommonBundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * UserAccess
 *
 * @ORM\Entity
 * @ORM\Table(name="SFAPP_fos_user_access")
 * @ORM\Entity(repositoryClass="CommonBundle\UserBundle\Entity\UserAccessRepository")
 */
class UserAccess
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=255)
     */
    private $route;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow", type="boolean")
     */
    private $allow;

    /**
  	 * @var \CommonBundle\UserBundle\Entity\Role
  	 *
  	 * @ORM\ManyToOne(targetEntity="CommonBundle\UserBundle\Entity\Role")
  	 * @ORM\JoinColumns({
  	 *   @ORM\JoinColumn(name="role", referencedColumnName="id", onDelete="CASCADE")
  	 * })
  	 */
    private $role;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set route
     *
     * @param string $route
     * @return UserAccess
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route
     *
     * @return string 
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set allow
     *
     * @param boolean $allow
     * @return UserAccess
     */
    public function setAllow($allow)
    {
        $this->allow = $allow;

        return $this;
    }

    /**
     * Get allow
     *
     * @return boolean 
     */
    public function getAllow()
    {
        return $this->allow;
    }

    /**
     * Set role
     *
     * @param integer $role
     * @return UserAccess
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return integer 
     */
    public function getRole()
    {
        return $this->role;
    }
	
	public function renderRoute($params)
	{
		if (str_replace(' ', '', $this->route) === '*') return '<strong>' . strtoupper($params['translator']->trans('AccessAll')) . '</strong> (' . $this->route . ')';
		else return $this->route;
	}
}
