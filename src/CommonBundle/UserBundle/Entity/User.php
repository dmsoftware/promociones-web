<?php

namespace CommonBundle\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use CommonBundle\AdministratorBundle\Entity\Menu;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Util\Util;

/**
 * @ORM\Entity
 * @ORM\Table(name="SFAPP_fos_user")
 * @ORM\Entity(repositoryClass="CommonBundle\UserBundle\Entity\UserRepository")
 */
class User extends BaseUser {

  /**
   * @ORM\Id
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  protected $id;

  /**
   * @ORM\Column(type="string", nullable=true)
   * 
   */
  protected $nombre;

  /**
   * @ORM\Column(type="string", nullable=true)
   */
  protected $apellidos;

  /**
   * @ORM\Column(type="string")
   * 
   */
  protected $username;

  /**
   * @ORM\Column(type="string")
   */
  protected $usernameCanonical;

  /**
   * @ORM\Column(type="string")
   * 
   */
  protected $email;

  /**
   * @ORM\Column(type="string")
   */
  protected $emailCanonical;

  /**
   * @ORM\Column(type="boolean")
   */
  protected $enabled;

  /**
   * The salt to use for hashing
   *
   * @ORM\Column(type="string")
   */
  protected $salt;

  /**
   * Encrypted password. Must be persisted.
   *
   * @ORM\Column(type="string")
   */
  protected $password;

  /**
   * Plain password. Used for model validation. Must not be persisted.
   *
   * @ORM\Column(type="string", nullable=true)
   */
  protected $plainPassword;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   */
  protected $lastLogin;

  /**
   * Random string sent to the user email address in order to verify it
   *
   * @ORM\Column(type="string", nullable=true)
   */
  protected $confirmationToken;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   */
  protected $passwordRequestedAt;

  /**
   * @ORM\Column(type="boolean")
   */
  protected $locked;

  /**
   * @ORM\Column(type="boolean")
   */
  protected $expired;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   */
  protected $expiresAt;

  /**
   * @ORM\Column(type="array")
   */
  protected $roles;

  /**
   * @ORM\Column(type="boolean")
   */
  protected $credentialsExpired;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   */
  protected $credentialsExpireAt;

  /**
   *    
   * @ORM\Column(type="string", nullable=true)
   */
  protected $locale;

  /**
   * @ORM\ManyToMany(targetEntity="CommonBundle\UserBundle\Entity\Group")
   * @ORM\JoinTable(name="SFAPP_fos_user_user_group",
   *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
   *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
   * )
   */
  protected $groups;

  /**
   * @ORM\ManyToMany(targetEntity="CommonBundle\AdministratorBundle\Entity\Menu", inversedBy="users", cascade={"persist", "remove" })
   * @ORM\JoinTable(name="SFAPP_menu_usuario")
   * */
  protected $menus;

  /**
   * @ORM\Column(name="clave_caducada", type="boolean", nullable=true)
   */
  private $claveCaducada;

  /**
   * @ORM\Column(name="fecha_caducidad_clave", type="date", nullable=true)
   */
  private $fechaCaducidadClave;

  /**
   * @ORM\Column(name="session_id", type="string", length=255, nullable=true)
   */
  private $sessionId;

  /**
   * @ORM\Column(name="logout_auto", type="boolean", nullable=true)
   */
  private $logoutAuto;

  public function __construct() {
    parent::__construct();
  }

  public function __toString() {
    $nombre = '';

    if ($this->getId()) {
      $nombre = $this->nombre . ' ' . $this->apellidos . ' (' . $this->username . ')';
    }

    return $nombre;
  }
  
  public function getNombre() {
    return $this->nombre;
  }

  public function setNombre($nombre) {
    $this->nombre = $nombre;
    return $this;
  }

  public function getApellidos() {
    return $this->apellidos;
  }

  public function setApellidos($apellidos) {
    $this->apellidos = $apellidos;
    return $this;
  }

  public function setLastLogin(\DateTime $time) {
    parent::setLastLogin($time);
  }

  public function getLocale() {
    return $this->locale;
  }

  public function setLocale($locale) {
    $this->locale = $locale;
    return $this;
  }

  public function addMenu(\CommonBundle\AdministratorBundle\Entity\Menu $menus) {
    $menus->addUser($this); 
    $this->menus[] = $menus;

    return $this;
  }

  public function removeMenu(\CommonBundle\AdministratorBundle\Entity\Menu $menus) {
    $this->menus->removeElement($menus);
  }

  public function getMenus() {
    return $this->menus;
  }

  public function getMenuIds() {
    $ids = array();

    foreach ($this->getMenus() as $menu) {
      $ids[] = $menu->getId();
    }

    return $ids;
  }

  public function checkAccess($_controller, $em){
    $info = \AppBundle\Util\Util::getControllerInfo($_controller);
    
    $menu = $em->getRepository('CommonBundleAdministratorBundle:Menu')->comprobarMenu($info['bundle'], $info['controller'], $info['action']);
	
    if($menu){
      return in_array($menu->getId(), $this->getMenuIds());
    }else{
      return true;
    }
  }
  
  public function setPassword($password) {
    if($password) {
      parent::setPassword($password);
    }
  }
  
  public function getEnabled(){
    return $this->enabled;
  }

  public function getTipo(){
    $tipo = '';

    if(in_array('ROLE_ADMIN', $this->roles)){
      $tipo = 'ROLE_ADMIN';
    } else {
	  $notFirst = false;
	  foreach($this->roles as $rol) {
		if ($notFirst) $tipo .= ' ';
		$tipo .= $rol;
		$notFirst = true;
	  }
	}

    return $tipo;
  }

  public function setTipo($tipo = ''){
    if($tipo){
      //$this->addRole(array($tipo));
	  $this->setRoles(array($tipo));
    }else{
      $this->setRoles(array());
    }    
  }

  public function getLocked(){
    return $this->locked;
  }

  public function getExpired(){
    return $this->expired;
  }

  public function getClaveCaducada(){
    return $this->claveCaducada;
  }

  public function setClaveCaducada($claveCaducada = false){
    $this->claveCaducada = $claveCaducada;
  }

  public function getLogoutAuto(){
    return $this->logoutAuto;
  }

  public function setLogoutAuto($logoutAuto = false){
    $this->logoutAuto = $logoutAuto;
  }

  /**
   * Set fecha
   *
   * @param \DateTime $fecha
   * @return Accion
   */
  public function setFechaCaducidadClave($fechaCaducidadClave) {
    $this->fechaCaducidadClave = $fechaCaducidadClave;

    return $this;
  }

  /**
   * Get fecha
   *
   * @return \DateTime 
   */
  public function getFechaCaducidadClave() {
    return $this->fechaCaducidadClave;
  }

  public function setSessionId($sessionId = null) {
    $this->sessionId = $sessionId;

    return $this;
  }

  public function getSessionId() {
    return $this->sessionId;
  }

  public function isAdmin(){
    return Util::semaphore($this->getTipo() === 'ROLE_ADMIN');
  }

  public function renderTipo($params){
	$rol = $params['em']->getRepository('CommonBundleUserBundle:Role')->findOneByMachinename($this->getTipo());
	
	if ($rol) return $rol->getName();
	else return false;
  }
}
