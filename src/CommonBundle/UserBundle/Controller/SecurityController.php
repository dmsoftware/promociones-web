<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CommonBundle\UserBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Controller\SecurityController as BaseController;

class SecurityController extends BaseController
{    
    public function loginAction() {
      $session = $this->container->get('request')->getSession();
      $em = $this->container->get('doctrine')->getManager();
      $em->getRepository('CommonBundleAdministratorBundle:Configuracion')->configurationInSession($session);
      
      $securityContext = $this->container->get('security.context');
      //if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
        return new RedirectResponse($this->container->get('router')->generate('promokiss_public_homepage'));
      //}
      
      return parent::loginAction();
    }
}
