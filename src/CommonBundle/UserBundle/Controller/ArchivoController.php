<?php

namespace CommonBundle\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CommonBundle\UserBundle\Entity\Archivo;
use CommonBundle\UserBundle\Form\ArchivoType;
use AppBundle\Util\Util;

use AppBundle\Controller\DefaultController as AppController;


/**
 * Archivo controller.
 *
 * @Route("/archivo")
 */
class ArchivoController extends AppController
{

	 /**
	  * Lists all Archivo entities.
	  *
	  * @Route("/", name="archivo")
	  * @Method("GET")
	  * @Template()
	  */
	 public function indexAction(Request $request, $class = 'Archivo')
	 {
		  return parent::indexAction($request, $class);
	 }
	 

	 /**
	  * Displays a form to create a new Archivo entity.
	  *
	  * @Route("/new", name="archivo_new")
	  * @Method({"GET", "POST"})
	  * @Template()
	  */
	 public function newAction(Request $request, $class = 'Archivo')
	 {
			 return parent::newAction($request, $class);
	 }

	 /**
	  * Finds and displays a Archivo entity.
	  *
	  * @Route("/{id}", name="archivo_show")
	  * @Method("GET")
	  * @Template()
	  */
	 public function showAction($id, Request $request, $class = 'Archivo')
	 {
		  return parent::showAction($id, $request, $class);
	 }

	 /**
	  * Displays a form to edit an existing Archivo entity.
	  *
	  * @Route("/{id}/edit", name="archivo_edit")
	  * @Method({"GET", "POST"})
	  * @Template()
	  */
	 public function editAction($id, Request $request, $class = 'Archivo')
	 {
		  return parent::editAction($id, $request, $class);
	 }


	 /**
	  * Deletes a Archivo entity.
	  *
	  * @Route("/{id}", name="archivo_delete")
	  * @Method("DELETE")
	  */
	 public function deleteAction($id, Request $request, $class = 'Archivo')
	 {
		  return parent::deleteAction($id, $request, $class);
	 }

	   /**
	    * Delete a Archivo entity.
	    *
	    * @Route("/{id}/delete", name="archivo_delete_one")
	    * @Method("GET", "POST")
	    */
		public function deleteoneAction($id, Request $request, $class = 'Archivo')
		{
			$status = 'error';
			$error_code = 1;
			$info = array();
			$em = $this->getDoctrine()->getManager();
			$params = $request->request->all();
			$entity = (isset($params['entity'])) ? ($params['entity']) : null;
			$entity_id = (isset($params['entity_id'])) ? ($params['entity_id']) : null;
			$fieldName = (isset($params['fieldName'])) ? ($params['fieldName']) : null;
			$archivo = $em->getRepository('CommonBundleUserBundle:Archivo')->findOneById($id);
			$error_code = 2;
			if($archivo && $entity && $entity_id && $fieldName)
			{
				$em->remove($archivo);
				$em->flush();
				$status = 'success';
				$error_code = 0;
			}
			else
			{
				$status = 'success';
				$error_code = 100;
			}
			return new Response(json_encode(array('status' => $status, 'error_code' => $error_code, 'info' => json_encode($info))));
		}

	 	/**
	 	 * get: array de ids de archivos
	 	 * proceso: guarda el orden según orden de entrada en el array
	 	 *
		 * @Route("/saveimgorder", name="archivo_saveimgorder")
		 * @Method({"GET", "POST"})
	 	 */
	 	public function saveimgorderAction(Request $request, $class = 'Archivo')
	 	{
			$status = 'error';
			$error_code = 1;
			$info = array();
			$em = $this->getDoctrine()->getManager();
			$params = $request->request->all();
			$id_array = (isset($params['id_array'])) ? ($params['id_array']) : null;
			$ids = ($id_array != null) ? explode(',',$id_array) : null;
			$error_code = 2;
			if($ids != null)
			{
				$k = 1;
				foreach ($ids as $id) {
					$archivo = $em->getRepository('CommonBundleUserBundle:Archivo')->findOneById(intval($id));
					$archivo->setOrden($k);
					$em->persist($archivo);
					$k++;
				}
				$em->flush();
				$status = 'success';
				$error_code = 0;
			}
			return new Response(json_encode(array('status' => $status, 'error_code' => $error_code, 'info' => json_encode($info))));
	 	}

		/**
		 * get: img, img width, img height, x coord, y coord, crop width, crop height
		 * return: imagen recortada a la selección realizada
		 *
		 * @Route("/resizeimg", name="archivo_resizeimg")
		 * @Method({"GET", "POST"})
		 */
		public function resizeImgAction(Request $request, $class = 'Archivo')
		{
			$status = 'error';
			$error_code = 1;
			$info = array();
			$em = $this->getDoctrine()->getManager();
			$params = $request->request->all();
			$id_img = (isset($params['id_img'])) ? ($params['id_img']) : null;
			$id_old = (isset($params['id_old'])) ? ($params['id_old']) : null;
			$coord_x = (isset($params['coord_x'])) ? ($params['coord_x']) : null;
			$coord_y = (isset($params['coord_y'])) ? ($params['coord_y']) : null;
			$crop_w = (isset($params['crop_w'])) ? ($params['crop_w']) : null;
			$crop_h = (isset($params['crop_h'])) ? ($params['crop_h']) : null;
			$resolution_w = (isset($params['resolution_w'])) ? ($params['resolution_w']) : null;
			$resolution_h = (isset($params['resolution_h'])) ? ($params['resolution_h']) : null;

			$entity = (isset($params['entity'])) ? ($params['entity']) : null;
			$entity_id = (isset($params['entity_id'])) ? ($params['entity_id']) : null;
			$fieldName = (isset($params['fieldName'])) ? ($params['fieldName']) : null;

			$error_code = 2;
			if(($id_img != null) && ($coord_x != null) && ($coord_y != null) && ($crop_w != null) && ($crop_h != null) 
				&& ($resolution_w != null) && ($resolution_h != null) && ($entity != null) && ($entity_id != null) && ($fieldName != null))
			{
				$archivo = $em->getRepository('CommonBundleUserBundle:Archivo')->findOneById($id_img);
				$nombre_archivo = $archivo->getFullFilePath();
				list($ancho, $alto) = getimagesize($nombre_archivo);
				$img_resized = imagecreatetruecolor($resolution_w, $resolution_h);
				$img_croped = imagecreatetruecolor($crop_w, $crop_h);
				$img_original = $this->imagecreatefromfile($nombre_archivo);

				imagecopyresampled($img_croped, $img_original, 0, 0, $coord_x, $coord_y, $crop_w, $crop_h, $crop_w, $crop_h);
				imagecopyresampled($img_resized, $img_croped, 0, 0, 0, 0, $resolution_w, $resolution_h, $crop_w, $crop_h);

				$path = $this->imagereturntofile($img_resized,$nombre_archivo);
				if(!$path)
				{
					$error_code = 3;
				}
				else
				{
					$lag = pathinfo( $path, PATHINFO_BASENAME );
					$status = 'success';
					$error_code = 0;
					$archivo_old = $em->getRepository('CommonBundleUserBundle:Archivo')->findOneById($id_old);
					if($archivo_old)
					{
						unlink($archivo_old->getFullFilePath());
						$info['rename'] = rename($archivo->getAbsolutePath().$lag,$archivo_old->getAbsolutePath().$lag);
						$archivo_old->setFile($archivo_old->getAbsolutePath().$lag);
						$em->persist($archivo_old);
						$em->flush();
						$em->remove($archivo);
						$em->flush();
						$info['path'] = '/'.$archivo_old->getAbsolutePath().$lag;
					}
					else
					{
						$archivo->setFile($archivo->getAbsolutePath().$lag);
						$em->persist($archivo);
						$em->flush();
						$info['path'] = '/'.$archivo->getAbsolutePath().$lag;
					}
				}
			}
			else if($id_img != null && (($resolution_w == null) || ($resolution_h == null)))
			{
				$archivo = $em->getRepository('CommonBundleUserBundle:Archivo')->findOneById($id_img);
				$lag = pathinfo( $archivo->getFile(), PATHINFO_BASENAME );
				$status = 'success';
				$error_code = 0;
				$archivo_old = $em->getRepository('CommonBundleUserBundle:Archivo')->findOneById($id_old);
				if($archivo_old)
				{
					unlink($archivo_old->getFullFilePath());
					$info['rename'] = rename($archivo->getAbsolutePath().$lag,$archivo_old->getAbsolutePath().$lag);
					$archivo_old->setFile($archivo_old->getAbsolutePath().$lag);
					$em->persist($archivo_old);
					$em->flush();
					$em->remove($archivo);
					$em->flush();
					$info['path'] = '/'.$archivo_old->getAbsolutePath().$lag;
				}
				else
				{
					$archivo->setFile($archivo->getAbsolutePath().$lag);
					$em->persist($archivo);
					$em->flush();
					$info['path'] = '/'.$archivo->getAbsolutePath().$lag;
				}
			}
			return new Response(json_encode(array('status' => $status, 'error_code' => $error_code, 'info' => json_encode($info))));
		}

		private function imagecreatefromfile( $filename ) {
			if (!file_exists($filename)) {
				throw new InvalidArgumentException('File "'.$filename.'" not found.');
			}
			switch ( strtolower( pathinfo( $filename, PATHINFO_EXTENSION ))) {
				case 'jpeg':
				case 'jpg':
					return imagecreatefromjpeg($filename);
				break;

				case 'png':
					return imagecreatefrompng($filename);
				break;

				case 'gif':
					return imagecreatefromgif($filename);
				break;

				default:
					throw new InvalidArgumentException('File "'.$filename.'" is not valid jpg, png or gif image.');
				break;
			}
		}

		private function imagereturntofile( $img, $filename ) {
			
			//$lag = pathinfo( $filename, PATHINFO_DIRNAME ).'/'.pathinfo( $filename, PATHINFO_FILENAME ).'.png';
			$lag = pathinfo( $filename, PATHINFO_DIRNAME ).'/'.pathinfo( $filename, PATHINFO_FILENAME ).'.jpg';

			unlink($filename);
			if(file_exists($lag))
				{unlink($lag);}

			//if(imagepng($img,$lag))
			if(imagejpeg($img,$lag, 85))
				{return $lag;}
			else
				{return false;}
		}

		/**
		 * return all img info
		 *
		 * @Route("/loadinfo", name="archivo_loadinfo")
		 * @Method({"GET", "POST"})
		 */
		public function loadinfoAction(Request $request, $class = 'Archivo')
		{
			$status = 'error';
			$error_code = 1;
			$info = array();
			$em = $this->getDoctrine()->getManager();
			$params = $request->request->all();

			$entity = (isset($params['entity'])) ? ($params['entity']) : null;
			$entity_id = (isset($params['entity_id'])) ? ($params['entity_id']) : null;
			$fieldName = (isset($params['fieldName'])) ? ($params['fieldName']) : null;
			$type = (isset($params['type'])) ? ($params['type']) : 'false';
			$error_code = 2;
			if($entity && $entity_id && $fieldName) {
				$fn = Util::getObjectFieldMethodName($fieldName, 'get');

				$partes = explode('\\',$entity);
				$entity = array_pop($partes);
				$sobra = array_pop($partes);
				$ruta = implode('', $partes);

				$object = $em->getRepository($ruta.':'.$entity)->findOneById($entity_id);
				$arrayCollection = $object->$fn();
				foreach ($arrayCollection as $obj) {
					$info[$obj->getId()]['id'] = $obj->getId();
					$info[$obj->getId()]['orden']  = $obj->getOrden();
					$size = filesize($obj->getFullFilePath());
					$info[$obj->getId()]['size'] = $this->formatBytes($size);
					$info[$obj->getId()]['alt'] = '';
					$info[$obj->getId()]['resolution'] = '';
					if($type == 'true') {
						$info[$obj->getId()]['alt'] = $obj->getAlt();
						$tipo = exif_imagetype($obj->getFullFilePath());
						if($tipo) {
							list($w, $h) = getimagesize($obj->getFullFilePath());
							$info[$obj->getId()]['resolution'] = $w.'x'.$h.' ';
							$info[$obj->getId()]['class'] = ($w > $h) ? 'v_center' : '';
						}
						else
						{
							$info[$obj->getId()]['resolution'] = '';
							$info[$obj->getId()]['class'] = '';
						}
					}
				}
				$status = 'success';
				$error_code = 0;
			}
			return new Response(json_encode(array('status' => $status, 'error_code' => $error_code, 'info' => json_encode($info))));
		}

		/**
		 * upload the file and update the entity
		 *
		 * @Route("/remove", name="archivo_remove")
		 * @Method({"GET", "POST"})
		 */
		public function removeAction(Request $request, $class = 'Archivo')
		{
			$status = 'error';
			$error_code = 1;
			$info = array('id' => 0, 'path' => '/');
			if ($request->isMethod('POST')) {
				$em = $this->getDoctrine()->getManager();
				$params = $request->request->all();

				$fieldId = (isset($params['field_id'])) ? ($params['field_id']) : null;
				$entity = (isset($params['entity'])) ? ($params['entity']) : null;
				$entity_id = (isset($params['entity_id'])) ? ($params['entity_id']) : null;
				$fieldName = (isset($params['fieldName'])) ? ($params['fieldName']) : null;

				$error_code = 2;
				if($fieldId) {
					$archivo = $em->getRepository('CommonBundleUserBundle:Archivo')->findOneById($fieldId);

					$fn = Util::getObjectFieldMethodName($fieldName, 'remove');

					$partes = explode('\\',$entity);
					$entity = array_pop($partes);
					$sobra = array_pop($partes);
					$ruta = implode('', $partes);

					$object = $em->getRepository($ruta.':'.$entity)->findOneById($entity_id);
					$object->$fn($archivo);
					$em->persist($object);
					$em->flush();

					$em->remove($archivo);
					$em->flush();
					$status = 'success';
					$error_code = 0;
				}
			}
			return new Response(json_encode(array('status' => $status, 'error_code' => $error_code, 'info' => json_encode($info))));
		}

		/**
		 * upload the file and update the entity
		 *
		 * @Route("/uploadedit", name="archivo_upload_edit")
		 * @Method({"GET", "POST"})
		 */
		public function uploadeditAction(Request $request, $class = 'Archivo')
		{
			$status = 'error';
			$error_code = 1;
			$info = array('id' => 0, 'path' => '/', 'alt' => '', 'size' => '', 'resolution' => '');
			if ($request->isMethod('POST')) {
				$em = $this->getDoctrine()->getManager();
				$params = $request->request->all();

				$ext_array = (isset($params['file_extensiones'])) ? (explode(',',$params['file_extensiones'])) : null;
				$resolution = (isset($params['file_resolution'])) ? (explode('x',$params['file_resolution'])) : null;
				$max = (isset($params['file_size'])) ? ($params['file_size'] * 1024 * 1024) : Archivo::MAX_FILE_SIZE;
				$entity_id = (isset($params['entity_id'])) ? ($params['entity_id']) : null;
				$error_code = 2;
				if($entity_id) {
					//print_r($request->files);
					$files = $request->files->get('archivo');
					$error_code = 6;
					if($files)
					{
						$file = current($files);

						$name = $file->getClientOriginalName();
						$ext = pathinfo($name,PATHINFO_EXTENSION);
						$fitx_size = $file->getClientSize();
						$system_max = $file->getMaxFilesize();

						$tipo = exif_imagetype($file);
						$res_ok = false;
						if($tipo && ($resolution != null)) {
							list($w, $h) = getimagesize($file);
							$res_ok = (($w >= $resolution[0]) && ($h >= $resolution[1])) ? true : false;
						}
						else {$res_ok = true;}


						if(!in_array(strtolower($ext), $ext_array) && ($ext_array != null))
						{
							$error_code = 3;
						}
						else
						{
							if(($fitx_size > $max) || ($fitx_size > $system_max))
							{
								$error_code = 4;
							}
							else
							{

								if(!$res_ok) {
									$error_code = 5;
								}
								else
								{
									if($tipo) {
										list($w, $h) = getimagesize($file);
										$info['resolution'] = $w.'x'.$h.' ';
									}

									$archivo_old = $em->getRepository('CommonBundleUserBundle:Archivo')->findOneById($entity_id);
									$archivo = new Archivo();
									$em->persist($archivo);
									$em->flush();
									/*
									$archivo->removeFile();
									$em->persist($archivo);
									$em->flush();
									*/

									$path = $archivo->getUploadRootDir();
									$name = $file->getClientOriginalName();
									$partes_ruta = pathinfo($name);
									$newName = $this->format_uri($partes_ruta['filename']).'.'.$partes_ruta['extension'];
									$file->move($path, $newName);
									$archivo->setFile($archivo->getAbsolutePath().$newName);
									$em->persist($archivo);
									$em->flush();

									$status = 'success';
									$error_code = 0;
									$info['id'] = $archivo->getId();
									$info['id_old'] = ($archivo_old) ? $archivo_old->getId() : 0;
									$info['path'] = $archivo->getFile();

								}
							}
						}
					}
				}
			}
			return new Response(json_encode(array('status' => $status, 'error_code' => $error_code, 'info' => json_encode($info))));
		}
		 /**
		  * upload the file and create relation with entity
		  *
		  * @Route("/upload", name="archivo_upload")
		  * @Method({"GET", "POST"})
		  */
		 public function archivouploadAction(Request $request, $class = 'Archivo')
		 {
			$status = 'error';
			$error_code = 1;
			$info = array('id' => 0, 'path' => '/', 'alt' => '', 'size' => '', 'resolution' => '');
			if ($request->isMethod('POST')) {
				$em = $this->getDoctrine()->getManager();
				$params = $request->request->all();

				$ext_array = (isset($params['file_extensiones'])) ? (explode(',',$params['file_extensiones'])) : null;
				$max = (isset($params['file_size'])) ? ($params['file_size'] * 1024 * 1024) : Archivo::MAX_FILE_SIZE;
				$resolution = (isset($params['file_resolution'])) ? (explode('x',$params['file_resolution'])) : null;
				$entity = (isset($params['entity'])) ? ($params['entity']) : null;
				$entity_id = (isset($params['entity_id'])) ? ($params['entity_id']) : null;
				$fieldName = (isset($params['fieldName'])) ? ($params['fieldName']) : null;
				$fileElementId = (isset($params['fileElementId'])) ? ($params['fileElementId']) : null;
				$error_code = 2;
				if($entity && $entity_id && $fieldName && $fileElementId) {
					$error_code = 3;
					$file = $request->files->get($fileElementId);

					$name = $file->getClientOriginalName();
					$ext = pathinfo($name,PATHINFO_EXTENSION);
					$fitx_size = $file->getClientSize();
					$system_max = $file->getMaxFilesize();

					$tipo = exif_imagetype($file);
					$res_ok = false;
					if($tipo && ($resolution != null)) {
						list($w, $h) = getimagesize($file);
						$res_ok = (($w >= $resolution[0]) && ($h >= $resolution[1])) ? true : false;
					}
					else {$res_ok = true;}

					if(!in_array(strtolower($ext), $ext_array) && ($ext_array != null))
					{
						$error_code = 3;
					}
					else
					{
						if(($fitx_size > $max) || ($fitx_size > $system_max))
						{
							$error_code = 4;
						}
						else
						{
							if(!$res_ok) {
								$error_code = 5;
							}
							else
							{
								if($tipo) {
									list($w, $h) = getimagesize($file);
									$info['resolution'] = $w.'x'.$h.' ';
								}
								$archivo = new Archivo();
								$em->persist($archivo);
								$em->flush();

								$path = $archivo->getUploadRootDir();
								$name = $file->getClientOriginalName();
								$partes_ruta = pathinfo($name);
								$newName = $this->format_uri($partes_ruta['filename']).'.'.$partes_ruta['extension'];
								$file->move($path, $newName);
								$archivo->setFile($archivo->getAbsolutePath().$newName);
								$em->persist($archivo);

								$fn = Util::getObjectFieldMethodName($fieldName, 'add');
								$fn_get = Util::getObjectFieldMethodName($fieldName, 'get');

								$partes = explode('\\',$entity);
								$entity = array_pop($partes);
								$sobra = array_pop($partes);
								$ruta = implode('', $partes);

								$object = $em->getRepository($ruta.':'.$entity)->findOneById($entity_id);
								$objs = $object->$fn_get();
								$orden = 0;
								foreach ($objs as $obj) {
									$orden = ($obj->getOrden() > $orden) ? $obj->getOrden() : $orden;
								}
								$orden++;
								$archivo->setOrden($orden);
								$object->$fn($archivo);
								$em->persist($object);
								$em->flush();

								$status = 'success';
								$error_code = 0;
								$info['id'] = $archivo->getId();
								$info['id_old'] = 0;
								$info['path'] = $archivo->getFile();
							}
						}
					}
				}
			}
			return new Response(json_encode(array('status' => $status, 'error_code' => $error_code, 'info' => json_encode($info))));
		 }

		 private function format_uri( $string, $separator = '-' )
		 {
			  $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
			  $special_cases = array( '&' => 'and', "'" => '');
			  $string = mb_strtolower( trim( $string ), 'UTF-8' );
			  $string = str_replace( array_keys($special_cases), array_values( $special_cases), $string );
			  $string = preg_replace( $accents_regex, '$1', htmlentities( $string, ENT_QUOTES, 'UTF-8' ) );
			  $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
			  $string = preg_replace("/[$separator]+/u", "$separator", $string);
			  return $string;
		 }

		private function formatBytes($bytes, $precision = 2) { 
			 $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

			 $bytes = max($bytes, 0); 
			 $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
			 $pow = min($pow, count($units) - 1); 

			 // Uncomment one of the following alternatives
			 $bytes /= pow(1024, $pow);
			 // $bytes /= (1 << (10 * $pow)); 

			 return round($bytes, $precision) . ' ' . $units[$pow]; 
		} 

}
