<?php

namespace CommonBundle\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CommonBundle\UserBundle\Entity\UserAccess;
use CommonBundle\UserBundle\Form\UserAccessType;

use AppBundle\Controller\DefaultController as AppController;


/**
 * UserAccess controller.
 *
 * @Route("/useraccess")
 */
class UserAccessController extends AppController
{

    /**
     * Lists all UserAccess entities.
     *
     * @Route("/", name="useraccess")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request, $class = 'UserAccess')
    {
        return parent::indexAction($request, $class);
    }
    

    /**
     * Displays a form to create a new UserAccess entity.
     *
     * @Route("/new", name="useraccess_new")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function newAction(Request $request, $class = 'UserAccess')
    {
          return parent::newAction($request, $class);
    }

    /**
     * Finds and displays a UserAccess entity.
     *
     * @Route("/{id}", name="useraccess_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, Request $request, $class = 'UserAccess')
    {
        return parent::showAction($id, $request, $class);
    }

    /**
     * Displays a form to edit an existing UserAccess entity.
     *
     * @Route("/{id}/edit", name="useraccess_edit")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function editAction($id, Request $request, $class = 'UserAccess')
    {
        return parent::editAction($id, $request, $class);
    }


    /**
     * Deletes a UserAccess entity.
     *
     * @Route("/{id}", name="useraccess_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id, Request $request, $class = 'UserAccess')
    {
        return parent::deleteAction($id, $request, $class);
    }

}
