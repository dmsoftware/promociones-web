<?php

namespace CommonBundle\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CommonBundle\UserBundle\Entity\Role;
use CommonBundle\UserBundle\Form\RoleType;

use AppBundle\Controller\DefaultController as AppController;


/**
 * Role controller.
 *
 * @Route("/role")
 */
class RoleController extends AppController
{

    /**
     * Lists all Role entities.
     *
     * @Route("/", name="role")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request, $class = 'Role')
    {
        return parent::indexAction($request, $class);
    }
    

    /**
     * Displays a form to create a new Role entity.
     *
     * @Route("/new", name="role_new")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function newAction(Request $request, $class = 'Role')
    {
          return parent::newAction($request, $class);
    }

    /**
     * Finds and displays a Role entity.
     *
     * @Route("/{id}", name="role_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, Request $request, $class = 'Role')
    {
        return parent::showAction($id, $request, $class);
    }

    /**
     * Displays a form to edit an existing Role entity.
     *
     * @Route("/{id}/edit", name="role_edit")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function editAction($id, Request $request, $class = 'Role')
    {
        return parent::editAction($id, $request, $class);
    }


    /**
     * Deletes a Role entity.
     *
     * @Route("/{id}", name="role_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id, Request $request, $class = 'Role')
    {
        return parent::deleteAction($id, $request, $class);
    }

}
