<?php

namespace CommonBundle\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CommonBundle\UserBundle\Entity\User;
use CommonBundle\UserBundle\Form\UserType;
use AppBundle\Util\Util;
use AppBundle\Controller\DefaultController as AppController;

class DefaultController extends AppController {

  public function indexAction(Request $request, $class = 'User') {
    return parent::indexAction($request, $class);
  }

  public function noPermissionsAction() {
    return $this->render('CommonBundleUserBundle:Default:noPermissions.html.twig');
  }

  public function changeLanguageAction($lang) {
    $user = $this->getUser();
    $request = $this->getRequest();
    $userManager = $this->container->get('fos_user.user_manager');

    $this->get('session')->set('_locale', $lang);

    $user->setLocale($lang);
    $userManager->updateUser($user);

    //Cambio o creo cookie
    $cookie_name = $this->container->getParameter('cookie_lang');
    $cookie_lang = $request->cookies->get($cookie_name);    
    //Creo cookie
    $cookie_lang = new Cookie($cookie_name, $lang, 2147483647);
    
    $redirect = new RedirectResponse($request->headers->get('referer'));  
    $redirect->headers->setCookie($cookie_lang);
    return $redirect;
  }

  /**
   * Creates a new User entity.
   *
   */
  public function createAction(Request $request) {
    $form = $this->container->get('fos_user.registration.form');
    $formHandler = $this->container->get('fos_user.registration.form.handler');
    $process = $formHandler->process(false);
    $valid = 0;

    if ($process) {
      $user = $form->getData();
      $valid = 1;
      //return $this->redirect($this->generateUrl('common_bundle_user_show', array('id' => $user->getId())));
    }

    return $this->render('CommonBundleUserBundle:Default:new.html.twig', array(
                'form' => $form->createView(),
                'valid' => $valid,
                'entity' => $user
    ));
  }

  /**
   * Displays a form to create a new User entity.
   *
   */
  public function newAction(Request $request, $class = 'User') {
    return parent::newAction($request, $class);
  }

  /**
   * Finds and displays a User entity.
   *
   */
  public function showAction($id, Request $request, $class = 'User') {
    $em = $this->getDoctrine()->getManager();

    $entity = $em->getRepository('CommonBundleUserBundle:User')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find User entity.');
    }


    return $this->render('CommonBundleUserBundle:Default:show.html.twig', array(
                'entity' => $entity
    ));
  }

  /**
   * Displays a form to edit an existing User entity.
   *
   */
  public function editAction($id, Request $request, $class = 'User') {
    return parent::editAction($id, $request, $class);
  }

  /**
   * Creates a form to edit a User entity.
   *
   * @param User $entity The entity
   *
   * @return \Symfony\Component\Form\Form The form
   */
  private function createEditForm1(User $entity) {
    
    $em = $this->getDoctrine()->getManager();
    $form = $this->createForm(new UserType($em), $entity);

    return $form;
  }

  /**
   * Edits an existing User entity.
   *
   */
  public function updateAction(Request $request, $id) {
    $em = $this->getDoctrine()->getManager();

    $entity = $em->getRepository('CommonBundleUserBundle:User')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find User entity.');
    }

    $editForm = $this->createEditForm1($entity);
    $editForm->handleRequest($request);

    $valid = 0;
    if ($editForm->isValid()) {
      $values = $request->request->get('commonbundle_userbundle_user');
      if (trim($values['password'])) {
        $entity->setPlainPassword($values['password']);
      }
      $em->flush();

      $valid = 1;
    }

    return $this->render('CommonBundleUserBundle:Default:edit.html.twig', array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'valid' => $valid,
    ));
  }

  /**
   * Deletes a User entity.
   *
   */
  public function deleteAction($id, Request $request, $class = 'User') {
    return parent::deleteAction($id, $request, $class);
  }

  public function enableAction($id) {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('CommonBundleUserBundle:User')->find($id);

    if (!$entity) {
      $resp = array('error' => $this->get('translator')->trans('element_not_finded'));
    } else {
      $entity->setEnabled(!$entity->isEnabled());
      $em->flush();

      $resp = array('enabled' => $entity->isEnabled());
    }

    return new Response(json_encode($resp));
  }

  public function permissionsAction() {
    $em = $this->getDoctrine()->getManager();
    $menus = Util::renderTree($em->getRepository('CommonBundleAdministratorBundle:Menu')->getMenuTree(), 0, null, false);
    $users = $em->getRepository('CommonBundleUserBundle:User')->findAll();
    $result = array();
    foreach ($users as $user) {
      $role = $user->getTipo();
      $role = explode(' ',$role);
      if(in_array("ROLE_ADMIN", $role) || in_array("ROLE_PUBLIC_COMPANY", $role))
      {
        $result[] = $user;
      }
    }
    $users = $result;
    return $this->render('CommonBundleUserBundle:Default:permissions.html.twig', array(
                'menus' => $menus,
                'users' => $users
    ));
  }

  public function editPermissionAction($user_id, $menu_id) {
    $resp = array();
    $em = $this->getDoctrine()->getManager();
    $enable = $this->getRequest()->get('enable', false);
    $user = $em->getRepository('CommonBundleUserBundle:User')->find($user_id);
    $menu = $em->getRepository('CommonBundleAdministratorBundle:Menu')->find($menu_id);

    if (!$user || !$menu) {
      $resp = array('error' => $this->get('translator')->trans('element_not_finded'));
    } else {
      $menus = $user->getMenuIds();
      if (!filter_var($enable, FILTER_VALIDATE_BOOLEAN) && !in_array($menu_id, $menus)) {
        $user->addMenu($menu);
        $em->flush();
      }
      if (filter_var($enable, FILTER_VALIDATE_BOOLEAN) && in_array($menu_id, $menus)) {
        $user->removeMenu($menu);
        $em->flush();
      }
    }

    return new Response(json_encode($resp));
  }

  public function resetClaveAction($id){
    $em = $this->getDoctrine()->getManager();
    $userManager = $this->container->get('fos_user.user_manager');
    $user = $em->getRepository('CommonBundleUserBundle:User')->find($id);

    $clave = Util::generatePassword();
    $user->setPlainPassword($clave);
    $user->setClaveCaducada(true);
    $userManager->updateUser($user);

    return $this->render('CommonBundleUserBundle:Default:resetClave.html.twig', array(
                'clave' => $clave,
                'cabecera' => $em->getRepository('CommonBundleUserBundle:User')->getHeadData($user, $this->getRequest()->getLocale(), $this->get('router'), $this->get('translator'))
    ));
  } 

}
