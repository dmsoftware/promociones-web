<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CommonBundle\UserBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Controller\ChangePasswordController as BaseController;

/**
 * Controller managing the password change
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class ChangePasswordController extends BaseController
{
    /**
     * Generate the redirection url when the resetting is completed.
     *
     * @param \FOS\UserBundle\Model\UserInterface $user
     *
     * @return string
     */
    protected function getRedirectionUrl(UserInterface $user)
    {
        $f = null;
        $em = $this->container->get('doctrine')->getManager();
        $conf = $em->getRepository('CommonBundleAdministratorBundle:Configuracion')->getConfiguration();
        if($conf && $conf->getResetearClabe()){
          $f = new \DateTime(date('Y-m-d', strtotime('now + ' . $conf->getResetearClabe() . ' days')));
        }

        $user->setClaveCaducada(false);
        $user->setFechaCaducidadClave($f);
        $userManager = $this->container->get('fos_user.user_manager');
        $userManager->updateUser($user);

        return $this->container->get('router')->generate('promokiss_public_homepage');
    }
}
