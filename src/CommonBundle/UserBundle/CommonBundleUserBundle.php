<?php

namespace CommonBundle\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CommonBundleUserBundle extends Bundle {

  public function getParent() {
    return 'FOSUserBundle';
  }

}
