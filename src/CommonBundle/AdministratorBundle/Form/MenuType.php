<?php

namespace CommonBundle\AdministratorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class MenuType extends AbstractType {

  /**
   * @param FormBuilderInterface $builder
   * @param array $options
   */
  public function buildForm(FormBuilderInterface $builder, array $options) {
    $isNew = isset($options['data']) && $options['data']->getId() ? false : true;
    
    $builder->add('translations', 'a2lix_translations_gedmo', array(
      'translatable_class' => 'CommonBundle\AdministratorBundle\Entity\Menu',
      'label' => false
    ));
    $builder->add('bundle', null, array('data' => $isNew ? 'CommonBundle' : $options['data']->getControlador()));
    $builder->add('controlador', null, array('data' => $isNew ? 'Default' : $options['data']->getControlador()));
    $builder->add('accion', null, array('data' => $isNew ? 'Index' : $options['data']->getAccion()));
  }

  /**
   * @param OptionsResolverInterface $resolver
   */
  public function setDefaultOptions(OptionsResolverInterface $resolver) {
    $resolver->setDefaults(array(
        'data_class' => 'CommonBundle\AdministratorBundle\Entity\Menu'
    ));
  }

  /**
   * @return string
   */
  public function getName() {
    return 'commonbundle_administratorbundle_menu';
  }

}
