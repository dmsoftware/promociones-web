<?php

namespace CommonBundle\AdministratorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConfiguracionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('file', null, array('label' => 'logo'))
            ->add('fileFavicon', null, array('label' => 'Favicon'))
            ->add('email')
            ->add('web')
            ->add('senderEmail')
            ->add('senderHost')
            ->add('senderPort', 'choice', array('choices' => array('25' => 25, '465' => 465), 'required' => false))
            ->add('senderEncryption', 'choice', array('choices' => array('' => '', 'tls' => 'tls', 'ssl' => 'ssl'), 'required' => false))
            ->add('senderUser', null, array('attr' => array('autocomplete' => 'off')))
            ->add('senderPass', 'password', array('attr' => array('autocomplete' => 'off'), 'required' => false))
            ->add('resetearClabe', 'choice', array('label' => 'Caducidad contraseñas', 'choices' => array('' => 'Nunca', 30 => '30 días', 60 => '60 días', 90 => '90 días', 120 => '120 días', 150 => '150 días', 180 => '180 días'), 'required' => false))
            ->add('conexionSimultanea', null, array('label' => '¿Permitir conexiones simultaneas con el mismo usuario?'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\AdministratorBundle\Entity\Configuracion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'commonbundle_administratorbundle_configuracion';
    }
}
