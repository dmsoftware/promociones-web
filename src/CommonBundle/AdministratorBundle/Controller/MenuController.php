<?php

namespace CommonBundle\AdministratorBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CommonBundle\AdministratorBundle\Entity\Menu;
use CommonBundle\AdministratorBundle\Form\MenuType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use AppBundle\Controller\DefaultController as AppController;

/**
 * Menu controller.
 *
 */
class MenuController extends AppController
{

    /**
     * Lists all Menu entities.
     *
     * @Route("/", name="menu")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request, $class = 'Menu')
    {
        return parent::indexAction($request, $class);        
    }

    /**
     * Displays a form to create a new Menu entity.
     *
     * @Route("/new", name="menu_new")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function newAction(Request $request, $class = 'Menu')
    {
          return parent::newAction($request, $class);
    }

    /**
     * Finds and displays a Menu entity.
     *
     * @Route("/{id}", name="menu_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, Request $request, $class = 'Menu')
    {
        return parent::showAction($id, $request, $class);
    }


    /**
     * Displays a form to edit an existing Menu entity.
     *
     * @Route("/{id}/edit", name="menu_edit")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function editAction($id, Request $request, $class = 'Menu')
    {
        return parent::editAction($id, $request, $class);
    }


    /**
     * Deletes a Menu entity.
     *
     * @Route("/{id}", name="menu_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id, Request $request, $class = 'Menu')
    {
        return parent::deleteAction($id, $request, $class);
    }


    /**
     * Creates a new Menu entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Menu();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('menu_show', array('id' => $entity->getId())));
        }

        return $this->render('CommonBundleAdministratorBundle:Menu:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Menu entity.
     *
     * @param Menu $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Menu $entity)
    {
        $form = $this->createForm(new MenuType(), $entity, array(
            'action' => $this->generateUrl('menu_create'),
            'method' => 'POST',
        ));
 
        $form->add('submit', 'submit', array('label' => $this->get('translator')->trans('form.action.create')));

        return $form;
    }

    
    
    /**
     * Edits an existing Menu entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CommonBundleAdministratorBundle:Menu')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Menu entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('menu_edit', array('id' => $id)));
        }

        return $this->render('CommonBundleAdministratorBundle:Menu:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    /**
     * Get current Route.
     *
     */
    public function currentAction($controller, Request $request, $class = 'Menu')
    {
        $nombre = '';
        $bundle = explode('\\', $controller);
        if (count($bundle) > 1) {
            $bundle = explode('Bundle', $bundle[1]);
            $bundle = $bundle[0];
        } else {
            $bundle = '';
        }
        $ruta = explode('::', $controller);
        if (count($ruta) > 1) {
            $accion = str_replace('Action', '', $ruta[1]);
            $controlador_ruta = explode('\\', $ruta[0]);
            $controlador = str_replace('Controller', '', $controlador_ruta[count($controlador_ruta) - 1]);

            $em = $this->getDoctrine()->getManager();
            $menu = $em->getRepository('CommonBundleAdministratorBundle:Menu')->findOneBy(array('bundle' => $bundle, 'controlador' => $controlador, 'accion' => $accion));
            
            if ($menu) {
                $nombre = self::current($menu->getId(),0);
            }

        }

        return new Response($nombre);
    }

    private function current($id, $cont) {

        
        $em = $this->getDoctrine()->getManager();
        $menu = $em->getRepository('CommonBundleAdministratorBundle:Menu')->findOneById($id);
        $nombre = $menu->getNombre();
        if (strpos($nombre, ':') !== false) {
            $nombreAux = explode(':', $nombre);
            $nombre = ucfirst($nombreAux[count($nombreAux) - 1]);
        }        
        if ($menu->getParentId() !== NULL && $menu->getParentId() > 0 && $cont < 10) {
            $cont = $cont +1;
            $menuParent = $em->getRepository('CommonBundleAdministratorBundle:Menu')->findOneById($menu->getParentId());
            if ($menuParent) {
                $nombreParent = self::current($menuParent->getId(), $cont);
                $nombre = $nombreParent . ' / ' . $nombre;
            }
        }

        return $nombre;
    }
}
