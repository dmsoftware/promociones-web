<?php

namespace CommonBundle\AdministratorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CommonBundle\AdministratorBundle\Entity\LogAction;

use AppBundle\Controller\DefaultController as AppController;


/**
 * LogAction controller.
 *
 * @Route("/logaction")
 */
class LogActionController extends AppController
{

    /**
     * @Route("/eliminarHistorico", name="logaction_eliminar_historico")
     */
    public function eliminarHistoricoAction(Request $request, $class = 'LogAction')
    {
        $sql = 'DELETE FROM SFAPP_logaction WHERE DATE(created) < DATE(DATE_SUB(NOW(), INTERVAL 1 MONTH))';
        
        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $this->redirect($this->generateUrl('logaction'));
    }

    /**
     * Lists all LogAction entities.
     *
     * @Route("/", name="logaction")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request, $class = 'LogAction')
    {
        return parent::indexAction($request, $class);
    }
    

    /**
     * Finds and displays a LogAction entity.
     *
     * @Route("/{id}", name="logaction_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, Request $request, $class = 'LogAction')
    {
        return parent::showAction($id, $request, $class);
    }
}
