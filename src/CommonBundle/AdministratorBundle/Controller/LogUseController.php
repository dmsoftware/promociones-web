<?php

namespace CommonBundle\AdministratorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CommonBundle\AdministratorBundle\Entity\LogUse;

use AppBundle\Controller\DefaultController as AppController;


/**
 * LogUse controller.
 *
 * @Route("/loguse")
 */
class LogUseController extends AppController
{

    /**
     * Lists all LogUse entities.
     *
     * @Route("/", name="loguse")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request, $class = 'LogUse')
    {
        return parent::indexAction($request, $class);
    }
    

    /**
     * Finds and displays a LogUse entity.
     *
     * @Route("/{id}", name="loguse_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, Request $request, $class = 'LogUse')
    {
        return parent::showAction($id, $request, $class);
    }
}
