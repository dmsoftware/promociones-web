<?php

namespace CommonBundle\AdministratorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CommonBundle\AdministratorBundle\Entity\IpControl;
use CommonBundle\AdministratorBundle\Form\IpControlType;

use AppBundle\Controller\DefaultController as AppController;


/**
 * IpControl controller.
 *
 * @Route("/ipcontrol")
 */
class IpControlController extends AppController
{

    /**
     * Lists all IpControl entities.
     *
     * @Route("/", name="ipcontrol")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request, $class = 'IpControl')
    {
        return parent::indexAction($request, $class);
    }
    

    /**
     * Displays a form to create a new IpControl entity.
     *
     * @Route("/new", name="ipcontrol_new")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function newAction(Request $request, $class = 'IpControl')
    {
          return parent::newAction($request, $class);
    }

    /**
     * Finds and displays a IpControl entity.
     *
     * @Route("/{id}", name="ipcontrol_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, Request $request, $class = 'IpControl')
    {
        return parent::showAction($id, $request, $class);
    }

    /**
     * Displays a form to edit an existing IpControl entity.
     *
     * @Route("/{id}/edit", name="ipcontrol_edit")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function editAction($id, Request $request, $class = 'IpControl')
    {
        return parent::editAction($id, $request, $class);
    }


    /**
     * Deletes a IpControl entity.
     *
     * @Route("/{id}", name="ipcontrol_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id, Request $request, $class = 'IpControl')
    {
        return parent::deleteAction($id, $request, $class);
    }

}
