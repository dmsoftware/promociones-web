<?php

namespace CommonBundle\AdministratorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CommonBundle\AdministratorBundle\Entity\Menu;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller {

  public function indexAction() {
    $user = $this->getUser();
    $request = $this->getRequest();
    $session = $request->getSession();
    $referer = $request->headers->get('referer');
    $em = $this->getDoctrine()->getManager();
    $re = $em->getRepository('CommonBundleAdministratorBundle:Menu');
    $userManager = $this->get('fos_user.user_manager');

    //Cambiar el idioma por el último idioma del usuario
    $request->setLocale($user->getLocale());

    //Guardar en sesión la estructura del menú del usuario
    if ($user->hasRole('ROLE_ADMIN')) {
      $session->set('menuTree', $re->getMenuTree(false,true));
    } else {
      $ids = array_merge(array(0), $user->getMenuIds());
      $session->set('menuTree', $re->getMenuTree($ids,false));
    }
    
    //Datos de configuración en la sesión
    $conf = $em->getRepository('CommonBundleAdministratorBundle:Configuracion')->configurationInSession($session);

    // Comprobar si la clave esta caducada
    if($user->getFechaCaducidadClave() && date_diff(new \DateTime(), $user->getFechaCaducidadClave())->format('%R%a') < 0){
      $user->setFechaCaducidadClave(null);
      $user->setClaveCaducada(true);
    }

    // Actualizar fecha de caducidad
    if($conf && $conf->getResetearClabe() && !$user->getFechaCaducidadClave()){
      $user->setFechaCaducidadClave(new \DateTime(date('Y-m-d', strtotime('now + ' . $conf->getResetearClabe() . ' days'))));
    }    

    $user->setSessionId($session->getId());

    // Actualizar usuario
    $userManager->updateUser($user);
      
    //Guardar el log de accesos
    $em->getRepository('CommonBundleAdministratorBundle:LogAccess')->saveAction('login', $user, $session, $referer);
    return $this->redirect($this->generateUrl('promocion'));
  }

  public function adminAction() {
    return $this->render('CommonBundleAdministratorBundle:Default:admin.html.twig');
  }

  public function configurationAction(Request $request) {     
    $em = $this->getDoctrine()->getManager();
    $conf = $em->getRepository('CommonBundleAdministratorBundle:Configuracion')->getConfiguration(true);

    $form = $this->createForm(new \CommonBundle\AdministratorBundle\Form\ConfiguracionType(), $conf);
    $form->handleRequest($request);
    
    if ($form->isValid()) {
      $conf->upload();
      $conf->uploadFavicon();

      $em->persist($conf);
      $em->flush();
      
      $session = $request->getSession();
      $session->getFlashBag()->add( 'success', $this->get('translator')->trans('form.flash.success') );
      $em->getRepository('CommonBundleAdministratorBundle:Configuracion')->configurationInSession($session, $conf);
    }

    return $this->render('CommonBundleAdministratorBundle:Default:configuration.html.twig', array(
                'form' => $form->createView(),
                'conf' => $conf
    ));
  }
}
