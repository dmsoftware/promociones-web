<?php

namespace CommonBundle\AdministratorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CommonBundle\AdministratorBundle\Entity\LogAccess;

use AppBundle\Controller\DefaultController as AppController;


/**
 * LogAccess controller.
 *
 * @Route("/logaccess")
 */
class LogAccessController extends AppController
{

    /**
     * Lists all LogAccess entities.
     *
     * @Route("/", name="logaccess")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request, $class = 'LogAccess')
    {
        return parent::indexAction($request, $class);
    }
        

    /**
     * Finds and displays a LogAccess entity.
     *
     * @Route("/{id}", name="logaccess_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, Request $request, $class = 'LogAccess')
    {
        return parent::showAction($id, $request, $class);
    }
}
