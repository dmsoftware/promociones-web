<?php
namespace CommonBundle\AdministratorBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use CommonBundle\AdministratorBundle\Entity\Menu;

class MenuCommand extends ContainerAwareCommand
{
  protected function configure() {
    $this->setName('crmsymfony:menu:load')->setDescription('Cargar el menú de la aplicación');
  }
  
  protected function execute(InputInterface $input, OutputInterface $output) {
    $em = $this->getContainer()->get('doctrine')->getManager();

    //Generar el menú de inicio (oculto para uso interno)
    $this->createMenu($em, array('nombre' => 'Inicio', 'oculto' => true, 'orden' => 0, 'ruta' => 'common_bundle_administrator_homepage', 'admin' => false));

    //Generar el menú de Sistema
    $m = $this->createMenu($em, array('nombre' => 'Sistema', 'orden' => 2, 'ruta' => ''));

    //Generar los menús dentro del menú Sistema
    $submenus = array(
      array('nombre' => 'Configuración', 'ruta' => 'common_bundle_administrator_config', 'accion' => 'configuracion'),
      array('nombre' => 'Usuarios', 'ruta' => 'common_bundle_user_homepage', 'bundle' => 'User'),
      array('nombre' => 'Roles', 'ruta' => 'role', 'bundle' => 'User', 'controlador' => 'Role'),
      array('nombre' => 'Permisos', 'ruta' => 'common_bundle_user_permissions', 'bundle' => 'User', 'accion' => 'permissions'),
      array('nombre' => 'Permisos Avanzados', 'ruta' => 'useraccess', 'bundle' => 'User', 'controlador' => 'UserAccess'),
      array('nombre' => 'Log Accesos', 'ruta' => 'logaccess', 'controlador' => 'LogAccess'),
      array('nombre' => 'Log Uso', 'ruta' => 'loguse', 'controlador' => 'LogUse'),
      array('nombre' => 'Log Acciones BD', 'ruta' => 'logaction', 'controlador' => 'LogAction'),
      array('nombre' => 'Resticción de IPs', 'ruta' => 'ipcontrol', 'controlador' => 'IpControl')
    );

    foreach($submenus as $i => $datos){
      $datos['orden'] = $i + 1;
      $datos['parent_id'] = $m->getId();

      $this->createMenu($em, $datos);
    }
  }

  private function createMenu($em, $datos){
    $m = new Menu();
    $m->setNombre($datos['nombre']);
    $m->setRuta($datos['ruta']);
    $m->setBundle(isset($datos['bundle']) ? $datos['bundle'] : 'Administrator');
    $m->setControlador(isset($datos['controlador']) ? $datos['controlador'] : 'Default');
    $m->setAccion(isset($datos['accion']) ? $datos['accion'] : 'index');
    $m->setOculto(isset($datos['oculto']));
    if(isset($datos['orden'])) $m->setOrden($datos['orden']);
    if(isset($datos['parent_id'])) $m->setParentId($datos['parent_id']);
    $m->setAdmin(isset($datos['admin']) ? false : true);

    $em->persist($m);
    $em->flush();

    return $m;
  }
}
