<?php

namespace CommonBundle\AdministratorBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Menu
 *
 * @ORM\Table(name="SFAPP_menu")
 * @ORM\Entity(repositoryClass="CommonBundle\AdministratorBundle\Entity\MenuRepository")
 * @Gedmo\TranslationEntity(class="CommonBundle\AdministratorBundle\Entity\MenuTranslation")
 */
class Menu {

  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @Gedmo\Translatable
   * @ORM\Column(name="nombre", type="string", length=255)
   */
  private $nombre;

  /**
   * @var string
   * 
   * @ORM\Column(name="bundle", type="string", length=255)
   */
  private $bundle;

  /**
   * @var string
   *
   * @ORM\Column(name="controlador", type="string", length=255)
   */
  private $controlador;

  /**
   * @var string
   *
   * @ORM\Column(name="accion", type="string", length=255)
   */
  private $accion;

  /**
   * @var integer
   *
   * @ORM\Column(name="parent_id", type="integer", nullable=true)
   */
  private $parentId;

  /**
   * @var integer
   *
   * @ORM\Column(name="orden", type="integer", nullable=true)
   */
  private $orden;

  /**
   * @var string
   * 
   * @ORM\Column(name="ruta", type="string", length=255)
   */
  private $ruta;

  /**
   * @ORM\ManyToMany(targetEntity="CommonBundle\UserBundle\Entity\User", mappedBy="menus", cascade={"persist", "remove" })
   * */
  protected $users;

  /**
   * @ORM\Column(type="boolean", options={"default": false})
   */
  protected $oculto;

  /**
   * @ORM\Column(type="boolean", options={"default": false})
   */
  protected $admin;

  /**
   * @Gedmo\Timestampable(on="create")
   * @ORM\Column(name="created", type="datetime")
   */
  private $created;

  /**
   * @ORM\Column(name="updated", type="datetime")
   * @Gedmo\Timestampable(on="update")
   */
  private $updated;

  /**
   * @ORM\OneToMany(
   *   targetEntity="MenuTranslation",
   *   mappedBy="object",
   *   cascade={"persist", "remove"}
   * )
   */
  private $translations;

  public function __construct() {
    $this->translations = new ArrayCollection();
  }

  public function __toString() {
    $nombre = '';
    if ($this->getId())
      $nombre = $this->getNombre();
    return $nombre;
  }

  public function getTranslations() {
    return $this->translations;
  }

  public function addTranslation(MenuTranslation $t) {
    if (!$this->translations->contains($t)) {
      $this->translations[] = $t;
      $t->setObject($this);
    }
  }

  /**
   * Get id
   *
   * @return integer 
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set nombre
   *
   * @param string $nombre
   * @return Menu
   */
  public function setNombre($nombre) {
    $this->nombre = $nombre;

    return $this;
  }

  /**
   * Get nombre
   *
   * @return string 
   */
  public function getNombre() {
    return $this->nombre;
  }

  /**
   * Set bundle
   *
   * @param string $bundle
   * @return Menu
   */
  public function setBundle($bundle) {
    $this->bundle = $bundle;

    return $this;
  }

  /**
   * Get bundle
   *
   * @return string 
   */
  public function getBundle() {
    return $this->bundle;
  }

  /**
   * Set controlador
   *
   * @param string $controlador
   * @return Menu
   */
  public function setControlador($controlador) {
    $this->controlador = $controlador;

    return $this;
  }

  /**
   * Get controlador
   *
   * @return string 
   */
  public function getControlador() {
    return $this->controlador;
  }

  /**
   * Set accion
   *
   * @param string $accion
   * @return Menu
   */
  public function setAccion($accion) {
    $this->accion = $accion;

    return $this;
  }

  /**
   * Get accion
   *
   * @return string 
   */
  public function getAccion() {
    return $this->accion;
  }

  /**
   * Set parentId
   *
   * @param integer $parentId
   * @return Menu
   */
  public function setParentId($parentId) {
    $this->parentId = $parentId;

    return $this;
  }

  /**
   * Get parentId
   *
   * @return integer 
   */
  public function getParentId() {
    return $this->parentId;
  }

  public function getCreated() {
    return $this->created;
  }

  public function getUpdated() {
    return $this->updated;
  }

  public function setCreated($created) {
    $this->created = $created;
    return $this;
  }

  public function setUpdated($updated) {
    $this->updated = $updated;
    return $this;
  }

  public function getTraducciones() {
    return $this->traducciones;
  }

  public function setTraducciones($traducciones) {
    $this->traducciones = $traducciones;
    return $this;
  }

  public function getOculto() {
    return $this->oculto;
  }

  public function setOculto($oculto) {
    $this->oculto = $oculto;
    return $this;
  }

  /**
   * Remove translations
   *
   * @param \CommonBundle\AdministratorBundle\Entity\MenuTranslation $translations
   */
  public function removeTranslation(\CommonBundle\AdministratorBundle\Entity\MenuTranslation $translations) {
    $this->translations->removeElement($translations);
  }

  public function getOrden() {
    return $this->orden;
  }

  public function setOrden($orden) {
    $this->orden = $orden;
    return $this;
  }

  public function getRuta() {
    return $this->ruta;
  }

  public function setRuta($ruta) {
    $this->ruta = $ruta;
    return $this;
  }

  /**
   * Add users
   *
   * @param \CommonBundle\UserBundle\Entity\User $users
   * @return Menu
   */
  public function addUser(\CommonBundle\UserBundle\Entity\User $users) {
    $this->users[] = $users;

    return $this;
  }

  /**
   * Remove users
   *
   * @param \CommonBundle\UserBundle\Entity\User $users
   */
  public function removeUser(\CommonBundle\UserBundle\Entity\User $users) {
    $this->users->removeElement($users);
  }

  /**
   * Get users
   *
   * @return \Doctrine\Common\Collections\Collection 
   */
  public function getUsers() {
    return $this->users;
  }


    /**
     * Set admin
     *
     * @param boolean $admin
     * @return Menu
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin
     *
     * @return boolean 
     */
    public function getAdmin()
    {
        return $this->admin;
    }
}
