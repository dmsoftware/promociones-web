<?php

namespace CommonBundle\AdministratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * LogUse
 *
 * @ORM\Table(name="SFAPP_loguse")
 * @ORM\Entity(repositoryClass="CommonBundle\AdministratorBundle\Entity\LogUseRepository")
 */
class LogUse
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
  	 * @var \CommonBundle\UserBundle\Entity\User
  	 *
  	 * @ORM\ManyToOne(targetEntity="CommonBundle\UserBundle\Entity\User")
  	 * @ORM\JoinColumns({
  	 *   @ORM\JoinColumn(name="usuario", referencedColumnName="id", onDelete="CASCADE")
  	 * })
  	 */
    private $usuario;

    /**
     * @var string
     *
     * @ORM\Column(name="controller", type="string", length=255)
     */
    private $controller;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="access", type="datetime")
     */
    private $access;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usuario
     *
     * @param integer $usuario
     * @return LogUse
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return integer 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set controller
     *
     * @param string $controller
     * @return LogUse
     */
    public function setController($controller)
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     * Get controller
     *
     * @return string 
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Set view
     *
     * @param string $view
     * @return LogUse
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get view
     *
     * @return string 
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return LogUse
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set access
     *
     * @param \DateTime $access
     * @return LogUse
     */
    public function setAccess($access)
    {
        $this->access = $access;

        return $this;
    }

    /**
     * Get access
     *
     * @return \DateTime 
     */
    public function getAccess()
    {
        return $this->access;
    }
}
