<?php

namespace CommonBundle\AdministratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * LogDelete
 *
 * @ORM\Table(name="SFAPP_logdelete")
 * @ORM\Entity(repositoryClass="CommonBundle\AdministratorBundle\Entity\LogDeleteRepository")
 */
class LogDelete {

  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var integer
   *
   * @ORM\Column(name="usuario", type="integer")
   */
  private $usuario;

  /**
   * @var string
   *
   * @ORM\Column(name="query", type="string", length=4000)
   */
  private $query;

  /**
   * @var string
   *
   * @ORM\Column(name="url", type="string", length=1000)
   */
  private $url;

  /**
   * @var integer
   *
   * @ORM\Column(name="num_registros", type="integer")
   */
  private $numRegistros;

  /**
   * @Gedmo\Timestampable(on="create")
   * @ORM\Column(name="created", type="datetime")
   */
  private $created;

  /**
   * Get id
   *
   * @return integer 
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set usuario
   *
   * @param integer $usuario
   * @return LogDelete
   */
  public function setUsuario($usuario) {
    $this->usuario = $usuario;

    return $this;
  }

  /**
   * Get usuario
   *
   * @return integer 
   */
  public function getUsuario() {
    return $this->usuario;
  }

  /**
   * Set query
   *
   * @param string $query
   * @return LogDelete
   */
  public function setQuery($query) {
    $this->query = $query;

    return $this;
  }

  /**
   * Get query
   *
   * @return string 
   */
  public function getQuery() {
    return $this->query;
  }

  /**
   * Set url
   *
   * @param string $url
   * @return LogDelete
   */
  public function setUrl($url) {
    $this->url = $url;

    return $this;
  }

  /**
   * Get url
   *
   * @return string 
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * Set numRegistros
   *
   * @param integer $numRegistros
   * @return LogDelete
   */
  public function setNumRegistros($numRegistros) {
    $this->numRegistros = $numRegistros;

    return $this;
  }

  /**
   * Get numRegistros
   *
   * @return integer 
   */
  public function getNumRegistros() {
    return $this->numRegistros;
  }

  public function getCreated() {
    return $this->created;
  }

  public function setCreated($created) {
    $this->created = $created;
    return $this;
  }

}
