<?php

namespace CommonBundle\AdministratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Configuracion
 *
 * @ORM\Table(name="SFAPP_configuracion")
 * @ORM\Entity(repositoryClass="CommonBundle\AdministratorBundle\Entity\ConfigurationRepository")
 */
class Configuracion {

  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
   */
  private $nombre;

  /**
   * @var string
   *
   * @ORM\Column(name="logo", type="string", length=255, nullable=true)
   */
  private $logo;

  /**
   * @var string
   *
   * @ORM\Column(name="favicon", type="string", length=255, nullable=true)
   */
  private $favicon;

  /**
   * @var string
   *
   * @ORM\Column(name="email", type="string", length=255, nullable=true)
   * @Assert\Email()
   */
  private $email;

  /**
   * @var string
   *
   * @ORM\Column(name="web", type="string", length=255, nullable=true)
   * @Assert\Url()
   */
  private $web;

  /**
   * @var string
   *
   * @ORM\Column(name="sender_host", type="string", length=100, nullable=true)
   * 
   */
  private $senderHost;

  /**
   * @var string
   *
   * @ORM\Column(name="sender_encryption", type="string", length=3, nullable=true)
   * @Assert\Choice(choices = {"ssl", "tls", ""})
   */
  private $senderEncryption;

  /**
   * @var string
   *
   * @ORM\Column(name="sender_port", type="integer", length=3, nullable=false)
   * @Assert\Choice(choices = {25, 465})
   */
  private $senderPort;

  /**
   * @var string
   *
   * @ORM\Column(name="sender_email", type="string", length=255, nullable=true)
   * @Assert\Email()
   */
  private $senderEmail;

  /**
   * @var string
   *
   * @ORM\Column(name="sender_user", type="string", length=255, nullable=true)
   */
  private $senderUser;

  /**
   * @var string
   *
   * @ORM\Column(name="sender_pass", type="string", length=255, nullable=true)
   */
  private $senderPass;

  /**
   * @Assert\Image(maxSize="6000000")
   */
  private $file;

  /**
   * @Assert\Image(maxSize="6000000",mimeTypes="image/x-icon")
   */
  private $fileFavicon;

  /**
   * @ORM\Column(name="resetear_clabe", type="integer", nullable=true)
   */
  private $resetearClabe;

  /**
   * @ORM\Column(name="conexion_simultanea", type="boolean", options={"default": false}, nullable=true)
   */
  private $conexionSimultanea;

  /**
   * Sets file.
   *
   * @param UploadedFile $file
   */
  public function setFile(UploadedFile $file = null) {
    $this->file = $file;
  }

  /**
   * Get file.
   *
   * @return UploadedFile
   */
  public function getFile() {
    return $this->file;
  }

  /**
   * Sets file.
   *
   * @param UploadedFile $file
   */
  public function setFileFavicon(UploadedFile $file = null) {
    $this->fileFavicon = $file;
  }

  /**
   * Get file.
   *
   * @return UploadedFile
   */
  public function getFileFavicon() {
    return $this->fileFavicon;
  }

  /**
   * Get id
   *
   * @return integer 
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set nombre
   *
   * @param string $nombre
   * @return Configuracion
   */
  public function setNombre($nombre) {
    $this->nombre = $nombre;

    return $this;
  }

  /**
   * Get nombre
   *
   * @return string 
   */
  public function getNombre() {
    return $this->nombre;
  }

  /**
   * Set logo
   *
   * @param string $logo
   * @return Configuracion
   */
  public function setLogo($logo) {
    $this->logo = $logo;

    return $this;
  }

  /**
   * Get logo
   *
   * @return string 
   */
  public function getLogo() {
    return $this->logo;
  }

  /**
   * Set email
   *
   * @param string $email
   * @return Configuracion
   */
  public function setEmail($email) {
    $this->email = $email;

    return $this;
  }

  /**
   * Get email
   *
   * @return string 
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * Set web
   *
   * @param string $web
   * @return Configuracion
   */
  public function setWeb($web) {
    $this->web = $web;

    return $this;
  }

  /**
   * Get web
   *
   * @return string 
   */
  public function getWeb() {
    return $this->web;
  }

  /**
   * Set senderEmail
   *
   * @param string $senderEmail
   * @return Configuracion
   */
  public function setSenderEmail($senderEmail) {
    $this->senderEmail = $senderEmail;

    return $this;
  }

  /**
   * Get senderEmail
   *
   * @return string 
   */
  public function getSenderEmail() {
    return $this->senderEmail;
  }

  /**
   * Set senderUser
   *
   * @param string $senderUser
   * @return Configuracion
   */
  public function setSenderUser($senderUser) {
    $this->senderUser = $senderUser;

    return $this;
  }

  /**
   * Get senderUser
   *
   * @return string 
   */
  public function getSenderUser() {
    return $this->senderUser;
  }

  /**
   * Set senderPass
   *
   * @param string $senderPass
   * @return Configuracion
   */
  public function setSenderPass($senderPass) {
    if ($senderPass)
      $this->senderPass = md5($senderPass);

    return $this;
  }

  /**
   * Get senderPass
   *
   * @return string 
   */
  public function getSenderPass() {
    return $this->senderPass;
  }

  public function getSenderHost() {
    return $this->senderHost;
  }

  public function getSenderEncryption() {
    return $this->senderEncryption;
  }

  public function setSenderHost($senderHost) {
    $this->senderHost = $senderHost;
    return $this;
  }

  public function setSenderEncryption($senderEncryption) {
    $this->senderEncryption = $senderEncryption;
    return $this;
  }

  public function getSenderPort() {
    return $this->senderPort;
  }

  public function setSenderPort($senderPort) {
    $this->senderPort = $senderPort;
    return $this;
  }

  public function getAbsolutePath($favicon = false) {
    if($favicon){
      return null === $this->favicon ? null : $this->getUploadRootDir() . '/favicon.ico';
    }else{
      return null === $this->logo ? null : $this->getUploadRootDir() . '/' . $this->logo;
    }
  }

  public function getWebPath() {
    return null === $this->logo ? null : $this->getUploadDir() . '/' . $this->logo;
  }

  protected function getUploadRootDir() {
    // the absolute directory path where uploaded
    // documents should be saved
    return __DIR__ . '/../../../../web/' . $this->getUploadDir();
  }

  protected function getUploadDir() {
    // get rid of the __DIR__ so it doesn't screw up
    // when displaying uploaded doc/image in the view.
    return 'uploads/logos';
  }

  public function upload() {
    $file = $this->getFile();

    if (null === $file) {
      return;
    }

    $extension = $file->guessExtension();
    if (!$extension) {
      $extension = 'bin';
    }

    if ($this->logo) {
      @unlink($this->getAbsolutePath());
    }

    $this->logo = substr(md5(rand()), 0, 7) . '.' . $extension;

    $file->move($this->getUploadRootDir(), $this->logo);
    $this->file = null;
  }

  public function uploadFavicon() {
    $file = $this->getFileFavicon();

    if (null === $file) {
      return;
    }

    $extension = $file->guessExtension();
    if (!$extension) {
      $extension = 'bin';
    }

    if ($this->favicon) {
      @unlink($this->getAbsolutePath(true));
    }

    $this->favicon = 'favicon.ico';

    $file->move($this->getUploadRootDir(), $this->favicon);
    $this->file = null;
  }


    /**
     * Set favicon
     *
     * @param string $favicon
     * @return Configuracion
     */
    public function setFavicon($favicon)
    {
        $this->favicon = $favicon;

        return $this;
    }

    /**
     * Get favicon
     *
     * @return string 
     */
    public function getFavicon()
    {
        return $this->favicon;
    }

    /**
     * Set resetearClabe
     *
     * @param integer $resetearClabe
     * @return Configuracion
     */
    public function setResetearClabe($resetearClabe)
    {
        $this->resetearClabe = $resetearClabe;

        return $this;
    }

    /**
     * Get resetearClabe
     *
     * @return integer 
     */
    public function getResetearClabe()
    {
        return $this->resetearClabe;
    }

    /**
     * Set conexionSimultanea
     *
     * @param boolean $conexionSimultanea
     * @return Configuracion
     */
    public function setConexionSimultanea($conexionSimultanea)
    {
        $this->conexionSimultanea = $conexionSimultanea;

        return $this;
    }

    /**
     * Get conexionSimultanea
     *
     * @return boolean 
     */
    public function getConexionSimultanea()
    {
        return $this->conexionSimultanea;
    }
}
