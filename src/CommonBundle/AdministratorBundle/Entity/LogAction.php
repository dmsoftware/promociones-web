<?php

namespace CommonBundle\AdministratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Util\Util;

/**
 * LogAction
 *
 * @ORM\Table(name="SFAPP_logaction")
 * @ORM\Entity(repositoryClass="CommonBundle\AdministratorBundle\Entity\LogActionRepository")
 */
class LogAction
{
  
  /**
   * @ORM\Id
   * @ORM\Column(name="id", type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;
  
  /**
   * @var \CommonBundle\UserBundle\Entity\User
   *
   * @ORM\ManyToOne(targetEntity="CommonBundle\UserBundle\Entity\User")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="usuario", referencedColumnName="id", onDelete="CASCADE")
   * })
   */
  private $usuario;
  
  /**
   * @ORM\Column(name="entity_id", type="integer", nullable=true)
   */
  private $entity_id;
  
  /**
   * @ORM\Column(name="entity_class", type="text", length=255)
   */
  private $entity_class;
  
  /**
   * @ORM\Column(name="modified", type="array", nullable=true)
   */
  private $modified;
  
  /**
   * @Gedmo\Timestampable(on="create")
   * @ORM\Column(name="created", type="datetime")
   */
  private $created;
  
  /**
   * @ORM\Column(name="action", type="string", length=25)
   */
  private $action;
  
  /**
   * @ORM\Column(name="origin", type="string", length=255, nullable=true)
   */
  private $origin;
  
  public $em;
  
  public $new;
  
  /**
   * Get id
   *
   * @return integer
   */
  public function getId() {
    return $this->id;
  }
  
  /**
   * Set usuario
   *
   * @param integer $usuario
   * @return LogAction
   */
  public function setUsuario($usuario) {
    $this->usuario = $usuario;
    
    return $this;
  }
  
  /**
   * Get usuario
   *
   * @return integer
   */
  public function getUsuario() {
    return $this->usuario;
  }
  
  /**
   * Set entity_id
   *
   * @param integer $entityId
   * @return LogAction
   */
  public function setEntityId($entityId) {
    $this->entity_id = $entityId;
    
    return $this;
  }
  
  /**
   * Get entity_id
   *
   * @return integer
   */
  public function getEntityId() {
    return $this->entity_id;
  }
  
  /**
   * Set entity_class
   *
   * @param string $entityClass
   * @return LogAction
   */
  public function setEntityClass($entityClass) {
    $this->entity_class = $entityClass;
    
    return $this;
  }
  
  /**
   * Get entity_class
   *
   * @return string
   */
  public function getEntityClass() {
    return $this->entity_class;
  }
  
  /**
   * Set modified
   *
   * @param array $modified
   * @return LogAction
   */
  public function setModified($modified) {
    $this->modified = $modified;
    
    return $this;
  }
  
  /**
   * Get modified
   *
   * @return array
   */
  public function getModified() {
    return $this->modified;
  }
  
  /**
   * Set created
   *
   * @param \DateTime $created
   * @return LogAction
   */
  public function setCreated($created) {
    $this->created = $created;
    
    return $this;
  }
  
  /**
   * Get created
   *
   * @return \DateTime
   */
  public function getCreated() {
    return $this->created;
  }
  
  /**
   * Set action
   *
   * @param string $action
   * @return LogAction
   */
  public function setAction($action) {
    $this->action = $action;
    
    return $this;
  }
  
  /**
   * Get action
   *
   * @return string
   */
  public function getAction() {
    return $this->action;
  }

  public function renderEntityClass($params){
    $tmp = explode('\\', $this->entity_class);

    return $tmp[count($tmp) - 1];
  }

  public function renderModified($params){
    $txt = array();
    $labels = array();

    $rep = $params['em']->getRepository(\AppBundle\Util\Util::getEntityClass($this->entity_class));

    if($rep && method_exists($rep, 'getLabels')){
      $labels = $rep->getLabels();
    }

    if($this->action === 'edit'){
      foreach($this->modified as $col => $values){
        $ant = $values[0];
        $des = $values[1];

        if(is_object($ant)){
          if(get_class($ant) === 'DateTime'){
            $ant = $ant->format('d-m-Y');
          }else{
            if(!method_exists($ant, '__toString')){
              $ant = '';
            }
          }
        } 
        if(is_object($des)){
          if(get_class($des) === 'DateTime'){
            $des = $des->format('d-m-Y');
          }else{
            if(!method_exists($des, '__toString')){
              $des = '';
            }
          }
        } 

        if($ant != $des){
          $txt[] = '<b>' . $params['translator']->trans(ucfirst(isset($labels[$col]) ? $labels[$col] : $col)) . '</b>: <i>' . $ant . '</i> :::: ' . $des;
        }
      }
    }

    if($this->action === 'new'){
      foreach($this->modified as $col => $values){
        $des = $values[1];
 
        if(is_object($des)){
          if(get_class($des) === 'DateTime'){
            $des = $des->format('d-m-Y');
          }else{
            if(!method_exists($des, '__toString')){
              $des = '';
            }
          }
        } 

        $txt[] = '<b>' . $params['translator']->trans(ucfirst(isset($labels[$col]) ? $labels[$col] : $col)) . '</b>: ' . $des;
      }
    }

    if($this->action === 'delete'){
      foreach($this->modified as $col => $value){
        if(is_object($value)){
          if(get_class($value) === 'DateTime'){
            $value = $value->format('d-m-Y');
          }else{
            if(!method_exists($value, '__toString')){
              $value = '';
            }
          }
        } 

        $txt[] = '<b>' . $params['translator']->trans(ucfirst(isset($labels[$col]) ? $labels[$col] : $col)) . '</b>: ' . $value;
      }
    }

    $resp = implode(' | ', $txt);

    if(strlen($resp) > 100){
      $resp = '<div style="overflow: hidden; width: 32rem; display: inline-block; height: 1rem;">' . $resp . '</div><a style="color: black; font-size: 1rem;" onclick="verMasInfo(this);"><i class="fa fa-eye"></i></a><div class="infoLogAction hide">' . implode('<br/>', $txt). '</div>';
    }

    return $resp;
  }

  public function createLogDeleteObject($user, $entity, $em, $origin = null){
    $this->setUsuario($user);
    $this->setAction('delete');

    if(method_exists($entity, 'getId')){
      $this->setEntityId($entity->getId());
    }

    $this->setEntityClass(get_class($entity));

    if($origin){
      $tmp = explode('\\', get_class($origin));
      $originClass = $tmp[count($tmp) - 1];

      if(method_exists($origin, 'getId')){
        $originClass .= ' ('. $origin->getId() . ')';
      }

      $this->setOrigin($originClass);
    }

    $datos = array();
    $rep = $em->getRepository(Util::getEntityClass(get_class($entity)));

    foreach($rep->getFieldMapping() as $col => $data){
      $fn = Util::getObjectFieldMethodName($col);
      if(method_exists($entity, $fn)){
        $datos[$col] = $entity->$fn();
      }
    }

    $this->setModified($datos);

    return $this;
  }

    /**
     * Set origin
     *
     * @param string $origin
     * @return LogAction
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * Get origin
     *
     * @return string 
     */
    public function getOrigin()
    {
        return $this->origin;
    }
}
