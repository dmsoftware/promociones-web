<?php

namespace CommonBundle\AdministratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Util\Util;

/**
 * IpControl
 *
 * @ORM\Table(name="SFAPP_ipcontrol")
 * @ORM\Entity(repositoryClass="CommonBundle\AdministratorBundle\Entity\IpControlRepository")
 */
class IpControl
{
  
  /**
   * @ORM\Id
   * @ORM\Column(name="id", type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;
  
  /**
   * @ORM\Column(name="ip", type="string", length=15)
   */
  private $ip;

  /**
   * @ORM\Column(name="observaciones", type="text", nullable=true)
   */
  private $observaciones;
  
  /**
   * @Gedmo\Timestampable(on="create")
   * @ORM\Column(name="created", type="datetime")
   */
  private $created;

  /**
   * @ORM\Column(name="updated", type="datetime")
   * @Gedmo\Timestampable(on="update")
   */
  private $updated;
  
  /**
   * Get id
   *
   * @return integer
   */
  public function getId() {
    return $this->id;
  }

    /**
     * Set ip
     *
     * @param string $ip
     * @return IpControl
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return IpControl
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return IpControl
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set observaciones
     *
     * @param string $observaciones
     * @return IpControl
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string 
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }
}
