<?php

namespace CommonBundle\AdministratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * LogAccess
 *
 * @ORM\Table(name="SFAPP_logacces")
 * @ORM\Entity(repositoryClass="CommonBundle\AdministratorBundle\Entity\LogAccessRepository")
 */
class LogAccess
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
   * @var \CommonBundle\UserBundle\Entity\User
   *
   * @ORM\ManyToOne(targetEntity="CommonBundle\UserBundle\Entity\User")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="usuario", referencedColumnName="id", onDelete="CASCADE")
   * })
   */
    private $usuario;

    /**
     * @var string
     *
     * @ORM\Column(name="accion", type="string", length=10)
     */
    private $accion;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=20, nullable=true)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="navegador", type="text", nullable=true)
     */
    private $navegador;

    /**
   * @Gedmo\Timestampable(on="create")
   * @ORM\Column(name="created", type="datetime")
   */
    private $created;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usuario
     *
     * @param integer $usuario
     * @return LogAccess
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return integer 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set accion
     *
     * @param string $accion
     * @return LogAccess
     */
    public function setAccion($accion)
    {
        $this->accion = $accion;

        return $this;
    }

    /**
     * Get accion
     *
     * @return string 
     */
    public function getAccion()
    {
        return $this->accion;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return LogAccess
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set navegador
     *
     * @param string $navegador
     * @return LogAccess
     */
    public function setNavegador($navegador)
    {
        $this->navegador = $navegador;

        return $this;
    }

    /**
     * Get navegador
     *
     * @return string 
     */
    public function getNavegador()
    {
        return $this->navegador;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return LogAccess
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }
}
