var txt_global_error = "Arazo bat gertatu da.";
var txt_data_tables = {
	"sProcessing": "Prozesatzen...",
	"sLengthMenu": "Ikusi <select class='JSselect2No'><option value='10'>10</option><option value='25'>25</option><option value='50'>50</option><option value='100'>100</option><option value='200'>200</option><option value='500'>500</option><option value='-1'>Guztiak</option></select> erregistro",
	"sZeroRecords": "Ez dira erregistrorik aurkitu",
	"sInfo": "_START_ tik _END_ ra _TOTAL_ erregistrotik",
	"sInfoEmpty": "0 tik 0 ra 0 erregistrotik",
	"sInfoFiltered": "",
	"sInfoPostFix": "",
	"sSearch": "Bilatu:",
	"sUrl": "",
	"oPaginate": {
		"sFirst":    "<< Lehenengoa",
		"sPrevious": "< Aurrekoa",
		"sNext":     "Hurrengoa >",
		"sLast":     "Azkena >>"
	
	}
};
var txt_date_picker = {
  format: 'Y-m-d', //Tiene que tener el mismo formato en la clase AppBundle\Util\Util en la función getDatePickerFormat
  weekStart: 1
};
var txt_delete_selected = "Aukeratutako elementuak ezabatuko dituzu. Jarraitu nahi duzu?";
var txt_remove_rel_form = "Erlazioa ezabatu";
var txt_add_rel_form = "Erlazioa gehitu";
var txt_add_object = "Elementua gehitu";
var txt_title_form_change = "Koloreztatuta dauden atalak aldaketak dituzte baina gorde gabe daude.";