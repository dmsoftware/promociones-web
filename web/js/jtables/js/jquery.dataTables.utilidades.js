/*plugin para ordenar numeros con comas eliminamos puntos y comas*/
	jQuery.fn.dataTableExt.oSort['formatted-num-asc'] = function(x,y){
		x = x.replace( '.', ',' ).replace(/[^\d\-\.\/]/g,'');
		y = y.replace( '.', ',' ).replace(/[^\d\-\.\/]/g,'');
		if(x.indexOf('/')>=0)x = eval(x);
		if(y.indexOf('/')>=0)y = eval(y);
		return x/1 - y/1;
	}
	jQuery.fn.dataTableExt.oSort['formatted-num-desc'] = function(x,y){
		x = x.replace( '.', ',' ).replace(/[^\d\-\.\/]/g,'');
		y = y.replace( '.', ',' ).replace(/[^\d\-\.\/]/g,'');
		if(x.indexOf('/')>=0)x = eval(x);
		if(y.indexOf('/')>=0)y = eval(y);
		return y/1 - x/1;
	}
	
	jQuery.fn.dataTableExt.oSort['uk_date-asc']  = function(a,b) {
		var ukDatea = a.split('/');
		var ukDateb = b.split('/');
	
		var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
		var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;
	
		return ((x < y) ? -1 : ((x > y) ?  1 : 0));
	};

	jQuery.fn.dataTableExt.oSort['uk_date-desc'] = function(a,b) {
		var ukDatea = a.split('/');
		var ukDateb = b.split('/');
	
		var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
		var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;
	
		return ((x < y) ? 1 : ((x > y) ?  -1 : 0));
	};

jQuery.fn.dataTableExt.oApi.fnSetFilteringDelay = function ( oSettings, iDelay ) {
    var _that = this;
 
    if ( iDelay === undefined ) {
        iDelay = 250;
    }
      
    this.each( function ( i ) {
        $.fn.dataTableExt.iApiIndex = i;
        var
            $this = this,
            oTimerId = null,
            sPreviousSearch = null,
            anControl = $( 'input', _that.fnSettings().aanFeatures.f );
          
            anControl.unbind( 'keyup' ).bind( 'keyup', function() {
            var $$this = $this;
  
            if (sPreviousSearch === null || sPreviousSearch != anControl.val()) {
                window.clearTimeout(oTimerId);
                sPreviousSearch = anControl.val(); 
                oTimerId = window.setTimeout(function() {
                    $.fn.dataTableExt.iApiIndex = i;
                    _that.fnFilter( anControl.val() );
                }, iDelay);
            }
        });
          
        return this;
    } );
    return this;
};