var txt_global_error = "Ha ocurrido un error y no se ha podido ejecutar la acción.";
var txt_data_tables = {
	"sProcessing": "Procesando...",
	"sLengthMenu": "Mostrar <select class='JSselect2No'><option value='10'>10</option><option value='25'>25</option><option value='50'>50</option><option value='100'>100</option><option value='200'>200</option><option value='500'>500</option><option value='-1'>Todos</option></select> registros",
	"sZeroRecords": "No se encontraron resultados",
	"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
	"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
	"sInfoFiltered": "",
	"sInfoPostFix": "",
	"sSearch": "Buscar:",
	"sUrl": "",
	"oPaginate": {
		"sFirst":    "<< Primero",
		"sPrevious": "< Anterior",
		"sNext":     "Siguiente >",
		"sLast":     "Ultimo >>"
	
	}
};
var txt_date_picker = {
  format: 'd-m-Y', //Tiene que tener el mismo formato en la clase AppBundle\Util\Util en la función getDatePickerFormat
  weekStart: 1
};
var txt_delete_selected = "Va a eliminar los registros seleccionados. ¿Desea continuar?";
var txt_remove_rel_form = "Quitar relación";
var txt_add_rel_form = "Añadir relación";
var txt_add_object = "Añadir elemento";
var txt_title_form_change = "Los campos coloreados han sido modificados pero todavia no se han guardado los cambios.";