var info_archivo = new Array();


$(document).ready(function(e){
	//console.log('kop:'+$('.archivo_content').length);
	if($('.archivo_content').length > 0)
	{
		init_archivo_configuration();
	}
});
if($('.archivo_content').length > 0)
{
	$(document).on('click','.archivo_icon i.fa',function(e){
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		$(this).closest('.archivo_widget').find('.archivo_link a').trigger("click");
	});
	$(document).on('click','.archivo_thumbnail',function(e){
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		$(this).closest('.archivo_widget').find('.archivo_link a').trigger("click");
	});
	$(document).on('click','.archivo_link a',function(e){
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		window.open($(this).attr('href'));
	});


	$(document).on('click','.add_archivo',function(e){
		e.stopPropagation();
		e.stopImmediatePropagation();
		$(this).parent().find('.new_archivo').trigger("click");
	});
	var info_img;
	$(document).on('click','.new_archivo',function(e){
		e.stopPropagation();
		e.stopImmediatePropagation();
		var parent = $(this).parent();
		var contentId = parent.find('.archivo_content').attr('id');
		if((parent.find('.archivo_widget').length >= info_archivo[contentId]['file_count']) && (info_archivo[contentId]['file_count'] != null))
		{
			parent.closest('fieldset').find('.archivo_error').text(info_archivo[contentId]['count_error']);
			parent.closest('fieldset').find('.archivo_error').fadeIn('fast').delay(5000).fadeOut('fast');
			e.preventDefault();
			return false;
		}
		else
		{
			return true;
		}
	});
	$(document).on('change','.new_archivo',function(e){
		e.stopPropagation();
		e.stopImmediatePropagation();
		var widget = $(this).parent().find('.archivo_content');
		var widgetId = widget.attr('id');
		var entity = widget.closest('.reveal-modal').find('.head_info_default').attr('data-class');
		var entity_id = widget.closest('.reveal-modal').find('.head_info_default').attr('data-id');
		var fieldName = widgetId.split('_');
		var fileElementId = $(this).attr('id');
		$.ajaxFileUpload({
			url         : "/archivo/upload",
			secureuri      :false,
			fileElementId  : fileElementId,
			dataType    : 'json',
			data        : {
				'file_extensiones':info_archivo[widgetId]['file_extensiones'],
				'file_size':info_archivo[widgetId]['file_size'],
				'file_resolution':info_archivo[widgetId]['file_resolution'],
				'fileElementId':fileElementId,
				'entity':entity,
				'entity_id':entity_id,
				'fieldName':fieldName[fieldName.length - 1]
			},
			success  : function (data, status)
			{
				var info = $.parseJSON(data.info);
				if(data.status != 'error')
				{
					var type = (widget.attr('data-image') == true) ? 'true' : 'false';
					if(type == 'true')
					{
						if(info_archivo[widgetId]['file_resolution'] != null)
							{manipulate_image(info,widget);}
						else
							{no_manipulate_image(info,widget);}
						info_img = info;
					}
					else
					{
						render_new_archivo(info,widget);
					}
				}
				else
				{
					if(data.error_code == '1') { widget.closest('fieldset').find('.archivo_error').html($('.archivo_translate_collection .error_code_1').html()); }
					else if(data.error_code == '2') { widget.closest('fieldset').find('.archivo_error').html($('.archivo_translate_collection .error_code_2').html()); }
					else if(data.error_code == '3') { widget.closest('fieldset').find('.archivo_error').html(info_archivo[widgetId]['extensiones_error']); }
					else if(data.error_code == '4') { widget.closest('fieldset').find('.archivo_error').html(info_archivo[widgetId]['size_error']); }
					else if(data.error_code == '5') { widget.closest('fieldset').find('.archivo_error').html(info_archivo[widgetId]['resolution_error']); }
					else { widget.closest('fieldset').find('.archivo_error').html($('.archivo_translate_collection .error_code_0').html()); }
					widget.closest('fieldset').find('.archivo_error').fadeIn('fast').delay(5000).fadeOut('fast');
				}
			}
		});
	});

	$(document).on('click','.archivo_edit',function(e){
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
		$(this).closest('.archivo_widget').find('.archivo_file').first().trigger("click");
	});
	$(document).on('change','.archivo_file',function(e){
		e.stopPropagation();
		e.stopImmediatePropagation();
		var widget = $(this).closest('.archivo_content');
		var widgetId = widget.attr('id');
		var parent = $(this).closest('.archivo_widget');
		parent.find('.archivo_loading').first().fadeIn('fast');
		var entity_id = $(this).closest('.archivo_widget').attr('data-id');
		var id = $(this).attr('id');
		var parent = $(this).closest('.archivo_widget');
		$.ajaxFileUpload({
			url         : "/archivo/uploadedit",
			secureuri      :false,
			fileElementId  : id,
			dataType    : 'json',
			data        : {
				'file_extensiones':info_archivo[widgetId]['file_extensiones'],
				'file_size':info_archivo[widgetId]['file_size'],
				'file_resolution':info_archivo[widgetId]['file_resolution'],
				'entity_id':entity_id
			},
			success  : function (data, status)
			{
				var info = $.parseJSON(data.info);
				if(data.status != 'error')
				{
					var type = (widget.attr('data-image') == true) ? 'true' : 'false';
					if(type == 'true')
					{
						if(info_archivo[widgetId]['file_resolution'] != null)
							{manipulate_image(info,widget);}
						else
							{no_manipulate_image(info,widget);}
						info_img = info;
					}
					else
					{
						no_manipulate_image(info,widget,true);
					}
				}
				else
				{
					if(data.error_code == '1') { widget.closest('fieldset').find('.archivo_error').html($('.archivo_translate_collection .error_code_1').html()); }
					else if(data.error_code == '2') { widget.closest('fieldset').find('.archivo_error').html($('.archivo_translate_collection .error_code_2').html()); }
					else if(data.error_code == '3') { widget.closest('fieldset').find('.archivo_error').html(info_archivo[widgetId]['extensiones_error']); }
					else if(data.error_code == '4') { widget.closest('fieldset').find('.archivo_error').html(info_archivo[widgetId]['size_error']); }
					else if(data.error_code == '5') { widget.closest('fieldset').find('.archivo_error').html(info_archivo[widgetId]['resolution_error']); }
					else { widget.closest('fieldset').find('.archivo_error').html($('.archivo_translate_collection .error_code_0').html()); }
					widget.closest('fieldset').find('.archivo_error').fadeIn('fast').delay(5000).fadeOut('fast');
				}
				parent.find('.archivo_loading').first().fadeOut('fast');
			}
		});
	});


	$(document).on('click','.archivo_remove',function(e){
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();

		if(confirm("Desea eliminar el archivo?"))
		{
			var that = $(this);
			var widget = that.closest('.archivo_content');
			var parent = that.closest('.archivo_widget');
			parent.closest('.archivo_widget').find('.archivo_loading').fadeIn('fast');
			var entity = widget.closest('.reveal-modal').find('.head_info_default').attr('data-class');
			var entity_id = widget.closest('.reveal-modal').find('.head_info_default').attr('data-id');
			var field_id = that.closest('.archivo_widget').attr('data-id');
			var fieldName = widget.attr('id').split('_');
			var url = "/archivo/remove";
			var data = {
				field_id:field_id
				,fieldName:fieldName[fieldName.length - 1]
				,entity:entity
				,entity_id:entity_id
			}
			var post = $.post(url, data);
			post.done(function(data){
				if(data.status != 'error') {
					parent.remove();
				}
			}).fail(function(){
				console.log($('.archivo_translate_collection .error_code_0').text());
			}).always(function(){
				that.closest('.archivo_widget').find('.archivo_loading').fadeOut('fast');
			});
		}
	});

	$(document).on('click','.close-reveal-modal',function(e){
		load_info_archivo();
	});


	$(document).on('click','.button_content .cancel', function(e){
		e.stopPropagation();
		e.stopImmediatePropagation();
		var id_img = $(this).closest('.manipulate_image_content').attr('data-id');
		var id_old = $(this).closest('.manipulate_image_content').attr('data-id_old');
		var widgetId = $(this).closest('.manipulate_image_content').attr('data-id_widget');
		var widget = $('.archivo_content[id="'+widgetId+'"]');
		var entity = widget.closest('.reveal-modal').find('.head_info_default').attr('data-class');
		var entity_id = widget.closest('.reveal-modal').find('.head_info_default').attr('data-id');
		var fieldName = widgetId.split('_');
		if(confirm("Desea eliminar los cambios?"))
		{
			var parent = $('.archivo_widget[data-id="'+id_img+'"]');
			var url = $(this).attr('data-url');
			var data = {
				entity:entity
				,entity_id:entity_id
				,fieldName:fieldName[fieldName.length - 1]
			}
			var post = $.post(url,data);
			post.done(function(data){
				if(data.status != 'error') {
					var data = $.parseJSON(data);
					//console.log(data);
				}
			}).fail(function(){
				console.log($('.archivo_translate_collection .error_code_0').text());
			});
		}
		$('.manipulate_image_content_back').remove();
	});
	$(document).on('click','.button_content .change', function(e){
		e.stopPropagation();
		e.stopImmediatePropagation();
		var parent = $(this).closest('.manipulate_image_content');
		var id_img = $(this).closest('.manipulate_image_content').attr('data-id');
		var id_old = $(this).closest('.manipulate_image_content').attr('data-id_old');
		var widgetId = $(this).closest('.manipulate_image_content').attr('data-id_widget');
		var id = (id_old == '0') ? id_img : id_old;
		var widget = $('.archivo_content[id="'+widgetId+'"]');
		var resolution = (info_archivo[widgetId]['file_resolution'] != null) ? info_archivo[widgetId]['file_resolution'].split('x') : null;

		var entity = widget.closest('.reveal-modal').find('.head_info_default').attr('data-class');
		var entity_id = widget.closest('.reveal-modal').find('.head_info_default').attr('data-id');
		var fieldName = widgetId.split('_');

		if(id_old == '0')
		{
			render_new_archivo(info_img,widget);
			load_info_archivo();
		}
		else
		{
			//var parent = $('.archivo_widget[data-id="'+id_old+'"]');
			//render_update_archivo(parent,info_img,widget);
			//load_info_archivo();
		}

		var w = (info_archivo[widgetId]['file_resolution'] != null) ? parseInt(resolution[0]) : null;
		var h = (info_archivo[widgetId]['file_resolution'] != null) ? parseInt(resolution[1]) : null;
		var url = "/archivo/resizeimg";
		var data = {
			id_img:id_img
			,id_old:id_old
			,coord_x:parseFloat($('.image_resize').css('left'))
			,coord_y:parseFloat($('.image_resize').css('top'))
			,crop_w:$('.image_resize').width()
			,crop_h:$('.image_resize').height()
			,resolution_w:w
			,resolution_h:h
			,fieldName:fieldName[fieldName.length - 1]
			,entity:entity
			,entity_id:entity_id
		}
		var post = $.post(url, data);
		post.done(function(data){
			var data = $.parseJSON(data);
			var info = $.parseJSON(data.info);
			if(data.status != 'error')
			{
				if(id_old == '0')
				{
					var id = id_img;
				}
				else
				{
					var id = id_old;
				}
				var src = $('.archivo_widget[data-id="'+id+'"]').find('.archivo_thumbnail').first().attr('src');
				$('.archivo_widget[data-id="'+id+'"]').find('.archivo_thumbnail').first().attr('src', info.path + '?t=' + new Date().getTime());
				var fileName = info.path.split('\/');
				$('.archivo_widget[data-id="'+id+'"]').find('.archivo_link > a').first().text(fileName[fileName.length - 1]);
				$('.archivo_widget[data-id="'+id+'"]').find('.archivo_link > a').first().attr('href', info.path + '?t=' + new Date().getTime());
				var s = info.path.replace(/^\/+/, "");
				$('.archivo_widget[data-id="'+id+'"]').find('.archivo_hidden_file').first().val(s + '?t=' + new Date().getTime());
				$('.manipulate_image_content_back').remove();
				load_info_archivo();
			}
			else
			{
				if(data.error_code == '1') { widget.closest('fieldset').find('.archivo_error').html($('.archivo_translate_collection .error_code_1').html()); }
				else if(data.error_code == '2') { widget.closest('fieldset').find('.archivo_error').html($('.archivo_translate_collection .error_code_2').html()); }
				else if(data.error_code == '3') { widget.closest('fieldset').find('.archivo_error').html($('.archivo_translate_collection .error_generate_file').html()); }
				else { widget.closest('fieldset').find('.archivo_error').html($('.archivo_translate_collection .error_code_0').html()); }
				widget.closest('fieldset').find('.archivo_error').fadeIn('fast').delay(5000).fadeOut('fast');
				$('.manipulate_image_content_back').remove();
			}
		}).fail(function(){
			console.log($('.archivo_translate_collection .error_code_0').text());
		}).always(function(){

		});
	});

	$(document).on('click','form button[type="submit"]',function(e){
		var form = $(this).closest('form');
		var widget = form.find('.archivo_content');
		if (widget.length) {
			e.preventDefault();
			e.stopPropagation();
			e.stopImmediatePropagation();
			$('.new_archivo').remove();
			$('.archivo_content').each(function(e) {
				var t = $(this).attr('id').split('_');
				var formEntity = t[0];
				var field = t[1];
				$(this).append('<select name="'+formEntity+'['+field+'][]" multiple class="archivo_list archivo_list_'+field+' hide"></select>');
				$(this).find('.archivo_widget').each(function(){
					$('.archivo_list_'+field).append('<option value="'+$(this).attr('data-id')+'" selected="selected">'+$(this).attr('data-id')+'</option>');
				});
			});
			form.submit();
		}
	});
}
/******************** FUNCIONES *******************/
function init_archivo_configuration()
{
	$('.archivo_content').each(function(e) {
		var that = $(this);
		var id = that.attr('id');
		var berri = that.parent().find('input[name="new_archivo"]');
		var berri_izen = 'new_archivo_' + id;
		berri.attr('id',berri_izen);
		berri.attr('name',berri_izen);
		info_archivo[id] = new Array();
		info_archivo[id]['file_size'] = ($(this).attr('data-max_file_size') !== undefined) ? $(this).attr('data-max_file_size') : null;
		info_archivo[id]['file_count'] = ($(this).attr('data-max_file_count') !== undefined) ? $(this).attr('data-max_file_count') : null;
		info_archivo[id]['size_error'] = ($(this).attr('data-max_size_error') !== undefined) ? $(this).attr('data-max_size_error') : $('.archivo_translate_collection .error_size').html();
		info_archivo[id]['count_error'] = ($(this).attr('data-max_count_error') !== undefined) ? $(this).attr('data-max_count_error') : $('.archivo_translate_collection .error_count').html();
		info_archivo[id]['file_extensiones'] = ($(this).attr('data-extensiones') !== undefined) ? $(this).attr('data-extensiones') : null;
		info_archivo[id]['extensiones_error'] = ($(this).attr('data-extensiones_error') !== undefined) ? $(this).attr('data-extensiones_error') : $('.archivo_translate_collection .error_extension').html();
		info_archivo[id]['file_resolution'] = ($(this).attr('data-max_resolution') !== undefined) ? $(this).attr('data-max_resolution') : null;
		info_archivo[id]['resolution_error'] = ($(this).attr('data-resolution_error') !== undefined) ? $(this).attr('data-resolution_error') : $('.archivo_translate_collection .error_resolution').html();
		if(!$(this).closest('fieldset').find('.archivo_error').length)
		{
			that.closest('fieldset').prepend('<div class="archivo_error alert-box alert"></div>');
		}

		that.closest('.entity_form_row').removeClass('entity_form_row').addClass('archivo_single_content');
		$(this).sortable({
			containment: $(this).closest('fieldset')
			,stop: function(event, ui){
				save_img_order(ui.item[0].parentNode.id);
			}
		});
		$(this).disableSelection();
	});
	load_info_archivo();
	set_all_img_order();
}
function set_all_img_order()
{
	$('.archivo_content').each(function(){
		if($(this).find('.archivo_widget[data-orden="0"]').length > 0)
		{
			set_img_order($(this));
		}
		save_img_order($(this).attr('id'));
		order_all_img($(this));
	});
}
function order_all_img(element)
{
	var $wrapper = element;
	$wrapper.find('.archivo_widget').sort(function (a, b) {
		return +a.dataset.orden - +b.dataset.orden;
	}).appendTo( $wrapper );
}
function set_img_order(element)
{
	var k = 1;
	element.find('.archivo_widget').each(function(){
		$(this).attr('data-orden',k);
		k = k + 1;
	});
}
function save_img_order(element)
{
	var orden = '';
	var lag = new Array();
	$('#'+element).find('.archivo_widget').each(function(){
		lag.push($(this).attr('data-id'));
	});
	orden = lag.join(',');


	var url = "/archivo/saveimgorder";
	var data = {
		id_array:orden
	}
	$('#cargando').show();
	var post = $.post(url, data);
	post.done(function(data){
		set_img_order($('#'+element));
		$('#cargando').hide();
	}).fail(function(){
		$('#cargando').hide();
		console.log($('.archivo_translate_collection .error_code_0').text());
	});
}
function load_info_archivo()
{
	$('.archivo_content').each(function(e){
		var that = $(this);
		var type = (that.attr('data-image') == true) ? 'true' : 'false';
		var entity = that.closest('.reveal-modal').find('.head_info_default').attr('data-class');
		var entity_id = that.closest('.reveal-modal').find('.head_info_default').attr('data-id');
		var fieldName = that.attr('id').split('_');
		var url = "/archivo/loadinfo";
		var data = {
			type:type
			,fieldName:fieldName[fieldName.length - 1]
			,entity:entity
			,entity_id:entity_id
		}
		var post = $.post(url, data);
		post.done(function(data){
			var data = $.parseJSON(data);
			var info = $.parseJSON(data.info);
			if(data.status != 'error') {
				$.each(info, function(i, item) {
					var uneko = $('.archivo_widget[data-id="'+item.id+'"]');
					if(uneko.length > 0)
					{
						uneko.attr('data-orden',item.orden);
						uneko.find('.archivo_info_alt').text(item.alt);
						uneko.find('.archivo_info_size_weight').text('('+item.size+')');
						uneko.find('.archivo_info_size_resolution').text(item.resolution);
						uneko.find('.archivo_thumbnail').addClass(item.class);
					}
				});
				order_all_img(that);
			}
			else
			{
				if(data.error_code == '1') { that.closest('.archivo_error').html($('.archivo_translate_collection .error_code_1').html()); }
				else if(data.error_code == '2') { that.closest('.archivo_error').html($('.archivo_translate_collection .error_code_2').html()); }
				else { that.closest('.archivo_error').html($('.archivo_translate_collection .error_code_0').html()); }
				that.closest('.archivo_error').fadeIn('fast').delay(5000).fadeOut('fast');
			}
		}).fail(function(){
			console.log($('.archivo_translate_collection .error_code_0').text());
		});
	});
}

function no_manipulate_image(info,widget,noImage=false)
{
	var id_img = info.id;
	var id_old = info.id_old;

	if(id_old == '0')
	{
		render_new_archivo(info,widget);
		load_info_archivo();
	}
	else
	{
		//var parent = $('.archivo_widget[data-id="'+id_old+'"]');
		//render_update_archivo(parent,info_img,widget);
		//load_info_archivo();
	}

	var url = "/archivo/resizeimg";
	var data = {
		id_img:id_img
		,id_old:id_old
	}
	var post = $.post(url, data);
	post.done(function(data){
		var data = $.parseJSON(data);
		var info = $.parseJSON(data.info);
		if(id_old == '0')
		{
			var id = id_img;
		}
		else
		{
			var id = id_old;
		}
		if(data.status != 'error')
		{
			var src = $('.archivo_widget[data-id="'+id+'"]').find('.archivo_thumbnail').first().attr('src');
			if(noImage)
			{
				$('.archivo_widget[data-id="'+id+'"]').find('.archivo_thumbnail').first().attr('src', '/uploads/no-image.png?t=' + new Date().getTime());
			}
			else
			{
				$('.archivo_widget[data-id="'+id+'"]').find('.archivo_thumbnail').first().attr('src', info.path + '?t=' + new Date().getTime());
			}
			var fileName = info.path.split('\/');
			$('.archivo_widget[data-id="'+id+'"]').find('.archivo_link > a').first().text(fileName[fileName.length - 1]);
			$('.archivo_widget[data-id="'+id+'"]').find('.archivo_link > a').first().attr('href', info.path + '?t=' + new Date().getTime());
			var s = info.path.replace(/^\/+/, "");
			$('.archivo_widget[data-id="'+id+'"]').find('.archivo_hidden_file').first().val(s + '?t=' + new Date().getTime());
			$('.manipulate_image_content_back').remove();
		}
		else
		{
			var errorTag = $('.archivo_widget[data-id="'+id+'"]').closest('.archivo_error');
			if(data.error_code == '1') { errorTag.html($('.archivo_translate_collection .error_code_1').html()); }
			else if(data.error_code == '2') { errorTag.html($('.archivo_translate_collection .error_code_2').html()); }
			else if(data.error_code == '3') { errorTag.html($('.archivo_translate_collection .error_generate_file').html()); }
			else { errorTag.html($('.archivo_translate_collection .error_code_0').html()); }
			errorTag.fadeIn('fast').delay(5000).fadeOut('fast');
			$('.manipulate_image_content_back').remove();
		}
	}).fail(function(){
		console.log($('.archivo_translate_collection .error_code_0').text());
	}).always(function(){

	});
}

function manipulate_image(info,widget)
{
	var ext_permitidas_para_resize = ['jpg', 'jpeg', 'gif', 'png'];
	var lag1 = info.path.split('.');
	var ext_file = lag1[lag1.length - 1].toLowerCase();
	var widgetId = widget.attr('id');
	if($.inArray(ext_file,ext_permitidas_para_resize) >= 0)
	{
		var resolution = (info_archivo[widgetId]['file_resolution'] != null) ? info_archivo[widgetId]['file_resolution'].split('x') : null;
		var w = (info_archivo[widgetId]['file_resolution'] != null) ? parseInt(resolution[0]) : 100;
		var h = (info_archivo[widgetId]['file_resolution'] != null) ? parseInt(resolution[1]) : 100;
		var back = $('<div class="manipulate_image_content_back"></div>');
		var content = $('<div class="manipulate_image_content" data-id="'+info.id+'" data-id_old="'+info.id_old+'" data-id_widget="'+widgetId+'"></div>');
		var resize_content = $('<div class="resize_content"></div>');
		var button_content = $('<div class="button_content"></div>');
		var button_change = $('<input type="button" class="button left change small" value="Guardar"/>');
		var button_cancel = $('<input type="button" data-url="/archivo/'+info.id+'/delete" class="button left cancel small" value="Cancelar"/>');
		var image = $('<img src="/'+info.path+'?t=' + new Date().getTime() + '">');
		var resize = $('<div class="image_resize"></div>');
		var div = $('<div class="image_resize_content"></div>');
		div.append(image);
		div.append(resize);
		resize_content.append(div);
		content.append(resize_content);
		button_content.append(button_change);
		button_content.append(button_cancel);
		content.append(button_content);
		back.append(content);
		$('body').append(back);

		$(image).load(function(){
			$('.image_resize').width(w);
			$('.image_resize').height(h);
			var ratioa = (info_archivo[widgetId]['file_resolution'] != null) ? w / h : false;
			$( ".image_resize" ).css({'top': '0px', 'left': '0px'});
			$( ".image_resize" ).resizable({
				containment: $('.image_resize_content'),
				aspectRatio: ratioa,
				minHeight: h,
				minWidth: w,
				stop: function(){
					var finalOffset = $(this).position();
				}
			});
			$('.image_resize').draggable(
			{
				containment: $('.image_resize_content')
			});
		});
	}
	else
	{
		if(info.id_old == '0')
		{
			render_new_archivo(info,widget);
		}
		else
		{
			no_manipulate_image(info,widget,true);
		}
		load_info_archivo();
	}
};
function render_update_archivo(parent,info,widget) {
	var type = widget.attr('data-image');
	var file_name = info.path.split('/');
	if(type == true)
	{
		parent.find('.archivo_thumbnail').attr('src','/'+info.path);
	}
	else
	{
		var icon = render_icon(info.path);
		parent.find('.archivo_icon > i').removeClass().addClass(icon);
	}
	parent.find('.archivo_link > a').attr('href','/'+info.path);
	parent.find('.archivo_hidden_file').val(info.path);
	parent.find('.archivo_link > a').text(file_name[file_name.length - 1]);
}

function render_new_archivo(info,widget) {
	var type = widget.attr('data-image');
	var file_name = info.path.split('/');
	var struct = (widget.attr('data-prototype'));
	console.log(info);
	if (type == true) {
		struct = struct.replace('src="/"','src="/'+info.path+'"');
		struct = struct.replace('data-url="/archivo//edit"','data-url="/archivo/'+info.id+'/edit"');
	}
	else
	{
		var icon = render_icon(info.path);
		struct = struct.replace('<i class="fa fa-file"></i>','<i class="'+icon+'"></i>');
	}
	struct = struct.replace('></a>','>'+file_name[file_name.length - 1]+'</a>');
	struct = struct.replace('<a href="/"','<a href="/'+info.path+'"');
	struct = struct.replace('class="archivo_hidden_file" type="hidden" value=""','class="archivo_hidden_file" type="hidden" value="'+info.path+'"');
	struct = struct.replace(/__name__/g,info.id);
	widget.append($(struct));
}

function render_icon(name) {
	var lag = name.split('.');
	var ext = lag[lag.length - 1].toLowerCase();
	$result = '';
	switch (ext) {
		case 'pdf':
			$result = 'fa fa-file-pdf-o';
			break;
		case 'xls':
		case 'xlsx':
			$result = 'fa fa-file-excel-o';
			break;
		case 'doc':
		case 'docx':
			$result = 'fa fa-file-word-o';
			break;
		default:
			$result = 'fa fa-file-o';
	}
	return $result;
}

function imgError(image) {
	 image.onerror = "";
	 image.src = "/uploads/no-image.png";
	 return true;
}
$(document).on('error','img',function(e){
	var errorImg = "/uploads/no-image.png";
	$(this).attr('src', errorImg);
});