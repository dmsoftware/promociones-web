function UtilClass() {
  var _self = this;

  this.documentReadyFunctions = function () {
    _self.menuBreadcrumbs();
    _self.objectRelCreation();
    _self.formChanges();
    _self.initSelect2();
    _self.initColorPicker();
    _self.initDatePicker();
    _self.timerAlertBox();
    _self.formFiltersChanges($('.JSformFilters'));
    
    if (typeof fnExecuteFunction !== 'undefined' && $.isFunction(fnExecuteFunction)) {
      fnExecuteFunction();
    }
  };

  this.menuBreadcrumbs = function () {
    $('#header_breadcrumb').html(_self.getSessionStorage('breadcrumbs'));

    $('#menuBreadcrumbs').on('click', 'a', function () {
      var path = $('#menuBreadcrumbs').attr('data-breadcrumbs-default');

      if (!$(this).hasClass('JSlogout')) {
        path = $(this).attr('title') ? $(this).attr('title') : $(this).html();

        if ($(this).attr('data-level')) {
          $(this).parents('li.has-dropdown').each(function () {
            var ele = $('a:first', $(this));
            var prev = ele.attr('title') ? ele.attr('title') : ele.html();
            path = prev + ' / ' + path;
          });
        }
      }

      _self.setSessionStorage('breadcrumbs', path);
    });
  };

  this.setSessionStorage = function (key, val) {
    if (typeof (Storage) !== "undefined") {
      sessionStorage[key] = val;
    } else {
      document.cookie = key + "=" + val + "; path=/";
    }
  };

  this.getSessionStorage = function (key) {
    var resp = '';

    if (typeof (Storage) !== "undefined") {
      if (sessionStorage[key]) {
        resp = sessionStorage[key];
      }
    } else {
      var name = key + "=";
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          resp = c.substring(name.length, c.length);
        }
      }
    }

    return resp;
  };

  this.cargando = function () {
    $('#cargando').show();
  };

  this.cargado = function () {
    $('#cargando').hide();
  };

  this.errorAjax = function (jqXHR, ajaxSettings, thrownError) {
    alert(txt_global_error);
    _self.cargado();
  };

  this.getScriptName = function () {
    var scrpt = '';

    if ($(location).attr('href').indexOf('app_dev.php') != -1) {
      scrpt = '/app_dev.php';
    }

    return scrpt;
  };

  this.initTableFilters = function (e) {
    var tableFilterFn = function(form, action){
      var relTable = form.attr('data-rel-table');
      action = action ? action : 'search';
      
      if (action === 'search') {
        if(window[relTable]){
          window[relTable].fnDraw();
        }
        if(typeof agendaCalendar != 'undefined'){
          agendaCalendar.fullCalendar( 'removeEventSource', agendaCalendarSource );
          agendaCalendarSource.url = _self.fnGetFilterData(baseAgendaUrl, $('#calendar').attr('data-rel-table')); 
          agendaCalendar.fullCalendar( 'addEventSource', agendaCalendarSource );
        }
      }
      if (action === 'clean') {
        form.trigger("reset");
        $('select', form).trigger("change");

        if(window[relTable]){
          window[relTable].fnDraw();
        }
        if(typeof agendaCalendar != 'undefined'){
          agendaCalendar.fullCalendar( 'removeEventSource', agendaCalendarSource );
          agendaCalendarSource.url = _self.fnGetFilterData(baseAgendaUrl, $('#calendar').attr('data-rel-table')); 
          agendaCalendar.fullCalendar( 'addEventSource', agendaCalendarSource );
        }
      }
      
      return false;
    };
    
    var btn = $(e);
    var action = btn.attr('data-action');
    var form = btn.closest('form');

    tableFilterFn(form, action);  

    _self.formFiltersChanges(form);    
  };

  this.formFiltersChanges = function (form) {
    if(form.length){
      $('*', form).each(function () {
        var ele = $(this);

        if(ele.is(':input')){
          var id = ele.attr('id');
          var label = ele.prev('label');

          if(!label.length){
            label = $('label[for=' + id + ']', form);
          }

          if(label.length){
            if (ele.val() && ele.val() != 'all') {
              label.addClass('form_change');
            } else {
              label.removeClass('form_change');
            } 
          } 
        }
      });
    }
  };

  this.getFormData = function (form) {
    var unindexed_array = form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
      indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
  };

  this.initColorPicker = function () {
    if ($('.JScolorPicker').length) {
      $('.JScolorPicker').each(function () {
        var ele = $(this);
        var color = ele.val() ? ele.val() : '';

        if (color) {
          ele.attr('style', 'background-color: ' + color + '!important;');
        }

        ele.ColorPicker({
          color: color,
          onShow: function (colpkr) {
            $(colpkr).fadeIn(500);
            return false;
          },
          onHide: function (colpkr) {
            $(colpkr).fadeOut(500);
            return false;
          },
          onChange: function (hsb, hex, rgb) {
            ele.val(hex);
            ele.attr('style', 'background-color: #' + hex + '!important;');
          }
        });
      });
    }
  };

  this.initDatePicker = function () {
    $('.fdatepicker').css('width', 'auto');
    $('.ftimepicker').css('width', 'auto');
    
    $('.fdatepicker').datetimepicker({
      lang: $('body').attr('data-locale'),
      timepicker: false,
      format: txt_date_picker.format,
      dayOfWeekStart: txt_date_picker.weekStart,
      closeOnDateSelect: true
    });
    
    $('.ftimepicker').datetimepicker({
      lang: $('body').attr('data-locale'),
      datepicker: false,
      format: 'H:i',
      step: 5,
      closeOnDateSelect: true
    });
    
    $('.fdatetimepicker').datetimepicker({
      lang: $('body').attr('data-locale'),
      format: txt_date_picker.format + ' H:i',
      step: 5
    });
  };

  this.iniciarModal = function (modal) {
    $('body').css('overflow', 'hidden');

    _self.setFormFieldSize(modal);
    _self.updateEntityModalPaginatorTexts(modal);
    
    modal.animate({scrollTop: 0});
    if (!$('a.close-reveal-modal', modal).length) {
      modal.append('<a class="close-reveal-modal">&#215;</a>');
    }
    
    _self.cargado();

    var input = $('form', modal).find('input[type=text],textarea,select').filter(':visible:first');
    if (input.length && !input.hasClass('fdatepicker')) {
      input.focus();
    }
    
    $('.tab-title a', modal).each(function(){
      var idRef = $(this).attr('href').replace('#', '');
      var newId = idRef + '_' + modal.attr('id');
      
      $(this).attr('href', '#' + newId);
      
      var tabContainer = $('.tabs-content #' + idRef, modal);
      tabContainer.attr('id', newId);
    });

    $('.tab-title a', modal).on('click', function(){
      modal.attr('data-origin-table-tab', $(this).attr('href'));
    });

    if(modal.attr('data-origin-table-tab') && $('a[href='+modal.attr('data-origin-table-tab')+']', modal).length){
      $('.tab-title.active', modal).removeClass('active');
      $('.tabs-content .content.active').removeClass('active');

      $('a[href='+modal.attr('data-origin-table-tab')+']', modal).closest('.tab-title').addClass('active');
      $(modal.attr('data-origin-table-tab')).addClass('active');
    }

    $(document).foundation('tab', 'reflow');

    _self.documentReadyFunctions();
  };

  this.formularioModalAjax = function ($form, $modal, tableClass) {
    var redirect = '';
    $form.ajaxForm({
      beforeSubmit: function (formData, jqForm, options) {
        _self.cargando();
      },
      success: function (response, statusText, xhr, $form) {
        $modal.html(response);

        if (Number($('.modal_form_valid', $modal).val()) === 1) {
          if ($('.modal_form_tabs', $modal).length && $('.modal_form_tabs', $modal).val()) {
            redirect = $('.modal_form_tabs', $modal).val();
          }

          if (window['oTableRel' + tableClass] && window['oTableRel' + tableClass].length) {
            window['oTableRel' + tableClass ].fnDraw();
          } else if (window['oTable' + tableClass] && window['oTable' + tableClass].length) {
            window['oTable' + tableClass ].fnDraw();
          } else if ($('#calendar').length) {
            $('#calendar').fullCalendar('refetchEvents');
          }

          if (redirect) {
            $.ajax({
              url: redirect,
              success: function (data, textStatus, jqXHR) {
                $modal.html(data);
                _self.iniciarModal($modal);
                _self.cargado();
              },
              error: function (jqXHR, textStatus, errorThrown) {
                _self.errorAjax(jqXHR, textStatus, errorThrown);
              }
            });
          } else {
            //$modal.foundation('reveal', 'close');
            _self.iniciarModal($modal);
            _self.cargado();
          }
        } else {
          _self.iniciarModal($modal);
          _self.cargado();
        }
      },
      error: function (jqXHR, ajaxSettings, thrownError) {
        _self.errorAjax(jqXHR, ajaxSettings, thrownError);
      }
    });
  };

  this.createDataTable = function (tabla, opciones, filtrosBusqueda) {
    var $_tabla = $('#tablaContenido_' + tabla);
    var postParameters = {};
    
    window['oTable' + tabla] = $_tabla.dataTable({
      "responsive": true,
      "bAutoWidth": false,
      "sPaginationType": "full_numbers",
      "oLanguage": txt_data_tables,
      "bFilter": true,
      "bProcessing": true,
      "bServerSide": true,
      "sAjaxSource": ($('html:contains("documentRender")').length) ? _self.getScriptName() + "/documentSeleccionAjax" : _self.getScriptName() + "/seleccionAjax",
      "aoColumns": $.parseJSON(opciones.estadoColumnas),
      "aaSorting": $.parseJSON(opciones.campoOrdenacionDefecto),
      "iDisplayLength": opciones.numRegistrosTabla,
      "iDisplayStart": opciones.paginaTabla,
      "oSearch": {"sSearch": opciones.busquedaTabla},
      "fnServerData": function (sSource, aoData, fnCallback) {
        sSource = _self.fnGetFilterData(sSource, tabla);
        aoData.push({"name": "entityClass", "value": opciones.entityClass});
        postParameters = aoData;
        
        $.ajax({
          "dataType": 'json',
          "type": "POST",
          "url": sSource,
          "data": aoData,
          "success": fnCallback
        });
      },
      "drawCallback": function(){
        _self.checkDateAlertFields();
      },
      "bSort": true
    });
    window['oTable' + tabla]['postParameters'] = postParameters;

    /** Seleccionar/Deseleccionar fila al pulsar en ella **/
    _self.fnSelectRowsOnClick($_tabla);

    /** Boton seleccionar todos **/
    _self.fnSeleccionarTodos(window['oTable' + tabla]);

    /** Boton deseleccionar todos **/
    _self.fnDeseleccionarTodos(window['oTable' + tabla]);

    /** Eliminar registros seleccionados **/
    _self.fnEliminiarRegistros(window['oTable' + tabla]);

    /** Exportar a excel **/
    _self.fnExportToExcel(window['oTable' + tabla]);
  };

  this.createDataTableRel = function (tabla, opciones) {
    var $_tabla = $('#tablaContenidoRel_' + tabla);
    var postParameters = {};

    if($_tabla.length){    
      window['oTableRel' + tabla] = $_tabla.dataTable({
        "bAutoWidth": false,
        "sPaginationType": "full_numbers",
        "oLanguage": txt_data_tables,
        "bFilter": true,
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": _self.getScriptName() + "/seleccionAjax",
        "aoColumns": $.parseJSON(opciones.estadoColumnas),
        "aaSorting": $.parseJSON(opciones.campoOrdenacionDefecto),
        "iDisplayLength": opciones.numRegistrosTabla,
        "iDisplayStart": opciones.paginaTabla,
        "oSearch": {"sSearch": opciones.busquedaTabla},
        "fnServerData": function (sSource, aoData, fnCallback) {
          sSource = _self.fnGetFilterData(sSource, tabla);
          
          aoData.push({"name": "entityClass", "value": opciones.entityClass});
          aoData.push({"name": "originId", "value": opciones.entity_id});
          aoData.push({"name": "originClass", "value": opciones.entity_class});
          aoData.push({"name": "nmRel", "value": $_tabla.hasClass('nmRel') ? $_tabla.attr('data-nmRel-class') : ''});
          if($_tabla.hasClass('nmRel')){
            aoData.push({ "name": "nmRelFiltro", "value": $('.JSnmRelFiltro', $_tabla.closest('.bloqueSeleccion')).val() });
          }
          postParameters = aoData;
          
          //La siguiente llamada, la hace por GET, por lo que pasa todos los parametros por URL. Incluida la SQL, por lo que si es demasiado grande, dependiendo del servidor, puede fallar.
          $.ajax({
            "dataType": 'json',
            "type": "POST",
            "url": sSource,
            "data": aoData,
            "success": fnCallback
          });
        },
        "drawCallback": function(settings){
          _self.checkDateAlertFields();
          $('.JScheckbox', $_tabla).each(function(){
            _self.modalNmRelsActions($(this));
          }); 
          $('.JScheckbox', $_tabla).on('change', function(){
            _self.modalNmRelsActions($(this));
          }); 

          if(opciones.hasOwnProperty('tabInfo') && opciones.tabInfo){
            var tabId = $_tabla.closest("div[id^='tab_'].content.bloqueSeleccion").attr('id');
            var tab = $('a[href=#'+tabId+']', $_tabla.closest('.reveal-modal')).closest('li');


            _self.tabInfo(tab, tabla, settings.fnRecordsDisplay(), $_tabla.hasClass('nmRel'));
          }
        },
        "bSort": true
      });
      window['oTableRel' + tabla]['postParameters'] = postParameters;

      /** Seleccionar/Deseleccionar fila al pulsar en ella **/
      _self.fnSelectRowsOnClick($_tabla);

      /** Boton seleccionar todos **/
      _self.fnSeleccionarTodos(window['oTableRel' + tabla]);

      /** Boton deseleccionar todos **/
      _self.fnDeseleccionarTodos(window['oTableRel' + tabla]);

      /** Eliminar registros seleccionados **/
      _self.fnEliminiarRegistros(window['oTableRel' + tabla]);

      /** Exportar a excel **/
      _self.fnExportToExcel(window['oTableRel' + tabla]);
    }
  };

  this.tabInfo = function(tab, tabla, nRecords, nmRel){
    var tabla = tabla ? tabla : false;
    var nRecords = nRecords ? nRecords : 0;
    var nmRel = nmRel ? nmRel : false;
    var url = tab.attr('data-info-url');

    $('.tab-info', tab).html('');

    if(url){
      var postParameters = {};

      if(tabla){
        url = _self.fnGetFilterData(url, tabla);
        postParameters = window['oTableRel' + tabla]['postParameters'];
      }

      $.ajax({
        url: url,
        type: 'POST',
        data: postParameters,
        success: function(response){
          $('.tab-info', tab).html('(' + response + ')');
        },
        error: function(jqXHR, ajaxSettings, thrownError){
          console.log(jqXHR);
        },
      });      
    }else{
      if(nmRel){
        if(tabla){
          var data = { nmRel: '', originId: '', originClass: '' };

          $.each(window['oTableRel' + tabla]['postParameters'], function(i, p){
            if(p.name == 'nmRel'){
              data.nmRel = p.value;
            }
            if(p.name == 'originClass'){
              data.originClass = p.value;
            }
            if(p.name == 'originId'){
              data.originId = p.value;
            }
          });

          $.ajax({
            url: '/infoNmRelation',
            data: data,
            success: function(response){
              $('.tab-info', tab).html('(' + response + ')');
            },
            error: function(jqXHR, ajaxSettings, thrownError){
              $('.tab-info', tab).html('(' + nRecords + ')');
            },
          });          
        }else{
          $('.tab-info', tab).html('(' + nRecords + ')');
        }
      }else{
        $('.tab-info', tab).html('(' + nRecords + ')');
      }
    }
  };
       
  this.fnGetFilterData = function(sSource, tabla){
    var filterForm = $('form[data-rel-table=oTableRel' + tabla + ']');
    if (filterForm.length) {
      var filterData = $('form[data-rel-table=oTableRel' + tabla + ']').serialize();
      sSource = _self.addParamToUrl(sSource, filterData);
    }else{
      filterForm = $('form[data-rel-table=oTable' + tabla + ']');
      if (filterForm.length) {
        var filterData = $('form[data-rel-table=oTable' + tabla + ']').serialize();
        sSource = _self.addParamToUrl(sSource, filterData);
      }
    }
    
    return sSource;
  };
  
  this.fnExportToExcel = function(oTable){
    $('.exportarExcel', oTable.closest('.bloqueSeleccion')).click(function () {
      var url = $(this).attr('data-url');
      var tabla = $(this).attr('data-rel');
      var sSource = _self.fnGetFilterData(url, tabla);
      _self.cargando();

      $.ajax({
        "dataType": 'text',
        "type": "POST",
        "url": sSource,
        "data": oTable.postParameters,
        "success": function(data){
          _self.cargado();
          url = _self.addParamToUrl(url, 'tabla=' + tabla);
          $(location).attr('href', url);
        },
        "error": function (jqXHR, ajaxSettings, thrownError) {
          _self.errorAjax(jqXHR, ajaxSettings, thrownError);
        }
      });
    });
  };
  
  this.fnSelectRowsOnClick = function (tabla) {
    tabla.on('click', 'tbody tr td', function () {
      var tr = $(this).closest('tr');
      if (tr.attr('id') != 'cabeceraTabla' && !$(this).hasClass('tdOpciones')) {
        tr.toggleClass('row_selected');
        if($('.JScheckbox', tr).length){
          $('.JScheckbox', tr).prop('checked', tr.hasClass('row_selected'));
         _self.modalNmRelsActions($('.JScheckbox', tr));
        }
      }
    });
  };

  this.fnSeleccionarTodos = function (tabla) {
    $('.seleccionarTodos', tabla.closest('.bloqueSeleccion')).click(function () {
      var aTrs = tabla.fnGetNodes();
      for (var i = 0; i < aTrs.length; i++) {
        if (!$(aTrs[i]).hasClass('row_selected')) {
          $(aTrs[i]).addClass('row_selected');
        }
      }
    });
  };

  this.fnDeseleccionarTodos = function (tabla) {
    $('.deseleccionarTodos', tabla.closest('.bloqueSeleccion')).click(function () {
      var aTrs = tabla.fnGetNodes();
      for (var i = 0; i < aTrs.length; i++) {
        if ($(aTrs[i]).hasClass('row_selected')) {
          $(aTrs[i]).removeClass('row_selected');
        }
      }
    });
  };

  this.fnEliminiarRegistros = function (tabla) {
    $('.eliminarRegistros', tabla.closest('.bloqueSeleccion')).click(function (event) {
      event.preventDefault();
      var anSelected = _self.fnGetSelected(tabla);

      if (anSelected.length) {
        if (confirm(txt_delete_selected)) {
          _self.cargando();

          var ids = [];
          var nm = 0;
          for (var i = 0; i < anSelected.length; i++) {
            var id = tabla.fnGetData(anSelected[i])[0].toString();
            ;
            if (id.indexOf('_') !== -1) {
              nm = 1;
            }
            ids.push(id);
          }

          $.ajax({
            url: $(this).attr('href'),
            data: {ids: ids, nm: nm},
            type: 'DELETE',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
              _self.cargado();
              tabla.fnDeleteRow(anSelected);
            },
            error: function (jqXHR, textStatus, errorThrown) {
              _self.errorAjax(jqXHR, textStatus, errorThrown);
            }
          });
        }
      }
    });
  };

  this.fnGetSelected = function (oTableLocal) {
    var aReturn = [];
    var aTrs = oTableLocal.fnGetNodes();

    for (var i = 0; i < aTrs.length; i++) {
      if ($(aTrs[i]).hasClass('row_selected')) {
        aReturn.push(aTrs[i]);
      }
    }

    return aReturn;
  };

  this.objectRelCreation = function () {
    $('[data-rel-entity]').each(function () {
      var obj = $(this);
      var parent = obj.parent();

      if (!$('.newRelObject', parent).length) {
        var btn = '<a class="newRelObject" onclick="appUtil.newRelObject(this);" title="' + txt_add_object + '"><i class="fa fa-plus"></i></a>';
        //obj.width(obj.width() - 30);
        obj.after(btn);
      }

      if (obj[0].nodeName === 'SELECT') {
        _self.select2configuration(obj);
      }
    });
  };

  this.newRelObject = function (e) {
    var parent = $(e).parent();
    var modal = $(e).closest('.reveal-modal');
    var scrpt = _self.getScriptName();
    var obj = $('[data-rel-entity]', parent);
    var entityClass = obj.attr('data-rel-entity');
    var originClass = obj.attr('data-rel-origin-class');
    var originId = obj.attr('data-rel-origin-id');
    var parentModal = $(e).closest('.reveal-modal');
    var data = { entityClass: entityClass };
    
    if(originClass && originId){
      data['originId'] = originId;
      data['originClass'] = originClass;
    }

    _self.cargando();
    
    $.ajax({
      url: scrpt + "/objectRelCreation",
      data: data,
      dataType: 'json',
      success: function (data, textStatus, jqXHR) {
        var modal = _self.createModal();
        if (parentModal.length) {
          modal.css('min-height', parentModal.outerHeight());
        }
        modal.attr('data-rel-id', obj.attr('id'));
        modal.html(data.html);
        modal.foundation('reveal', 'open', {multiple_opened: true});
        _self.cargado();
      },
      error: function (jqXHR, textStatus, errorThrown) {
        _self.errorAjax(jqXHR, textStatus, errorThrown);
      }
    });
  };

  this.showModal = function (e) {
    _self.cargando();
    var parentModal = $(e).closest('.reveal-modal');
    var tabla = $(e).closest('.tablaContenido');

    $.ajax({
      url: $(e).attr('data-url'),
      dataType: 'html',
      success: function (data, textStatus, jqXHR) {
        var modal = _self.createModal();
        if (parentModal.length) {
          modal.css('min-height', parentModal.outerHeight());
        }
        
        modal.html(data); 
        _self.editEntityModalPaginator(e, tabla, modal);       
        modal.foundation('reveal', 'open', {multiple_opened: true});
      },
      error: function (jqXHR, textStatus, errorThrown) {
        _self.errorAjax(jqXHR, textStatus, errorThrown);
      }
    });
  };

  this.createModal = function (cls) {
    cls = cls ? cls : 'xlarge';
    var id = 'myModal_' + Date.now();

    $('body').append('<div id="' + id + '" class="JSremoveOnClose reveal-modal ' + cls + '" data-reveal></div>');
    var modal = $('#' + id);

    return modal;
  };

  this.addRelationForm = function (form) {
    var deleteLink = '<a href="#" class="delete-relation" title="' + txt_remove_rel_form + '"><i class="fa fa-trash-o"></i></a>';

    $('.add-another-relation').attr('title', txt_add_rel_form);

    $('.entity_relation_form_row').each(function () {
      $('ul:first li', $(this)).each(function () {
        if (!$('.delete-relation', $(this)).length) {
          $('div:first', $(this)).append(deleteLink);
        }
      });
    });


    form.on('click', '.add-another-relation', function (e) {
      e.preventDefault();

      var relList = $('.fields-list', $(this).closest('.entity_relation_form_row'));
      var newWidget = relList.attr('data-prototype');
      var n = $('li', relList).length;

      newWidget = newWidget.replace(/__name__/g, n);

      var newLi = $('<li></li>').html(newWidget);
      $('div:first', newLi).append(deleteLink);
      newLi.appendTo(relList);

      _self.objectRelCreation();
    }).on('click', '.delete-relation', function (e) {
      e.preventDefault();
      var li = $(this).closest('li');
      li.remove();
    });
  };

  this.initSelect2 = function () {
    $("select:not(.JSselect2No)").each(function () {
      var ele = $(this);

      _self.select2configuration(ele);
    });
  };

  this.select2configuration = function (ele) {
    if (!ele.hasClass('JSselect2No') && !ele.closest('.rowFormDateTime').length) {
      var conf = {
        language: $('body').attr('data-locale'),
        placeholder: ele.attr('placeholder'),
        width: 'element'
      };

      if (ele.attr('data-ajax--url')) {
        var confAjax = {
          ajax: {
            delay: 250,
            url: ele.attr('data-ajax--url'),
            dataType: 'json',
            data: function (params) {
              return {
                q: params.term
              };
            },
            processResults: function (data) {
              return {
                results: data
              };
            },
            cache: false
          },
          minimumInputLength: 2
        };
        $.extend(conf, confAjax);
      }

     ele.select2(conf);
     ele.on("select2:open", function (e) { 
      var ul = $('#select2-' + e.target.id + '-results');
      if(ul.length){
        var firstOption = $('li:eq(0)', ul);
        if(firstOption.length && $.trim(firstOption.html()) == ''){
          firstOption.html('----');
        }
      } 
     });
    }
  };

  this.formChanges = function () {
    $('form *').each(function () {
      var loginForm = $(this).closest('.login-form');

      if(!loginForm.length){
        var ele = $(this);

        if(ele.is(':input')){
          var form = ele.closest('form');

          if(!form.hasClass('JSformFilters')){
            var id = ele.attr('id');
            var label = ele.prev('label');
            if(!label.length) label = $('label[for=' + id + ']', form);

            ele.data('oldVal', ele.val()); 
            if(ele.is(':checkbox')){
              ele.data('oldVal', ele.is(':checked'));
            }
            
            ele.bind("propertychange change click keyup input paste", function () {
              var val = ele.val();

              if(ele.is(':checkbox')){
                val = ele.is(':checked');
              }

              if (ele.data('oldVal') != val) {
                if (label.length) {
                  label.addClass('form_change');
                  if($('.entity_form_info', label).length){
                    if(val){
                      $('.entity_form_info', label).hide();
                    }else{
                      $('.entity_form_info', label).show();
                    }
                  }
                }
                if ($('[type=submit]', form).length) {
                  $('[type=submit]', form).addClass('form_change_submit');
                  if(!$('[type=submit] .JSalertFormChange', form).length){
                    $('[type=submit]', form).prepend('<i class="fa fa-exclamation-triangle JSalertFormChange"></i>');
                    $('[type=submit]', form).attr('title', txt_title_form_change);
                  }
                }
              } else {
                label.removeClass('form_change');
                if(!val && $('.entity_form_info', label).length){
                  $('.entity_form_info', label).show();
                }
                if(!$('label.form_change', form).length && $('[type=submit]', form).length){
                  $('[type=submit]', form).removeClass('form_change_submit');
                  $('[type=submit] .JSalertFormChange', form).remove();
                  $('[type=submit]', form).attr('title', '');
                }
              }
            });
            
            if(ele.hasClass('JSdateAlertField') && ele.val() && (!$('.JSdateAlertEndField', form).length || !$('.JSdateAlertEndField', form).val())){
              var fecha = moment(ele.val(), txt_date_picker.format.toUpperCase());        
              _self.addIconAlertField(fecha, label);
            }
          }
        }
      }
    });
  };
  
  this.parpadeo = function(){
    $('.JSblink').fadeIn(500).delay(250).fadeOut(500, _self.parpadeo);
  };
  
  this.diffBetweenDates = function(a, b){
    b = b ? b : moment().startOf('day');
    
    var diff = a.diff(b, 'days');
    
    return diff;
  };
  
  this.addParamToUrl = function(url, param){
    if(url.indexOf('?') === -1){
      url += '?';
    }else{
      url += '&';
    }
    
    url += param;
      
    return url;
  };
  
  this.moreInfoCalendarEvent = function(e){
    e.preventDefault();
    e.stopPropagation();
    
    var fc = $(e.target).closest('.fc-content');
    var o = $('.JSmoreInfo', fc);
    
    if(o.hasClass('fa-caret-down')){
      fc.css('white-space', 'unset');
    }else{
      fc.css('white-space', 'nowrap');
    }
    o.toggleClass('fa-caret-down');
    o.toggleClass('fa-caret-up');
    
    return false;
  };
  
  this.checkDateAlertFields = function(){    
    $('td.JSdateAlertField').each(function(){
      var td = $(this);
      var tr = td.closest('tr');
      var tdEnd = $('td.JSdateAlertEndField', tr);
            
      if(td.html() && (!tdEnd.length || !tdEnd.html())){
        var fecha = moment(td.html(), txt_date_picker.format.toUpperCase());
        _self.addIconAlertField(fecha, td);
      }
    });
    //_self.parpadeo();
  };
  
  this.addIconAlertField = function(fecha, ele){
    var diff = _self.diffBetweenDates(fecha);

    if(diff < 0){
      if(!$('.fa-exclamation-triangle',ele).length){
        ele.append('<i class="fa fa-exclamation-triangle JSblink" style="margin: 0 5px;"></i>');
      }
    }
    if(diff == 0){
      if(!$('.fa-bell',ele).length){
        ele.append('<i class="fa fa-bell JSblink" style="margin: 0 5px;"></i>');
      }
    }
  };
  
  this.timerAlertBox = function(){
    setTimeout(function() {
      $(".alert-box").fadeOut(2000, "linear");
    }, 3000 );
  };
  
  this.checkboxAll = function(e){
    var checked = $(e).is(':checked');
    var table = $(e).closest('.tablaContenido');
    
    $('.JScheckbox', table).prop('checked', checked);
    if(checked){
      $('.JScheckbox', table).parents('tr').addClass('row_selected');
    }else{
      $('.JScheckbox', table).parents('tr').removeClass('row_selected');
    }
  };
  
  this.checkboxOne = function(e){
    var table = $(e).closest('.tablaContenido');
    var checkAll = $('.JScheckboxAll', table);
    
    if(checkAll.length){
      var total = $('tbody .JScheckbox', table).length;
      var checked = $('tbody .JScheckbox', table).filter(':checked').length;
      
      checkAll.prop('checked', total === checked);
    }
  };
  
  this.editNmRelation = function(e){
    var table = $(e).closest('.tablaContenido');
    if(table.length){
      var url = _self.addParamToUrl($(e).attr('data-url'), 'relClass=' + table.attr('data-rel-class') + '&refClass=' + table.attr('data-ref-class'));
      
      $(e).attr('data-url', url);
      
      _self.showModal(e);
    }
  };
  
  this.modalNmRelsActions = function(obj){
    if(obj.is(':checked')){
      obj.closest('tr').addClass('row_selected');
      $('.tdOpciones a', obj.closest('tr')).removeClass('hide');
    }else{
      obj.closest('tr').removeClass('row_selected');
      $('.tdOpciones a', obj.closest('tr')).addClass('hide');
    }
  };
  
  this.saveNmRelation = function(tabla, url){
    _self.cargando();
    var table = $('#' + tabla);
    var params = [
      'relClass=' + table.attr('data-rel-class'),
      'refClass=' + table.attr('data-ref-class'),
    ].join('&');
    var src = _self.addParamToUrl(url, params);
    var data = [];
    
    $('.JScheckbox', table).each(function(){
      var ids = $(this).val().split('_');
      data.push({
        'checked': $(this).is(':checked'),
        'originId': ids[0],
        'id': ids[1]
      });
    });
    
    $.ajax({
      url: src,
      data: { checks: JSON.stringify(data) },
      success: function (data, textStatus, jqXHR) {
        // var oTable = table.dataTable();
        // oTable.fnDraw();
        var tabId = table.closest("div[id^='tab_'].content.bloqueSeleccion").attr('id');
        var tab = $('a[href=#'+tabId+']', table.closest('.reveal-modal')).closest('li');

        _self.tabInfo(tab, tabla.replace('tablaContenidoRel_', ''), 0, true);
        _self.cargado();
      },
      error: function (jqXHR, textStatus, errorThrown) {
        _self.errorAjax(jqXHR, textStatus, errorThrown);
      }
    });
  };
  
  this.setFormFieldSize = function(container){
    var obj = $('textarea', container).closest('.entity_form_row');
    
    if(obj.length){
      obj.css('width', '100%');
    }

    var obj = $('input.width100', container).closest('.entity_form_row');
    
    if(obj.length){
      obj.css('width', '100%');
    }
  };

  this.modalPrevEntity = function(e){
    var modal = $(e).closest('.reveal-modal');
    var currentIndex = Number(modal.attr('data-origin-table-current'));

    if(currentIndex > 0){
      modal.attr('data-origin-table-current', currentIndex - 1);

      _self.modalUpdateEntity(modal);
    }
  };

  this.modalNextEntity = function(e){
    var modal = $(e).closest('.reveal-modal');
    var currentIndex = Number(modal.attr('data-origin-table-current'));
    var totalIndex = Number(modal.attr('data-origin-table-total'));

    if((currentIndex + 1) < totalIndex){
      modal.attr('data-origin-table-current', currentIndex + 1);

      _self.modalUpdateEntity(modal);
    }
  };

  this.modalUpdateEntity = function(modal){
    _self.cargando();

    var tabla = modal.attr('data-origin-table');
    var postParameters = window[tabla]['postParameters'];
    var currentIndex = Number(modal.attr('data-origin-table-current'));
    var sSource = _self.fnGetFilterData(_self.getScriptName() + "/seleccionAjax", tabla.replace('oTableRel', '').replace('oTable', ''));

    postParameters[3].value = currentIndex;
    postParameters[4].value = "1";

    $.ajax({
      url: sSource,
      type: 'POST',
      dataType: 'json',
      data: postParameters,
      success: function(response){
        if(response.hasOwnProperty('aaData') && response.aaData.hasOwnProperty(0)){
          var entityId = response.aaData[0][0];
          var url = modal.attr('data-origin-table-url').replace('__ID__', entityId);

          $.ajax({
            url: url,
            dataType: 'html',
            success: function(response){
              modal.html(response);

              _self.iniciarModal(modal);
              _self.cargado();
            },
            error: function(jqXHR, ajaxSettings, thrownError){
              appUtil.errorAjax(jqXHR, ajaxSettings, thrownError);
            },
          });
        }else{
          _self.cargado();
        }
      },
      error: function(jqXHR, ajaxSettings, thrownError){
        appUtil.errorAjax(jqXHR, ajaxSettings, thrownError);
      },
    });
  };

  this.updateEntityModalPaginatorTexts = function(modal){
    if(modal.attr('data-origin-table-current')){
      var currentIndex = Number(modal.attr('data-origin-table-current'));

      $('.entityEditPaginatorCurrent', modal).html(currentIndex + 1);
      $('.entityEditPaginatorTotal', modal).html(modal.attr('data-origin-table-total'));

      $('.entityEditPaginator i.fa:eq(0)', modal).addClass('fa-caret-left');
      $('.entityEditPaginator i.fa:eq(1)', modal).addClass('fa-caret-right');

      if(currentIndex === 0){
        $('.entityEditPaginator i.fa:eq(0)', modal).removeClass('fa-caret-left');
      }
      if(currentIndex === ( Number(modal.attr('data-origin-table-total')) - 1)){
        $('.entityEditPaginator i.fa:eq(1)', modal).removeClass('fa-caret-right');
      }
    }else if($('.entityEditPaginator', modal).length) {
      $('.entityEditPaginator', modal).hide();
    }
  };

  this.editEntityModalPaginator = function(e, tabla, modal){
    if(tabla && tabla.attr('id') && $(e).hasClass('action_edit')){
      var i = $(e).closest('tr').index();
      var pageInfo = tabla.DataTable().page.info();
      var isTablaRel = (tabla.attr('id').indexOf('tablaContenidoRel_') != -1);
      var currentIndex = Number(pageInfo.start) + i;

      if(isTablaRel){
        var tId = 'oTableRel' + tabla.attr('id').replace('tablaContenidoRel_', '');
      }else{
        var tId = 'oTable' + tabla.attr('id').replace('tablaContenido_', '');
      }

      modal.attr('data-origin-table', tId);
      modal.attr('data-origin-table-current', currentIndex);
      modal.attr('data-origin-table-total', pageInfo.recordsDisplay);
      modal.attr('data-origin-table-url', $(e).attr('data-url-entity'));

      if(currentIndex === 0){
        $('.entityEditPaginator i.fa:eq(0)', modal).removeClass('fa-caret-left');
      }
      if(currentIndex === ( pageInfo.recordsDisplay - 1)){
        $('.entityEditPaginator i.fa:eq(1)', modal).removeClass('fa-caret-right');
      }

      $('.entityEditPaginatorCurrent', modal).html(currentIndex + 1);
      $('.entityEditPaginatorTotal', modal).html(pageInfo.recordsDisplay);
    }else if($('.entityEditPaginator', modal).length) {
      $('.entityEditPaginator', modal).hide();
    }
  };
};

appUtil = new UtilClass();
