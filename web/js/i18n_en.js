var txt_global_error = "An error has occurred.";
var txt_data_tables = {
	"sProcessing": "Processing...",
	"sLengthMenu": "Show <select class='JSselect2No'><option value='10'>10</option><option value='25'>25</option><option value='50'>50</option><option value='100'>100</option><option value='200'>200</option><option value='500'>500</option><option value='-1'>Todos</option></select> entries",
	"sZeroRecords": "No matching records found",
	"sInfo": "Showing _START_ to _END_ from _TOTAL_ entries",
	"sInfoEmpty": "Showing 0 to 0 from 0 entries",
	"sInfoFiltered": "",
	"sInfoPostFix": "",
	"sSearch": "Search:",
	"sUrl": "",
	"oPaginate": {
		"sFirst":    "<< First",
		"sPrevious": "< Previous",
		"sNext":     "Next >",
		"sLast":     "Last >>"
	
	}
};
var txt_date_picker = {
  format: 'Y-m-d', //Tiene que tener el mismo formato en la clase AppBundle\Util\Util en la función getDatePickerFormat
  weekStart: 1
};
var txt_delete_selected = "You are going to remove the selected records. Do you want to continue?";
var txt_remove_rel_form = "Delete relation";
var txt_add_rel_form = "Add relation";
var txt_add_object = "Add element";
var txt_title_form_change = "The colored fields have been modified but still unsaved changes.";