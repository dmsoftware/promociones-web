function activarDesactivarUsuario(url, id) {
  appUtil.cargando();

  $.ajax({
    url: url,
    dataType: 'json',
    success: function (data) {
      if (data.hasOwnProperty('error')) {
        alert(data.error);
      } else {
        var obj = $('#usuario_' + id + ' img');

        if (data.enabled) {
          obj.attr('src', '/images/green.png');
        } else {
          obj.attr('src', '/images/red.png');
        }
      }

      appUtil.cargado();
    },
    error: function (jqXHR, ajaxSettings, thrownError) {
      appUtil.errorAjax(jqXHR, ajaxSettings, thrownError);
    }
  });
}

function cargarTablaUsuarios() {
  var options = {
    'entityClass': 'CommonBundleUserBundle:User',
    'estadoColumnas': [{"bVisible": false, "bSearchable": false},null,null,{"sClass": "text-center"},null,null,{"sClass": "tdOpciones text-left","bSearchable": false,"bSortable": false}],
    'campoOrdenacionDefecto': 0,
    'direccionOrdenacion': 'asc',
    'numRegistrosTabla': 10,
    'paginaTabla': 0,
    'busquedaTabla': ''
  };
  
  appUtil.createDataTable('usuarios', options, '');
}