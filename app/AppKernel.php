<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new AppBundle\AppBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new A2lix\TranslationFormBundle\A2lixTranslationFormBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Liuggio\ExcelBundle\LiuggioExcelBundle(),
            new CommonBundle\AdministratorBundle\CommonBundleAdministratorBundle(),
            new CommonBundle\UserBundle\CommonBundleUserBundle(),
//	        new Doctrine\Bundle\MongoDBBundle\DoctrineMongoDBBundle(),
            new WhiteOctober\TCPDFBundle\WhiteOctoberTCPDFBundle(),
            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            new RaulFraile\Bundle\LadybugBundle\RaulFraileLadybugBundle()
        );
        
        if(class_exists('Sst\AppBundle\SstAppBundle')){
          $bundles[] = new Sst\AppBundle\SstAppBundle();
        }
        
        if(class_exists('Guascor\AppBundle\GuascorAppBundle')){
          $bundles[] = new Guascor\AppBundle\GuascorAppBundle();
        }
        
        if(class_exists('Prizer\AppBundle\PrizerAppBundle')){
          $bundles[] = new Prizer\AppBundle\PrizerAppBundle();
        }
        
        if(class_exists('pruebas\AppBundle\pruebasAppBundle')){
          $bundles[] = new pruebas\AppBundle\pruebasAppBundle();
        }
        
        if(class_exists('DMIntegra\AppBundle\DMIntegraAppBundle')){
          $bundles[] = new DMIntegra\AppBundle\DMIntegraAppBundle();
          $bundles[] = new Realestate\MssqlBundle\RealestateMssqlBundle();
        }
        
        if(class_exists('ZarautzAgenda\AppBundle\ZarautzAgendaAppBundle')){
          $bundles[] = new ZarautzAgenda\AppBundle\ZarautzAgendaAppBundle();
        }

      	if(class_exists('Promokiss\AppBundle\PromokissAppBundle')){
      	    $bundles[] = new Promokiss\AppBundle\PromokissAppBundle();
            $bundles[] = new Promokiss\PublicBundle\PromokissPublicBundle();
            //$bundles[] = new Promokiss\UsuariosBundle\PromokissUsuariosBundle();
            $bundles[] = new FOS\RestBundle\FOSRestBundle();
            $bundles[] = new Nelmio\ApiDocBundle\NelmioApiDocBundle();
            $bundles[] = new Nelmio\CorsBundle\NelmioCorsBundle();
      	}

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {;
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
//            $bundles[] = new IsmaAmbrosi\Bundle\GeneratorBundle\IsmaAmbrosiGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
